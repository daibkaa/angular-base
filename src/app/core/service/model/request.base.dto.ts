export interface HttpOptions {
    url?: string;
    path?: string;
    body?: any;
    headers?: any;
    params?: any;
    cacheMins?: number;
    isAuthentication?: boolean;
    requestId?: string;
}

export interface HttpResponse {
    code: number;
    data: any;
    duration: number;
    msg?: string;
    path: string;
    timestamp: string;
    requestId?: string;
}
