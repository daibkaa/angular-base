import { Component, Injector, ViewChild } from '@angular/core';
import { ComponentBaseAbstract } from '@shared-sm';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.scss']
})
export class ImportComponent extends ComponentBaseAbstract {

  activeTab = 0;
  constructor(
    protected injector: Injector,
  ) {
    super(injector);
  }

  protected componentInit() {
  }

  onTabChanged($event) {
  }

}
