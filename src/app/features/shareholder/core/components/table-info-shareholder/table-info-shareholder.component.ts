import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DformPaginationPlusComponent } from '@shared-sm';
import { ShareholderService } from '../../services/shareholder.service';

@Component({
  selector: 'app-table-info-shareholder',
  templateUrl: './table-info-shareholder.component.html',
  styleUrls: ['./table-info-shareholder.component.scss'],
})
export class TableInfoShareholdeComponent implements OnInit {

  @Input() dataSource: MatTableDataSource<any>;
  @Input() hasDataSource = false;
  @Input() displayedColumns: string[];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  constructor(
    public _shareholderService: ShareholderService,
  ) {
  }

  // Xử lý dữ liệu đầu vào
  ngOnInit(): void {}

  destroyData() {
  }

}