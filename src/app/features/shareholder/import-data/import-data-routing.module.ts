import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/auth/auth.guard';
import { PermissionCatalogUtilitiesGuard } from 'src/app/core/auth/permission-catalog-utilities.guard';
import { ImportInfoComponent } from './import-data.component';

const routes: Routes = [
  // { path: '', redirectTo: 'shareholder-management', pathMatch: 'full' },
  { 
    path: '', 
    component: ImportInfoComponent, 
    data: { title: 'Thông tin import' },
    canActivate: [AuthGuard, PermissionCatalogUtilitiesGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ImportRoutingModule {
}
