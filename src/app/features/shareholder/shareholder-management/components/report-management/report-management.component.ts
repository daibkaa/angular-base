import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ComponentAbstract, ComponentBaseAbstract, DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import * as moment from 'moment';
import { distinctUntilChanged, finalize, map, mergeMap, takeUntil } from 'rxjs/operators';
import { BC_TYPE, EXCEL_XLS, EXCEL_XLSX, HttpResponse, Status } from 'src/app/core';
import { checkFileType, dateNoneSeparation, removeEmptyObject } from 'src/app/core/utils/utils.function';
import { REJECT_COLUMN_DISPLAYS } from '../../../core/constant';
import { COMPARE_DATE, REPORT_DATE, REPORT_TYPE } from '../../form-setting/form-setting';
import { ShareholderManagementService } from '../../services/shareholder-management.service';

@Component({
  selector: 'app-report-management',
  templateUrl: './report-management.component.html',
  styleUrls: ['./report-management.component.scss'],
})
export class ReportMmanagementComponent extends ComponentAbstract {

  public dataSource: MatTableDataSource<any>;
  public hasDataSource = false;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  reportType = REPORT_TYPE();
  reportDate = REPORT_DATE();
  compareDate = COMPARE_DATE();

  displayedColumns: string[];

  isShowCompareDate: boolean = false;

  showtable;
  listColumnGroup;

  readonly BC_TYPE = BC_TYPE;
  
  constructor(
    protected _injector: Injector,
    private _shareholderManagementService: ShareholderManagementService,
  ) {
    super(_injector);
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.reportType, this.reportDate, this.compareDate]);
    this.onChangeReportType();
  }

  _renderItem(column: string, index) {
    return `${column}-${index}`
  }

  onChangeReportType() {
    this.form.get(this.reportType.key).valueChanges.pipe(
      takeUntil(this.ngUnsubscribe),
      distinctUntilChanged()).subscribe(value => {
        if(value == BC_TYPE.GDLON || value == BC_TYPE.CO_CAU_CD) {
          this.isShowCompareDate = true;
        } else {
          this.isShowCompareDate = false;
        }
      })
  }

  destroyData() {
  }

  search() {
    this.pageIndex = 0;
    this.pageSize = 10;
    this.searchReport();
  }

  searchReport(): void {
    this.validateAllFields(this.form);
    if(!this.form.valid) {
      this.dialogService.error({
        title: 'Thông báo',
        innerHTML: 'Vui lòng nhập thông tin tìm kiếm'
      });
      return;
    }
    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue()
    let params: any = {
      reportType: formData?.reportType,
      pageNo: this.pageIndex,
      pageSize: this.pageSize
    }
    if(formData?.reportDate && formData.reportDate != '') {
      params.reportDate = moment(formData.reportDate).format('YYYY-MM-DD')
    } else {
      params.reportDate = ''
    }
    if(formData?.compareDate && formData.compareDate != '' && this.isShowCompareDate) {
      params.compareDate = moment(formData.compareDate).format('YYYY-MM-DD')
    }
    this._shareholderManagementService.getDataReport(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
      if (res?.code === Status.SUCCESS) {
        if(res?.data?.items) {
          const data = res.data.items.map(m => {
            return {
              ...m,
              stt: this.form.getRawValue().reportType == 'CO_CAU_CD' ? m.stt : (this.pageIndex * this.pageSize) + Number(m.stt)
            }
          });
          if(formData.reportType == BC_TYPE.CO_CAU_CD) {
            this.showtable = BC_TYPE.CO_CAU_CD;
            this.listColumnGroup = data[0].coCauCoDongList

            const colums = data.reduce((a, o) => {
              let tmpItem = {};
              o.coCauCoDongList.reduce((obj, item, index) => {
                
                Object.keys(item).forEach(m => {
                  tmpItem[this._renderItem(m, index)] = item[m];
                })
                obj = {...tmpItem}
                return obj;
              }, {})
              let item = {...o};
              delete item.coCauCoDongList;
              const newItem = {
                stt: item.stt,
                coDong: item.coDong,
                soDksh: item.soDksh,
                ...tmpItem,
                tyLeSoHuuThayDoi: item.tyLeSoHuuThayDoi,
                ghiChu: item.ghiChu
              }
              a.push(newItem)
              return a
            }, [])
            let indexMax = 0;
            colums.forEach((e, i) => {
              if(i > 0 && Object.keys(colums[(i)])?.length > Object.keys(colums[(i - 1)])?.length) {
                indexMax = i;
              }
            })
            this.dataSource = new MatTableDataSource(colums);
            this.displayedColumns = colums.length ? Object.keys(colums[indexMax]) : [];
            this.displayedColumns = (this.displayedColumns || []).filter(f => !REJECT_COLUMN_DISPLAYS.includes(f));
          } else {
            this.dataSource = new MatTableDataSource(data);
            this.showtable = null;
            this.displayedColumns = data.length ? Object.keys(data[0]) : [];
            this.displayedColumns = (this.displayedColumns || []).filter(f => !REJECT_COLUMN_DISPLAYS.includes(f));
          }

          this.hasDataSource = data.length ? true : false;
          
          this.totalItem = res.data.totalCount
        }
      }
    }, error => {
      this.displayedColumns = [];
      this.hasDataSource = false;
      this.dataSource = new MatTableDataSource([]);
      this.totalItem = 0;
    })
  }

  exportExcel() {
    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue()
    let params: any = {
      reportType: formData?.reportType,
      // pageNo: this.pageIndex,
      // pageSize: this.pageSize
    }
    if(formData?.reportDate && formData.reportDate != '') {
      params.reportDate = moment(formData.reportDate).format('YYYY-MM-DD')
    } else {
      params.reportDate = ''
    }
    if(formData?.compareDate && formData.compareDate != '' && this.isShowCompareDate) {
      params.compareDate = moment(formData.compareDate).format('YYYY-MM-DD')
    }
    this._shareholderManagementService.export(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
        // this.openPDF(res.body)
        const blob = new Blob([res.body], { type: 'application/vnd.ms-excel' });
        const url= window.URL.createObjectURL(blob);
        let anchor = document.createElement("a");
        let downloadFile = '';
        switch(formData.reportType) {
          case BC_TYPE.MBB_TONG_HOP: {
            downloadFile = `BC1.MBB_TONG_HOP_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;
          }; break;
          case BC_TYPE.GDLON: {
            downloadFile = `BC2.GDLON_${dateNoneSeparation(params.reportDate, '-')}_${dateNoneSeparation(params.compareDate, '-')}.xlsx`;
          }; break;
          case BC_TYPE.MBB_COTUC_TM: {
            downloadFile = `BC3.MBB_COTUC_TM_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;
          }; break;
          case BC_TYPE.MBB_PHAT_HANH: {
            downloadFile = `BC4.MBB_PHAT_HANH_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;
          }; break;
          case BC_TYPE.CO_CAU_CD: {
            downloadFile = `BC15.CO_CAU_CD_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;
          }; break;
        }

        // const nameFile = formData?.saoKe == '1' ? `BC14.SAO_KE_COTUC_TM.xlsx` : `BC13.SAO_KE_CP.xlsx`

        anchor.download = downloadFile;
        anchor.href = url;
        anchor.click();
    })
  }

  /**
 * Event load data phân trang ((page)="ChanePageIndex($event);")
 */
  ChanePageIndex($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex;
    this.searchReport();
  }

}