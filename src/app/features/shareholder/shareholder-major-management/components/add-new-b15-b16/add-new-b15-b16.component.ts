import { ChangeDetectorRef, Component, Inject, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BC_TYPE } from '@core';
import { ComponentAbstract } from '@shared-sm';
import * as moment from 'moment';
import { IS_ADD_NEW } from '../../form-setting/add-new';
import { STT, CHUCVUMOIQH, DIENTHOAI, EMAIL, LOAIHINHID, NGAY_CAP, NGAY_HIEU_LUC, NOI_CAP, SO_DKSH, TENTCCN, TKGDCK, CHIMUC, DIA_CHI } from '../../form-setting/add-new-b14';
import { ShareholderMajorManagementService } from '../../services/shareholder-major-management.service';

@Component({
  selector: 'app-add-new-b15-b16',
  templateUrl: './add-new-b15-b16.component.html',
  styleUrls: ['./add-new-b15-b16.component.scss']
})
export class AddNewB15B16Component extends ComponentAbstract {

  isAddNew = IS_ADD_NEW();
  
  CHIMUC = CHIMUC();
  STT = STT();
  TENTCCN = TENTCCN();
  TKGDCK = TKGDCK();
  CHUCVUMOIQH = CHUCVUMOIQH();
  DIA_CHI = DIA_CHI();
  LOAIHINHID = LOAIHINHID();
  SO_DKSH = SO_DKSH();
  NGAY_CAP = NGAY_CAP();
  NOI_CAP = NOI_CAP();
  DIENTHOAI = DIENTHOAI();
  EMAIL = EMAIL();
  NGAY_HIEU_LUC = NGAY_HIEU_LUC();

  title;

  constructor(
    protected injector: Injector,
    public _shareholderMajorManagementService: ShareholderMajorManagementService,
    public dialogRef: MatDialogRef<AddNewB15B16Component>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {
    super(injector);
  }
  checked = false;

  protected componentInit(): void {
    this.CHIMUC.options = this.data?.listChiMuc;

    this.form = this.itemControl.toFormGroup([this.STT, this.TENTCCN, this.TKGDCK, this.CHUCVUMOIQH, this.DIA_CHI, this.LOAIHINHID, this.SO_DKSH, this.NGAY_CAP, this.NOI_CAP, this.DIENTHOAI, this.EMAIL, this.CHIMUC]);

    if(this.data?.data?.id) {
      this.form.patchValue(this.data.data)
      this.form.get(this.CHIMUC.key).disable();
      if(this.data?.reportType == BC_TYPE.LUAT_TCTD) {
        this.title = 'Chỉnh sửa thông tin NNB LUAT TCTD'
      } else {
        this.title = 'Chỉnh sửa thông tin NNB LUAT CK'
      }
    } else {
      if(this.data?.reportType == BC_TYPE.LUAT_TCTD) {
        this.title = 'Thêm mới thông tin NNB LUAT TCTD'
      } else {
        this.title = 'Thêm mới thông tin NNB LUAT CK'
      }
    }
  }

  saveData() {
    this.validateAllFields(this.form);
    if (!this.form.valid) {
      this.dialogService.error({
        title: 'dialog.notification',
        message: 'dialog.valid-error',
      }, res => {
        if (res) {
        }
      });
      return;
    }
    
    const formData = this.form.getRawValue()
    let paramDialog = {
      data: {
        ...formData,
        id: this.data.data?.id || null
      },
    }
    this.dialogRef.close(paramDialog)

  }

  closeDialog() {
    if (this.dialogRef.close) { this.dialogRef.close(null); }
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  testData() {
    const data = {
      cdLon: "CDL test",
      diaChi: 'Dia chi test',
      id: null,
      moiQuanHe: null,
      nganhNghe: 'Nganh nghe test',
      ngayCap: "2004-10-07",
      noiCap: "118 Hưng Phú, P.8, Q.8, Tp.HCM",
      parentId: null,
      reportDate: "2022-05-09",
      soDksh: "0300450962",
      stt: "III",
      tenNguoiDaiDienVon: 'Nguoi dai dien test',
      cmtNguoiDaiDien: '142723338',
      validDate: '2022-06-01'
    }
    this.form.patchValue(data)
  }

}
