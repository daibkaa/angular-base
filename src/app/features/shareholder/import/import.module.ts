import { MatIconModule } from '@angular/material/icon';
import { SharedSMModule } from '@shared-sm';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ImportRoutingModule } from './import-routing.module';
import { MatTabsModule } from '@angular/material/tabs';
import { ShareholderCongressService } from './services/shareholder-congress.service';
import { MatTableModule } from '@angular/material/table';
import { ShareHolderCoreModule } from '../core/core.module';
import { ImportComponent } from './import.component';

const COMPONENTS_DYNAMIC = [];
const SERVICE = [ShareholderCongressService];

@NgModule({
  imports: [
    CommonModule,
    SharedSMModule,
    ImportRoutingModule,
    MatIconModule,
    MatTabsModule,
    MatTableModule,
    ShareHolderCoreModule
  ],
  declarations: [
    ImportComponent
  ],
  entryComponents: COMPONENTS_DYNAMIC,
  providers: [...SERVICE]
})
export class ImportModule {
}
