import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { BC_TYPE, HttpOptions, PATH } from 'src/app/core';
import { HttpClientService, Verbs } from 'src/app/core/service/httpclient.service';

@Injectable({
  providedIn: 'root',
})

export class ShareholderMajorManagementService {
  constructor(
    private httpClient: HttpClientService) {
  }

  importValidateEvent(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.IMPORT.IMPORT_VALIDATE,
      body: data
    };
    return this.httpClient.put(options);
  }

  importDataEvent(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.IMPORT.IMPORT_DATA,
      body: data
    };
    return this.httpClient.post(options);
  }

  getDataReport(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.INFO_REPORT,
      params: data
    };
    return this.httpClient.get(options);
  }

  getInfo(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.INFO_MANAGEMENT,
      params: data
    };
    return this.httpClient.get(options);
  }

  getTransactionChange(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.TRANSACTION_CHANGE,
      params: data
    };
    return this.httpClient.get(options);
  }

  getStockMBB(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.STOCK_MBB,
      params: data
    };
    return this.httpClient.get(options);
  }

  updateStockMBB(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.STOCK_MBB,
      body: data
    };
    return this.httpClient.post(options);
  }

  inSertOrUpdateNLQCDL(data: any, params, type) {
    let path = PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.INSEART_OR_UPDATE;
    switch(type) {
      case BC_TYPE.NNB_NLQ_TT22: {
        path = PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.INSERT_OR_UPDATE_T22;
        break;
      }
      case BC_TYPE.NNB_NLQ_LUAT_TCTD: {
        path = PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.INSERT_OR_UPDATE_LUAT_TCTD;
        break;
      }
      case BC_TYPE.NNB_NLQ_LUAT_CK: {
        path = PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.INSERT_OR_UPDATE_LUAT_CK;
        break;
      }
      default: {
        path = PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.INSEART_OR_UPDATE; break;
      }
    }
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: path,
      body: data,
      params: params
    };
    return this.httpClient.put(options);
  }

  deleteNLQCDL(body: any , type) {
    let path = PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.B13_CDL;
    switch(type) {
      case BC_TYPE.TT22: {
        path = PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.B14_T22;
        break;
      }
      case BC_TYPE.LUAT_TCTD : {
        path = PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.B16_LUAT_TCTD;
        break;
      }
      case BC_TYPE.LUAT_CK: {
        path = PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.B15_LUAT_CK;
        break;
      }
      default: {
        path = PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.B13_CDL; break;
      }
    }
    const options: HttpOptions = {
      url: environment.urlAuth,
      path,
      body,
    };
    return this.httpClient.delete(options);
  }

  export(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.EXPORT,
      params: data
    };
    return this.httpClient.download(Verbs.GET, options);
  }

  exportInfo(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.INFO_EXPORT,
      params: data
    };
    return this.httpClient.download(Verbs.GET, options);
  }

  inSertOrUpdateT22(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.INSERT_OR_UPDATE_T22,
      body: data
    };
    return this.httpClient.put(options);
  }

  deleteT22(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.INSERT_OR_UPDATE_T22,
      params: data
    };
    return this.httpClient.delete(options);
  }

  inSertOrUpdateLuatTCTD(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.INSERT_OR_UPDATE_LUAT_TCTD,
      body: data
    };
    return this.httpClient.put(options);
  }

  deleteLuatTCTD(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.INSERT_OR_UPDATE_LUAT_TCTD,
      params: data
    };
    return this.httpClient.delete(options);
  }

  inSertOrUpdateLuatCK(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.INSERT_OR_UPDATE_LUAT_CK,
      body: data
    };
    return this.httpClient.put(options);
  }

  deleteLuatCK(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.INSERT_OR_UPDATE_LUAT_CK,
      params: data
    };
    return this.httpClient.delete(options);
  }

  getInfoManage(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.INFO_MANAGER,
      params: data
    };
    return this.httpClient.get(options);
  }

  inSertOrUpdateB13B16(data: any, rawType) {
    let path = PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.B13_CDL;
    switch(rawType) {
      case BC_TYPE.TT22: {
        path = PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.B14_T22
      }; break;
      case BC_TYPE.LUAT_CK: {
        path = PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.B15_LUAT_CK
      }; break;
      case BC_TYPE.LUAT_TCTD: {
        path = PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.B16_LUAT_TCTD
      }; break;
      default: {
        path = PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.B13_CDL
      }; break;
    }
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: path,
      body: data,
    };
    return this.httpClient.put(options);
  }

  getListChimuc(params: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.CHI_MUC,
      params: params
    };
    return this.httpClient.get(options);
  }

  exportC3C4(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.EXPORT_TRANSACTION_CHANGE,
      params: data
    };
    return this.httpClient.download(Verbs.GET, options);
  }

}
