import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ComponentAbstract, ComponentBaseAbstract, DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import { BehaviorSubject, forkJoin, interval, Subscription } from 'rxjs';
import { finalize, map, mergeMap, takeUntil } from 'rxjs/operators';
import { EXCEL_XLS, EXCEL_XLSX, HttpResponse, MessageSeverity, Status } from 'src/app/core';
import { checkFileType } from 'src/app/core/utils/utils.function';
import { ShareholderManagementService } from '../../../shareholder-management/services/shareholder-management.service';
import { ShareholderService } from '../../services/shareholder.service';
import { TableWarningComponent } from '../table-warning/table-warning.component';

@Component({
  selector: 'app-import-data',
  templateUrl: './import-data.component.html',
  styleUrls: ['./import-data.component.scss'],
})
export class AppImportDataComponent extends ComponentAbstract {

  processBatch: any;
  isCheckBalance = true;
  percent = 0;
  endProcess = false;
  isUpdate = false;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;
  @ViewChild('file') myInputVariable: ElementRef;
  displayedColumns: string[] = ['no', 'accNumber', 'result'];
  pageEvent: PageEvent = new PageEvent();
  dataSource: MatTableDataSource<any>;
  selectedFiles: File;
  fileName: string;

  private layoutChangesSubscription: Subscription;
  subscribe_interval: Subscription;
  
  constructor(
    protected _injector: Injector,
    private _shareholderService: ShareholderService,
  ) {
    super(_injector);
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
  }

  destroyData() {
    // this.settings.setNavState('collapsed', true);
    if (this.layoutChangesSubscription) {
      this.layoutChangesSubscription.unsubscribe();
    }
  }

  handleFileInput(event) {
    const lengthListFile = event.target.files.length;
    if (lengthListFile) {
      if (event && !checkFileType(event, [EXCEL_XLS, EXCEL_XLSX])) {
        this.dialogService.error({
          title: 'Thông báo',
          message: `Sai định dạng file`,
        }, (result: any) => { if (result) { } });
        return;
      }
      this.percent = 0;
      this.selectedFiles = <File>event.target.files[0];
      this.fileName = this.selectedFiles.name;
      const formData = new FormData();
      if (this.selectedFiles) {
        this.indicator.showActivityIndicator()
        formData.append('files', event.target.files[0], this.fileName);
        this._shareholderService.importValidateEvent(formData).pipe(
          map((res: HttpResponse) => {
            if(res.code == Status.SUCCESS) {
              return res?.data;
            }
          }),
          mergeMap((param: any) => {
            formData.append('isWarning', '0');
            return this._shareholderService.importDataEvent(formData)
          }),
          takeUntil(this.ngUnsubscribe),
          finalize(() => this.indicator.hideActivityIndicator())
        ).subscribe((res: any) => {
          if (res && res.code === Status.SUCCESS && !res?.data?.note) {
            this.toastr.showToastr(
              `Tải file thành công`,
              'Thông báo!',
              MessageSeverity.success,
            );
            if(this.fileName.includes('VSD_CHOT')) {
              const data = {
                reportDate: this.getDateFileImport(this.fileName)
              }
              this._shareholderService.warningataEvent(data).pipe(
                takeUntil(this.ngUnsubscribe),
                finalize(() => this.indicator.hideActivityIndicator())
              ).subscribe((res: any) => {
                if (res && res.code === Status.SUCCESS) {
                  if(res?.data?.length) {
                    this.dialogService.componentDialog(TableWarningComponent, {
                      width: '70%',
                      height: '93%',
                      data: {
                        data: res?.data
                      }
                    }, (res) => {
                    });
                  }
                }
                })
              }
          } else {
            const message = res?.data?.note;
            const requestId = res?.requestId;
            this.dialogService.error({
              title: 'Thông báo',
              innerHTML: requestId ? `${message} <br> ${requestId}` : `${message}`
            }, dialog => {
              if (dialog) {
              }
            });
          }
        }, (error: any) => {
          if(error?.error?.code == Status.EXIST) {
            this.dialogService.dformconfirm({
              title: 'Thông báo',
              innerHTML: `File đã tồn tại trên hệ thống <br> Bạn có muốn tiếp tục upload file?`,
              closeBtn: 'Hủy bỏ',
              acceptBtn: 'Xác nhận',
            }, (result: any) => {
              if (result) {
                if(result.status == 2) {
                  this.indicator.showActivityIndicator();
                  formData.append('isWarning', '1');
                  this._shareholderService.importDataEvent(formData).pipe(
                    takeUntil(this.ngUnsubscribe),
                    finalize(() => this.indicator.hideActivityIndicator())
                  ).subscribe((res: any) => {
                    if (res && res.code === Status.SUCCESS && !res?.data?.note) {
                      this.toastr.showToastr(
                        `Tải file thành công`,
                        'Thông báo!',
                        MessageSeverity.success,
                      );
                      if(this.fileName.includes('VSD_CHOT')) {
                        const data = {
                          reportDate: this.getDateFileImport(this.fileName)
                        }
                        this._shareholderService.warningataEvent(data).pipe(
                          takeUntil(this.ngUnsubscribe),
                          finalize(() => this.indicator.hideActivityIndicator())
                        ).subscribe((res: any) => {
                          if (res && res.code === Status.SUCCESS) {
                            if(res?.data?.length) {
                              this.dialogService.componentDialog(TableWarningComponent, {
                                width: '70%',
                                height: '93%',
                                data: {
                                  data: res?.data
                                }
                              }, (res) => {
                              });
                            }
                          }
                          })
                        }
                    } else {
                      const message = res?.data?.note;
                      const requestId = res?.requestId;
                      this.dialogService.error({
                        title: 'Thông báo',
                        innerHTML: requestId ? `${message} <br> ${requestId}` : `${message}`
                      }, dialog => {
                        if (dialog) {
                        }
                      });
                    }
                  })
                }
              }
            });  
          } else {
            const message = error.error && error.error?.msg ? error.error?.msg : 'Có lỗi xảy ra. Vui lòng liên hệ để được hỗ trợ.';
            const requestId = error.error && error.error?.requestId ? error.error?.requestId : null;
            this.dialogService.error({
              title: 'Thông báo',
              innerHTML: requestId ? `${message} <br> ${requestId}` : `${message}`
            }, dialog => {
              if (dialog) {
              }
            });
          }
        })
      }
    }
    this.myInputVariable.nativeElement.value = "";
  }


  dragFileInput(file) {
    if (file && file.length > 1) {
      this.dialogService.error({
        title: 'dialog.notification',
        innerHTML: 'Chỉ cho phép chọn một file định dạng xlsx',
      }, res => { if (res) { } });
      return;
    }
    if (file && file.length == 1) {
      // Kiểm tra định dang file
      if (file && [EXCEL_XLS, EXCEL_XLSX].findIndex(f => f === file[0].type) < 0) {
        this.dialogService.error({
          title: 'dialog.notification',
          message: `Sai định dạng file`,
        }, (result: any) => { if (result) { } });
        return;
      }
      this.selectedFiles = file[0];
      this.fileName = this.selectedFiles.name;
      const formData = new FormData();
      if (this.selectedFiles) {
        this.indicator.showActivityIndicator();
        formData.append('files', this.selectedFiles, this.fileName);
        this._shareholderService.importValidateEvent(formData).pipe(
          map((res: HttpResponse) => {
            if(res.code == Status.SUCCESS) {
              return res?.data;
            }
          }),
          mergeMap(param => this._shareholderService.importDataEvent({...formData, isWarning: 0 })),
          takeUntil(this.ngUnsubscribe),
          finalize(() => this.indicator.hideActivityIndicator())
        ).subscribe((res: any) => {
          if (res && res.code === Status.SUCCESS && !res?.data?.note) {
            this.toastr.showToastr(
              `Tải file thành công`,
              'Thông báo!',
              MessageSeverity.success,
            );
            if(this.fileName.includes('VSD_CHOT')) {
              const data = {
                reportDate: this.getDateFileImport(this.fileName)
              }
              this._shareholderService.warningataEvent(data).pipe(
                takeUntil(this.ngUnsubscribe),
                finalize(() => this.indicator.hideActivityIndicator())
              ).subscribe((res: any) => {
                if (res && res.code === Status.SUCCESS) {
                  if(res?.data?.length) {
                    this.toastr.showToastr(
                      `Cảnh báo thay đổi`,
                      'Thông báo!',
                      MessageSeverity.warning,
                    );
                  }
                }
                })
              }
          } else {
            const message = res?.data?.note;
            const requestId = res?.requestId;
            this.dialogService.error({
              title: 'Thông báo',
              innerHTML: requestId ? `${message} <br> ${requestId}` : `${message}`
            }, dialog => {
              if (dialog) {
              }
            });
          }
        }, (error: any) => {
          if(error?.error?.code == Status.EXIST) {
            this.dialogService.dformconfirm({
              title: 'Thông báo',
              innerHTML: `File đã tồn tại trên hệ thống <br> Bạn có muốn tiếp tục upload file?`,
              closeBtn: 'Hủy bỏ',
              acceptBtn: 'Xác nhận',
            }, (result: any) => {
              if (result) {
                if(result.status == 2) {
                  this.indicator.showActivityIndicator();
                  formData.append('isWarning', '1');
                  this._shareholderService.importDataEvent(formData).pipe(
                    takeUntil(this.ngUnsubscribe),
                    finalize(() => this.indicator.hideActivityIndicator())
                  ).subscribe((res: any) => {
                    if (res && res.code === Status.SUCCESS && !res?.data?.note) {
                      this.toastr.showToastr(
                        `Tải file thành công`,
                        'Thông báo!',
                        MessageSeverity.success,
                      );
                      if(this.fileName.includes('VSD_CHOT')) {
                        const data = {
                          reportDate: this.getDateFileImport(this.fileName)
                        }
                        this._shareholderService.warningataEvent(data).pipe(
                          takeUntil(this.ngUnsubscribe),
                          finalize(() => this.indicator.hideActivityIndicator())
                        ).subscribe((res: any) => {
                          if (res && res.code === Status.SUCCESS) {
                            if(res?.data?.length) {
                              this.toastr.showToastr(
                                `Cảnh báo thay đổi`,
                                'Thông báo!',
                                MessageSeverity.warning,
                              );
                            }
                          }
                          })
                        }
                    } else {
                      const message = res?.data?.note;
                      const requestId = res?.requestId;
                      this.dialogService.error({
                        title: 'Thông báo',
                        innerHTML: requestId ? `${message} <br> ${requestId}` : `${message}`
                      }, dialog => {
                        if (dialog) {
                        }
                      });
                    }
                    })
                  }
              }
            });  
          } else {
            const message = error.error && error.error?.msg ? error.error?.msg : 'Có lỗi xảy ra. Vui lòng liên hệ để được hỗ trợ.';
            const requestId = error.error && error.error?.requestId ? error.error?.requestId : null;
            this.dialogService.error({
              title: 'Thông báo',
              innerHTML: requestId ? `${message} <br> ${requestId}` : `${message}`
            }, dialog => {
              if (dialog) {
              }
            });
          }
        })
      }
    }
    this.myInputVariable.nativeElement.value = "";
  }

  getDateFileImport(fileName: string) {
    const name = fileName.split('.')[1]
    const date = name.slice(name.length - 8, name.length)
    return date.slice(0, 4) + '-' + date.slice(4, 6) + '-' + date.slice(6);
  }

}