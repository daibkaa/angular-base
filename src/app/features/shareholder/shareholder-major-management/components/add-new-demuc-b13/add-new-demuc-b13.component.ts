import { ChangeDetectorRef, Component, Inject, Injector } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { ComponentAbstract } from '@shared-sm';
import * as moment from 'moment';
import { PARENTID, STT, CDLON, NGUOINOIBO, CHUTHICHNGUOINOIBO, NGAY_HIEU_LUC } from '../../form-setting/demuc-b13';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-add-new-demuc-b13',
  templateUrl: './add-new-demuc-b13.component.html',
  styleUrls: ['./add-new-demuc-b13.component.scss']
})
export class AddNewDeMucB13Component extends ComponentAbstract {
  
  PARENTID = PARENTID();
  STT = STT();
  CDLON = CDLON();
  NGUOINOIBO = NGUOINOIBO();
  CHUTHICHNGUOINOIBO= CHUTHICHNGUOINOIBO();
  NGAY_HIEU_LUC = NGAY_HIEU_LUC();

  listDemuc: Array<any> = [];
  showListDemuc: boolean = false;
  listNNB: Array<any> = [];
  showListNNB: boolean = false;

  constructor(
    protected injector: Injector,
    public dialogRef: MatDialogRef<AddNewDeMucB13Component>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {
    super(injector);
  }
  checked = false;

  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.PARENTID, this.STT, this.CDLON]);

    this.PARENTID.options = ([...this.data.listData] || []).filter(f => !f.parentId).map(m => {
      return {
        key: m.id,
        value: m.cdLon
      }
    });
    if(this.data?.data?.id) {
      this.form.patchValue(this.data.data);
      this.form.get(this.PARENTID.key).disable();
      this.listDemuc = ([...this.data.listData] || []).filter(f => (f.parentId == this.data.data?.parentId) && !f.deMuc && !f.soDksh)
    }

    // this.onChangeParentId();
  }

  onChangeParentId() {
    this.form.get(this.PARENTID.key).valueChanges.pipe(
      takeUntil(this.ngUnsubscribe),
      distinctUntilChanged()).subscribe(value => {
        if(value) {
          this.listDemuc = ([...this.data.listData] || []).filter(f => (f.parentId == value) && !f.deMuc && !f.soDksh)
        } else {
          this.listDemuc = [];
        }
        
      })
  }

  onSelect(item) {
    this.form.get(this.STT.key).setValue(item.stt)
    this.form.get(this.CDLON.key).setValue(item.cdLon)
  }

  focusEvent($event) {
    this.showListDemuc = true;
  }

  focusOutEvent($event) {
    setTimeout(() => {
      this.showListDemuc = false;
      const item = ([...this.listDemuc] || []).find(f => f.stt == this.form.get(this.STT.key).value);
      if(item) {
        this.form.get(this.CDLON.key).setValue(item.cdLon)
      } else {
        // do something
      } 
    }, 200)
  }

  saveData() {
    this.validateAllFields(this.form);
    if (!this.form.valid) {
      this.dialogService.error({
        title: 'dialog.notification',
        message: 'dialog.valid-error',
      }, res => {
        if (res) {
        }
      });
      return;
    }

    
    const formData = this.form.getRawValue()
    let paramDialog = {
      data: {
        ...formData,
        id: this.data.data?.id || null
      },
    }
    this.dialogRef.close(paramDialog)

  }

  closeDialog() {
    if (this.dialogRef.close) { this.dialogRef.close(null); }
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

}
