export const D_NUMBER_PHONE = 'numberPhone';
export const D_FREE = 'textFree';
export const D_FREE_TEXT = 'dtextFree';
export const D_TEXT = 'text';
export const D_CURRENCY = 'currency';
export const URL_BASE = '';
export const GENERATE_TOKEN = 'generate-token';
export const GENERATE_OTP = 'save-otp-info';
export const REVOKE_TOKEN = 'revoke-token';
export const VERIFYOTP = 'verify-otp';

export const MAKER = 'MAKER'; // Role nhập liệu
export const APPROVER = 'APPROVER'; // Role duyệt

export const STATUS = [
    { key: '', value: 'Tất cả trạng thái', class: '' },
    { key: 'E', value: 'Đang xử lý', class: 'wf-status-inprocess' },
    { key: 'W', value: 'Chờ duyệt', class: 'wf-status-waitting' },
    { key: 'A', value: 'Đã duyệt', class: 'wf-status-approved' },
    { key: 'U', value: 'Yêu cầu bổ sung', class: 'wf-status-inprocess' },
    { key: 'P', value: 'Duyệt một phần', class: 'wf-status-halfapprove' },
    { key: 'F', value: 'Duyệt thất bại', class: 'wf-status-halfapprove' },
    { key: 'R', value: 'Từ chối duyệt', class: 'wf-status-reject' },
    { key: 'D', value: 'Hủy', class: 'wf-status-reject' },
];
