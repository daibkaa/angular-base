import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { SharedSMModule } from '@shared-sm';
import { MaterialModule } from '../shared/material.module';
import { ActivityIndicatorModule } from './../shared/activity-indicator/activityIndicator.module';
import { AtiLayoutComponent } from './ati-layout/ati-layout.component';
import { HeaderComponent } from './header/header.component';
import { UserComponent } from './header/widgets/user.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { UserPanelComponent } from './sidebar/user-panel.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';

@NgModule({
  declarations: [
    AtiLayoutComponent,
    SidebarComponent,
    UserPanelComponent,
    SidemenuComponent,
    HeaderComponent,
    UserComponent,
    PageHeaderComponent
  ],
  imports: [
    CommonModule, 
    RouterModule,
    ActivityIndicatorModule,
    TranslateModule,
    MatMenuModule,
    MatIconModule,
    MatListModule,
    MatSidenavModule,
    MatToolbarModule,
    MatTabsModule,
    MatRippleModule,
    MatTooltipModule,
    MatBadgeModule,
    MatButtonModule,
    MatButtonToggleModule,
    FlexLayoutModule,
    SharedSMModule,
    MaterialModule
  ],
})
export class ThemeModule { }
