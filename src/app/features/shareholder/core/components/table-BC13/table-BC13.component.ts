import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { COLUMN_DEFAULT } from '@core';
import { DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import { REJECT_COLUMN_DISPLAYS } from '../../constant';
import { ShareholderService } from '../../services/shareholder.service';

@Component({
  selector: 'app-table-BC13',
  templateUrl: './table-BC13.component.html',
  styleUrls: ['./table-BC13.component.scss'],
})
export class TableBC13Component implements OnInit {

  @Input() dataSource: MatTableDataSource<any>;
  @Input() hasDataSource = false;
  displayedColumns: string[] = ['stt', 'hoTen', 'soDksh', 'ngayCap', 'diaChiVsd', 'loaiCk', 'ngayPhatSinh', 'noiDung', 'nhap', 'xuat', 'action'];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;
  @Output() exportExcel = new EventEmitter();

  readonly REJECT_COLUMN_DISPLAYS = REJECT_COLUMN_DISPLAYS;

  constructor(
    public _shareholderService: ShareholderService,
  ) {
  }

  // Xử lý dữ liệu đầu vào
  ngOnInit(): void {}

  destroyData() {
  }

  onExport(element, index) {
    this.exportExcel.emit(element)
  }

}