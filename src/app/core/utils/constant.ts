import { environment } from '@env/environment';

export const URL_BASE = environment.urlAuth;
export const URL_LOGIN = environment.urlAuth;

export enum LocalStoreEnum {
  Token = 'JWT',
  RefreshToken = 'RJWT',
  User_Infor = 'UI',
  Time_Expiration = 'TE',
  Date_Expiration = 'DATE',
  Key_Reload = 'Key_Reload',
}

export const Status = {
  SUCCESS: 0,
  EXIST: 101,
  ERROR: 500,
};

export const PATH = {
  GENERATE_TOKEN: 'generate-token',
  REVOKE_TOKEN: 'revoke-token',

  IMPORT: {
    IMPORT_VALIDATE: 'import',
    IMPORT_DATA: 'import',
    WARNING: 'warning',
    DOWNLOAD: 'download-file-import'
  },

  URL_CATEGORY: {
    CATEGORY: 'category'
  },

  SHARE_HOLDER_MANAGEMENT: {
    INFO_REPORT: 'ttcd/report',
    INFO_MANAGEMENT: 'ttcd/info',
    EXPORT_REPORT: 'ttcd/report/export',
    EXPORT_INFO: 'ttcd/info/export'
  },
  
  SHARE_HOLDER_MAJOR_MANAGEMENT: {
    INFO_REPORT: 'nlq/report',
    INFO_MANAGEMENT: 'nlq/info',
    INSEART_OR_UPDATE: 'nlq/info/nlq-cd-lon',
    DELETE: 'nlq/info/nlq-cd-lon',
    EXPORT: 'nlq/report/export',
    TRANSACTION_CHANGE: 'nlq/transaction/change',
    STOCK_MBB: 'nlq/stock-mbb',
    INSERT_OR_UPDATE_T22: 'nlq/info/nnb-nlq-tt-22',
    INSERT_OR_UPDATE_LUAT_TCTD: 'nlq/info/nnb-nlq-luat-tctd',
    INSERT_OR_UPDATE_LUAT_CK: 'nlq/info/nnb-nlq-luat-ck',
    INFO_MANAGER: 'nlq/raw-info',
    B13_CDL:'nlq/raw-info/nlq-cd-lon',
    B14_T22: 'nlq/raw-info/nnb-nlq-tt-22',
    B15_LUAT_CK: 'nlq/raw-info/nnb-nlq-luat-ck',
    B16_LUAT_TCTD: 'nlq/raw-info/nnb-nlq-luat-tctd',
    INFO_EXPORT: 'nlq/info/export',
    CHI_MUC: 'nlq/raw-info/chi-muc',
    EXPORT_TRANSACTION_CHANGE: 'nlq/transaction/change/export'
  },

  SHARE_HOLDER_CONGRESS: {
    INFO_REPORT: 'dhcd/search',
    EXPORT: 'dhcd/exportFile',
    EXPORT_INFO: 'dhcd/export/c5',
    QUERY_INFO: 'dhcd/truy-van-thong-tin'
  },

  EXPORT: {
    EXPORT_EXCEL: 'saoKe/export',
    SEARCH: 'saoKe/search'
  }
};

export const URL_GET_ALL = [
  { key: 'CATEGORY', value: 'listCategory', url: URL_BASE },
];

export const MENU_LIST = [
  {
    "id": 1,
    "route": "ati/import",
    "name": "Import dữ liệu",
    "parentId": 0,
    "position": 3,
    "active": true,
    "activeService": false,
    "type": "FUNCTION",
    "typeLink": "angularLink",
    "icon": "group_work"
  },
  {
    "id": 2,
    "route": "ati/shareholder-management",
    "name": "Quản lý cổ đông",
    "parentId": 0,
    "position": 3,
    "active": true,
    "activeService": false,
    "type": "FUNCTION",
    "typeLink": "angularLink",
    "icon": "group_work"
  },
  {
    "id": 3,
    "route": "ati/shareholder-major-management",
    "name": "Cổ đông lớn, NNB & NLQ",
    "parentId": 0,
    "position": 3,
    "active": true,
    "activeService": false,
    "type": "FUNCTION",
    "typeLink": "angularLink",
    "icon": "donut_large"
  },
  {
    "id": 4,
    "route": "ati/shareholder-congress",
    "name": "Đại hội cổ đông",
    "parentId": 0,
    "position": 3,
    "active": true,
    "activeService": false,
    "type": "FUNCTION",
    "typeLink": "angularLink",
    "icon": "location_city"
  },
  {
    "id": 5,
    "route": "ati/copy-share-dividend",
    "name": "Sao kê cổ phiếu, cổ tức",
    "parentId": 0,
    "position": 3,
    "active": true,
    "activeService": false,
    "type": "FUNCTION",
    "typeLink": "angularLink",
    "icon": "copyright"
  },
  {
    "id": 5,
    "route": "ati/import-info",
    "name": "quản lý import",
    "parentId": 0,
    "position": 3,
    "active": true,
    "activeService": false,
    "type": "FUNCTION",
    "typeLink": "angularLink",
    "icon": "copyright"
  },
];

export const GENERATE_TOKEN = 'generate-token';
export const REVOKE_TOKEN = 'revoke-token';

export const EXCEL_XLS = 'application/vnd.ms-excel';
export const EXCEL_XLSX = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

/**
 * Tổng hợp url kiểm tra bỏ qua timeout
 */
export const URL_TIMEOUT = [
  PATH.IMPORT.IMPORT_DATA,
  PATH.IMPORT.DOWNLOAD,
  PATH.SHARE_HOLDER_MANAGEMENT.EXPORT_INFO,
  PATH.SHARE_HOLDER_MANAGEMENT.EXPORT_REPORT,
  PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.EXPORT,
  PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.INFO_EXPORT,
  PATH.SHARE_HOLDER_MAJOR_MANAGEMENT.EXPORT_TRANSACTION_CHANGE,
  PATH.SHARE_HOLDER_CONGRESS.EXPORT,
  PATH.SHARE_HOLDER_CONGRESS.EXPORT_INFO,
  PATH.EXPORT.EXPORT_EXCEL,
]

/**
 * Tổng hợp url kiểm tra bỏ qua show message error
 */
export const URL_ERROR = [
  'api/import'
];

export const BLANK_STRING = '';

export const KEY_VALUE_COLUMN = {
  'no': 'STT', 
  'hoTen': 'Họ tên', 
  'soDksh': 'Số DKSH', 
  'ngayCap': 'Ngày cấp', 
  'diaChiVsd': 'Địa chỉ VSD', 
  'dienThoaiVsd': 'Điện thoại VSD', 
  'emailVsd': 'Email VSD', 
  'chuaLk': 'Chưa LK', 
  'daLk': 'Đã LK', 
  'tongCp': 'Tổng CP', 
  'tyLe': 'Tỷ lệ(%)', 
  'ptClk': 'PT CLK', 
  'ptDlk': 'PT DLK', 
  'tongPt': 'Tổng PT', 
  'cntc': 'CNTC', 
  'tnnn': 'TNNN', 
  'quocTich': 'Quốc tịch',
  'chuThichDeMuc': "Chú thích đề mục",
  'chuThichNguoiNoiBo': "Chú thích người nội bộ",
  'chucVuMoiQh': "Chức vụ/ Mối QH",
  'deMuc': "Đề mục",
  'diaChi': "Địa chỉ",
  'dienThoai': "Điện thoại",
  'email': "Email",
  'gdTrongThang': "GD trong tháng",
  'gioiTinh': "Giới tính",
  'loaiHinhId': "Loại hình",
  'nguoiNoiBo': "Người nội bộ",
  'noiCap': "Nơi cấp",
  'soLuong': "Số lượng",
  'tenTcCn': "Tên TCCN",
  'tkgdck': "tkgdck",
  'soLuongCoDong': 'Số lượng cổ đông', 
  'coPhanSoHuu': 'Cổ phần sở hữu', 
  'tyLeSoHuu': 'Tỷ lệ sở hữu',
  'stt': 'STT',
  'nhomCoDong': 'Nhóm cổ đông',
  'cpDaiDien': 'CP Đại Diện',
  'cpThamDu': 'CP tham dự',
  'phieuBQ': 'Phiếu BQ',
  'diaChiMb': 'Địa chỉ MB',
  'soDtMbs': 'Số ĐT MBS',
  'dienThoaiMb': 'Điện thoại MB',
  'dienThoaiDc': 'Điện thoại DC',
  'emailDc': 'Email DC',
  'cdLon': 'Cổ đông lớn',
  'cdChienLuoc': 'Cổ đông chiến lược',
  'dnNhaNuoc': 'DN Nhà nước',
  'dnqd': 'DN QĐ',
  'cbnvMb': 'CBVNV MB',
  'nhomNdtnnLienQuan': 'Nhóm NDTNN liên quan',
  'dienThoaiMbs': 'Điện thoại MBS',
  'cotucClk': 'Cổ tức CLK',
  'cotucDlk': 'Cổ tức DLK',
  'tongCotuc': 'Tổng cổ tức',
  'thueChuaLk': 'Thuế chưa LK',
  'thueDaLk': 'Thuế đã LK',
  'tongThueTncn': 'Tổng thuế TNCN',
  'cotucClkSauthue': 'Cổ tức CLK sau thuế',
  'cotucDlkSauthue': 'Cổ tức DLK sau thuế',
  'tongCotucSauthue': 'Tổng cổ tức sau thuê',
  'nhomNdtnn': 'Nhóm NDTNN',
  'tyLeTraCotuc': 'Tỷ lệ trả cổ tức',
  'loaiCk': 'Loai CK',
  'ngayPhatSinh': 'Ngày phát sinh',
  'noiDung': 'Nội dung',
  'nhap': 'Nhập',
  'xuat': 'Xuất',
  'tenNguoiDaiDienVon': 'Tên người đại diện vốn',
  'chucVuTaiDonVi': 'Chức vụ tại đơn vị',
  'coPhan': 'Cổ phần',
  'cdSangLap': 'CĐ sáng lập',
  'nhomCD': 'Nhóm cổ đông'
}

export const COLUMN_DEFAULT = ['no', 'hoTen', 'soDksh', 'ngayCap', 'diaChiVsd', 'dienThoaiVsd', 'emailVsd', 'chuaLk', 'daLk', 'tongCp', 'tyLe', 'ptClk', 'ptDlk', 'tongPt', 'cntc', 'tnnn', 'quocTich']

export const BC_TYPE = {
  MBB_TONG_HOP: 'MBB_TONG_HOP',
  GDLON: 'GDLON',
  MBB_COTUC_TM: 'MBB_COTUC_TM',
  MBB_PHAT_HANH: 'MBB_PHAT_HANH',
  CO_CAU_CD: 'CO_CAU_CD',
  CA_NHAN: 'CA_NHAN',
  TO_CHUC: 'TO_CHUC',
  CO_DONG_LON: 'CO_DONG_LON',
  CO_DONG_CHIEN_LUOC: 'CO_DONG_CHIEN_LUOC',
  CBNV: 'CBNV',
  CO_DONG_CO_GIAO_DICH_LON: 'CO_DONG_CO_GIAO_DICH_LON',
  NNB_NLQ_TT22: 'NNB_NLQ_TT22',
  NNB_NLQ_LUAT_TCTD: 'NNB_NLQ_LUAT_TCTD',
  NNB_NLQ_LUAT_CK: 'NNB_NLQ_LUAT_CK',
  CDLON_NLQ: 'CDLON_NLQ',
  TT22: 'TT22',
  LUAT_TCTD: 'LUAT_TCTD',
  LUAT_CK: 'LUAT_CK',
  BC5: 'BC5',
  BC6: 'BC6',
  BC7: 'BC7',
  BC8: 'BC8',
  CD_LON: 'CD_LON'
}