import { DateTimeItem, DropdownItem, NgSelectItem, TextboxItem } from "@shared-sm";
import * as moment from "moment";

export const CREATED_BY = () => new TextboxItem({
    key: 'createdBy',
    label: 'Người import',
    placeholder: 'Người import',
    value: '',
    layout: '50',
    directives: ''
});

export const FILE_NAME = () => new TextboxItem({
    key: 'fileName',
    label: 'Tên file import',
    placeholder: 'Tên file import',
    value: '',  
    layout: '50',
    directives: ''
});

export const IMPORT_DATE_FROM = () => new DateTimeItem({
    key: 'importDateFrom',
    placeholder: 'Từ ngày',
    label: 'Từ ngày',
    value: moment().format('YYYY-MM-DD'),
    layout: '50',
    minDate: '1900-01-01',
    required: true
});

export const IMPORT_DATE_TO = () => new DateTimeItem({
    key: 'importDateTo',
    placeholder: 'Đến ngày',
    label: 'Đến ngày',
    value: moment().format('YYYY-MM-DD'),
    layout: '50',
    minDate: '1900-01-01',
    required: true
});

export const GROUP_TYPE = () => new NgSelectItem({
    key: 'isWarning',
    placeholder: 'Cảnh báo import',
    label: 'Cảnh báo import',
    value: '',
    layout: '50',
    options: [
       {
            key: '0',
            value: 'Thành công'
       },
       {
        key: '1',
        value: 'Cảnh báo'
        }
    ]
});