export const DATA_BC8 = 
[
  {
    "ids": 0,
    "stt": null,
    "nhomCoDong": "Tổng cộng",
    "soDksh": null,
    "cpDaiDien": 1000000000,
    "cpThamDu": 780811984,
    "tyLeThamDu": 100,
    "vanDeDtoList": []
  },
  {
    "ids": 1,
    "stt": null,
    "nhomCoDong": "Đồng ý",
    "soDksh": null,
    "cpDaiDien": null,
    "cpThamDu": null,
    "tyLeThamDu": null,
    "vanDeDtoList": [
      {
        "phieuBQ": 23028844,
        "tyLe": 2.9499999999999997,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 353903161,
        "tyLe": 45.339999999999996,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 26899701,
        "tyLe": 3.44,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 487404895,
        "tyLe": 62.44,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 245038643,
        "tyLe": 31.39,
        "loaiVd": "Vấn đề 5"
      }
    ]
  },
  {
    "ids": 2,
    "stt": "I.1",
    "nhomCoDong": "Cổ đông sở hữu lớn",
    "soDksh": null,
    "cpDaiDien": 465336284,
    "cpThamDu": 465336284,
    "tyLeThamDu": null,
    "vanDeDtoList": [
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 350584700,
        "tyLe": 44.91,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 465336284,
        "tyLe": 59.61,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 231693584,
        "tyLe": 29.68,
        "loaiVd": "Vấn đề 5"
      }
    ]
  },
  {
    "ids": 3,
    "stt": "1.1",
    "nhomCoDong": "Viettel",
    "soDksh": "0100109106",
    "cpDaiDien": 120618700,
    "cpThamDu": 120618700,
    "tyLeThamDu": 25.92,
    "vanDeDtoList": [
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 120618700,
        "tyLe": 15.45,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 120618700,
        "tyLe": 15.45,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 5"
      }
    ]
  },
  {
    "ids": 4,
    "stt": "1.2",
    "nhomCoDong": "CN Cty trực thăng",
    "soDksh": "0100107966-006",
    "cpDaiDien": 50668083,
    "cpThamDu": 50668083,
    "tyLeThamDu": 10.89,
    "vanDeDtoList": [
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 50668083,
        "tyLe": 6.49,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 50668083,
        "tyLe": 6.49,
        "loaiVd": "Vấn đề 5"
      }
    ]
  },
  {
    "ids": 5,
    "stt": "1.3",
    "nhomCoDong": "TCT Tân cảng",
    "soDksh": "0300514849",
    "cpDaiDien": 64083501,
    "cpThamDu": 64083501,
    "tyLeThamDu": 13.77,
    "vanDeDtoList": [
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 64083501,
        "tyLe": 8.21,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 64083501,
        "tyLe": 8.21,
        "loaiVd": "Vấn đề 5"
      }
    ]
  },
  {
    "ids": 6,
    "stt": "1.4",
    "nhomCoDong": "TCT Trực thăng",
    "soDksh": "0100107966",
    "cpDaiDien": 116942000,
    "cpThamDu": 116942000,
    "tyLeThamDu": 25.13,
    "vanDeDtoList": [
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 116942000,
        "tyLe": 14.98,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 116942000,
        "tyLe": 14.98,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 116942000,
        "tyLe": 14.98,
        "loaiVd": "Vấn đề 5"
      }
    ]
  },
  {
    "ids": 7,
    "stt": "1.5",
    "nhomCoDong": "SCIC",
    "soDksh": "0101992921",
    "cpDaiDien": 113024000,
    "cpThamDu": 113024000,
    "tyLeThamDu": 24.29,
    "vanDeDtoList": [
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 113024000,
        "tyLe": 14.48,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 113024000,
        "tyLe": 14.48,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 5"
      }
    ]
  },
  {
    "ids": 8,
    "stt": "I.2",
    "nhomCoDong": "Cán bộ Nhân viên",
    "soDksh": "01073002963",
    "cpDaiDien": 22383591,
    "cpThamDu": 22383591,
    "tyLeThamDu": 4.81,
    "vanDeDtoList": [
      {
        "phieuBQ": 21536230,
        "tyLe": 2.76,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 661532,
        "tyLe": 0.08,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 21536230,
        "tyLe": 2.76,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 21536230,
        "tyLe": 2.76,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 5"
      }
    ]
  },
  {
    "ids": 9,
    "stt": "I.3",
    "nhomCoDong": "Công đoàn",
    "soDksh": "65/QĐ/TVĐU",
    "cpDaiDien": 365019,
    "cpThamDu": 365019,
    "tyLeThamDu": 0.08,
    "vanDeDtoList": [
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 365019,
        "tyLe": 0.05,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 365019,
        "tyLe": 0.05,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 5"
      }
    ]
  },
  {
    "ids": 10,
    "stt": "I.4",
    "nhomCoDong": "CĐ trong nước khác",
    "soDksh": null,
    "cpDaiDien": 416664475,
    "cpThamDu": 197826210,
    "tyLeThamDu": 42.51,
    "vanDeDtoList": [
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 5"
      }
    ]
  },
  {
    "ids": 11,
    "stt": "I.5",
    "nhomCoDong": "CĐ nước ngoài",
    "soDksh": null,
    "cpDaiDien": 95250631,
    "cpThamDu": 94900880,
    "tyLeThamDu": 20.39,
    "vanDeDtoList": [
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 167362,
        "tyLe": 0.02,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 167362,
        "tyLe": 0.02,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 5"
      }
    ]
  },
  {
    "ids": 12,
    "stt": "II",
    "nhomCoDong": "Không đồng ý",
    "soDksh": null,
    "cpDaiDien": null,
    "cpThamDu": null,
    "tyLeThamDu": null,
    "vanDeDtoList": [
      {
        "phieuBQ": 1080729,
        "tyLe": 0.13999999999999999,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 1147074,
        "tyLe": 0.15,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 354478,
        "tyLe": 0.04,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 12200223,
        "tyLe": 1.56,
        "loaiVd": "Vấn đề 5"
      }
    ]
  },
  {
    "ids": 13,
    "stt": "II.1",
    "nhomCoDong": "CĐ trong nước",
    "soDksh": null,
    "cpDaiDien": null,
    "cpThamDu": null,
    "tyLeThamDu": null,
    "vanDeDtoList": [
      {
        "phieuBQ": 913367,
        "tyLe": 0.12,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 1147074,
        "tyLe": 0.15,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 187116,
        "tyLe": 0.02,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 12032861,
        "tyLe": 1.54,
        "loaiVd": "Vấn đề 5"
      },
      {
        "phieuBQ": 167362,
        "tyLe": 0.02,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 167362,
        "tyLe": 0.02,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 167362,
        "tyLe": 0.02,
        "loaiVd": "Vấn đề 5"
      }
    ]
  },
  {
    "ids": 14,
    "stt": "II.2",
    "nhomCoDong": "CĐ nước ngoài",
    "soDksh": null,
    "cpDaiDien": null,
    "cpThamDu": null,
    "tyLeThamDu": null,
    "vanDeDtoList": [
      {
        "phieuBQ": 167362,
        "tyLe": 0.02,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 167362,
        "tyLe": 0.02,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 167362,
        "tyLe": 0.02,
        "loaiVd": "Vấn đề 5"
      }
    ]
  },
  {
    "ids": 15,
    "stt": "III",
    "nhomCoDong": "Không ý kiến",
    "soDksh": null,
    "cpDaiDien": null,
    "cpThamDu": null,
    "tyLeThamDu": null,
    "vanDeDtoList": [
      {
        "phieuBQ": 244523,
        "tyLe": 0.03,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 977474,
        "tyLe": 0.13,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 4841631,
        "tyLe": 0.62,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 977474,
        "tyLe": 0.13,
        "loaiVd": "Vấn đề 5"
      }
    ]
  },
  {
    "ids": 16,
    "stt": "III.1",
    "nhomCoDong": "CĐ trong nước",
    "soDksh": null,
    "cpDaiDien": null,
    "cpThamDu": null,
    "tyLeThamDu": null,
    "vanDeDtoList": [
      {
        "phieuBQ": 244523,
        "tyLe": 0.03,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 977474,
        "tyLe": 0.13,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 4841631,
        "tyLe": 0.62,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 977474,
        "tyLe": 0.13,
        "loaiVd": "Vấn đề 5"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 5"
      }
    ]
  },
  {
    "ids": 17,
    "stt": "III.2",
    "nhomCoDong": "CĐ nước ngoài",
    "soDksh": null,
    "cpDaiDien": null,
    "cpThamDu": null,
    "tyLeThamDu": null,
    "vanDeDtoList": [
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 1"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 2"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 3"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 4"
      },
      {
        "phieuBQ": 0,
        "tyLe": 0,
        "loaiVd": "Vấn đề 5"
      }
    ]
  }
]

export const DATA_BC7 = 
[
    {
      "id": 0,
      "stt": "1",
      "nhomCD": "CĐ trong nước",
      "soCdAfter": 55,
      "soLuongCpAfter": 884446131,
      "tyLeAfter": 73.77000000000001,
      "soCdNow": 48,
      "soLuongCpNow": 685911104,
      "tyLeNow": 68.59,
      "chenhLechTyLe": -5.18
    },
    {
      "id": 1,
      "stt": "1.1",
      "nhomCD": "CĐ Cá nhân",
      "soCdAfter": 26,
      "soLuongCpAfter": 79728101,
      "tyLeAfter": 6.65,
      "soCdNow": 20,
      "soLuongCpNow": 71305238,
      "tyLeNow": 7.13,
      "chenhLechTyLe": 0.48
    },
    {
      "id": 2,
      "stt": "1.2",
      "nhomCD": "CĐ Tổ chức",
      "soCdAfter": 29,
      "soLuongCpAfter": 804718030,
      "tyLeAfter": 67.12,
      "soCdNow": 28,
      "soLuongCpNow": 614605866,
      "tyLeNow": 61.46,
      "chenhLechTyLe": -5.66
    },
    {
      "id": 3,
      "stt": "2",
      "nhomCD": "CĐ nước ngoài",
      "soCdAfter": 22,
      "soLuongCpAfter": 113625096,
      "tyLeAfter": 9.47,
      "soCdNow": 16,
      "soLuongCpNow": 94900880,
      "tyLeNow": 9.49,
      "chenhLechTyLe": 0.02
    },
    {
      "id": 4,
      "stt": "2.1",
      "nhomCD": "CĐ Cá nhân",
      "soCdAfter": 8,
      "soLuongCpAfter": 62739459,
      "tyLeAfter": 5.23,
      "soCdNow": 6,
      "soLuongCpNow": 52114045,
      "tyLeNow": 5.21,
      "chenhLechTyLe": -0.02
    },
    {
      "id": 5,
      "stt": "2.2",
      "nhomCD": "CĐ Tổ chức",
      "soCdAfter": 14,
      "soLuongCpAfter": 50885637,
      "tyLeAfter": 4.24,
      "soCdNow": 10,
      "soLuongCpNow": 42786835,
      "tyLeNow": 4.28,
      "chenhLechTyLe": 0.04
    },
    {
      "id": 6,
      "stt": null,
      "nhomCD": "Tổng cộng",
      "soCdAfter": 77,
      "soLuongCpAfter": 998071227,
      "tyLeAfter": 83.24000000000001,
      "soCdNow": 64,
      "soLuongCpNow": 780811984,
      "tyLeNow": 78.08,
      "chenhLechTyLe": -5.16
    }
  ]