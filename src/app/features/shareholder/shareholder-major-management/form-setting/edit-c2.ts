import { DateTimeItem, TextboxItem } from "@shared-sm";

export const TENTCCN = () => new TextboxItem({
    key: 'tenTcCn',
    placeholder: 'tenTcCn',
    label: 'Tên TCCN',
    value: '',
    layout: '50',
    required: true,
    readOnly: true
});

export const SODKSH = () => new TextboxItem({
    key: 'soDksh',
    placeholder: '',
    label: 'Số DKSH',
    value: '',
    layout: '50',
    required: true,
    readOnly: true
});

export const BATDAU = () => new DateTimeItem({
    key: 'batDau',
    placeholder: '',
    label: 'Bắt đầu',
    value: '',
    layout: '50',
});

export const KETTHUC = () => new DateTimeItem({
    key: 'ketThuc',
    placeholder: '',
    label: 'Kết thúc',
    value: '',
    layout: '50',
});

export const SOLUONGDKGD = () => new TextboxItem({
    key: 'soLuongDkGd',
    placeholder: '',
    label: 'Số lượng DKGD',
    value: '',
    layout: '50',
    // required: true
});

export const THUCTEGD = () => new TextboxItem({
    key: 'thucTeGd',
    placeholder: '',
    label: 'Thực tế GD',
    value: '',
    layout: '50',
    // required: true
});

export const CBTTDKGD = () => new DateTimeItem({
    key: 'cbttDkGd',
    placeholder: '',
    label: 'CBTT DKGD',
    value: '',
    layout: '50',
});

export const CBTTKQGD = () => new DateTimeItem({
    key: 'cbttKqGd',
    placeholder: '',
    label: 'CBTT KQGD',
    value: '',
    layout: '50',
});

export const GIAITOAMBS = () => new DateTimeItem({
    key: 'giaiToaMbs',
    placeholder: '',
    label: 'Giai tỏa MBS',
    value: '',
    layout: '50',
});

export const EMAIL = () => new TextboxItem({
    key: 'email',
    placeholder: '',
    label: 'Email',
    value: '',
    layout: '50',
});

export const GHICHU = () => new DateTimeItem({
    key: 'ghiChu',
    placeholder: '',
    label: 'Ghi chú',
    value: '',
    layout: '50',
});