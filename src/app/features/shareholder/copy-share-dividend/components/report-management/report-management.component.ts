import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ComponentAbstract, ComponentBaseAbstract, DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize, map, mergeMap, takeUntil } from 'rxjs/operators';
import { EXCEL_XLS, EXCEL_XLSX, getBlobUrl, HttpResponse, Status } from 'src/app/core';
import { checkFileType, removeEmptyObject } from 'src/app/core/utils/utils.function';
import { REJECT_COLUMN_DISPLAYS } from '../../../core/constant';
import { ENDDATE, OWNER_REGISTRATIO_NUMBER, SAOKE, STARTDATE, TEN } from '../../form-setting/form-setting';
import { CopyShareDividendService } from '../../services/copy-share-dividend.service';

@Component({
  selector: 'app-report-management',
  templateUrl: './report-management.component.html',
  styleUrls: ['./report-management.component.scss'],
})
export class ReportManagementComponent extends ComponentAbstract {

  public dataSource: MatTableDataSource<any>;
  public hasDataSource = false;
  fullDataSource;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  $saoke = SAOKE();
  $ten = TEN();
  $ownerRegisterNumber = OWNER_REGISTRATIO_NUMBER();
  startDate = STARTDATE();
  endDate = ENDDATE();

  displayedColumns: string[];
  reportType;
  
  constructor(
    protected _injector: Injector,
    private _copyShareDividendService: CopyShareDividendService,
  ) {
    super(_injector);
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.$saoke, this.$ten, this.$ownerRegisterNumber, this.startDate, this.endDate]);
    this.searchReport();
  }

  destroyData() {
  }

  searchReport(): void {
    this.validateAllFields(this.form);
    if(!this.form.valid) {
      this.dialogService.error({
        title: 'Thông báo',
        innerHTML: 'Vui lòng nhập thông tin tìm kiếm'
      });
      return;
    }

    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue();
    let params = {...formData}
    if(formData?.startDate && formData?.startDate != '') {
      params.startDate = moment(formData.startDate).format('YYYY-MM-DD')
    } else {
      params.startDate = null;
    }
    if(formData?.endDate && formData?.endDate != '') {
      params.endDate = moment(formData.endDate).format('YYYY-MM-DD')
    } else {
      params.endDate = null;
    }
    this.reportType = formData.saoKe;
    this._copyShareDividendService.search(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
      if (res?.code === Status.SUCCESS) {
        if(res?.data?.length) {
          this.hasDataSource = true;
          this.fullDataSource = res.data
          this.totalItem = res.data.length
          // const data = res.data.sort((a,b) => Number(a.stt) - Number(b.stt))
          const data = res.data.map((obj, index) => { obj.stt = index + 1; return obj; });
          const newData = data.slice(Number(this.pageSize) * Number(this.pageIndex), Number(this.pageSize) * (Number(this.pageIndex) + 1))
          this.dataSource = new MatTableDataSource(newData);
        } else {
          this.totalItem = 0;
          this.hasDataSource = false;
          this.dataSource = new MatTableDataSource([]);
        }
      } else {
        this.totalItem = 0;
        this.hasDataSource = false;
        this.dataSource = new MatTableDataSource([]);
      }
    }, error => {
      this.totalItem = 0;
      this.hasDataSource = false;
      this.dataSource = new MatTableDataSource([]);
    })
  }

  exportExcel($event) {
    this.validateAllFields(this.form);
    if(!this.form.valid) {
      this.dialogService.error({
        title: 'Thông báo',
        innerHTML: 'Vui lòng nhập thông tin tìm kiếm'
      });
      return;
    }

    this.indicator.showActivityIndicator();
    let formData = this.form.getRawValue();
   
    let body = {
      ...formData,
      soDksh: $event?.soDksh,
      ten: $event?.hoTen,
    }
    if(formData?.startDate && formData?.startDate != '') {
      body.startDate = moment(formData.startDate).format('YYYY-MM-DD')
    } else {
      body.startDate = null;
    }
    if(formData?.endDate && formData?.endDate != '') {
      body.endDate = moment(formData.endDate).format('YYYY-MM-DD')
    } else {
      body.endDate = null;
    }
    this._copyShareDividendService.export(body).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
        // this.openPDF(res.body)
        const blob = new Blob([res.body], { type: 'application/vnd.ms-excel' });
        const url= window.URL.createObjectURL(blob);
        let anchor = document.createElement("a");

        const nameFile = formData?.saoKe == '1' ? `BC14.SAO_KE_COTUC_TM.xlsx` : `BC13.SAO_KE_CP.xlsx`
        const downloadFile = nameFile

        anchor.download = downloadFile;
        anchor.href = url;
        anchor.click();
    })
  }

  openPDF(data) {
    var blob = new window.Blob([data], { type: 'application/vnd.ms-excel' });

    var fileURL = URL.createObjectURL(blob);
    var win = window.open();
    win.document.write('<iframe src="' + fileURL + '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>')
}


  /**
 * Event load data phân trang ((page)="ChanePageIndex($event);")
 */
    ChanePageIndex($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex;
    const newData = this.fullDataSource.slice(Number(this.pageSize) * Number(this.pageIndex), Number(this.pageSize) * (Number(this.pageIndex) + 1))
    this.dataSource = new MatTableDataSource(newData);
  }

}