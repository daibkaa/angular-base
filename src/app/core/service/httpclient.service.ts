import { LocalStoreEnum, PATH, URL_BASE } from './../utils/constant';
import { Router } from '@angular/router';
import { CacheProcess } from './cache-process.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpParameterCodec } from "@angular/common/http";
import { DialogManagementService } from './dialog-management.service';
import { HttpOptions } from './model/request.base.dto';
import { HttpErrorInterface, HttpInterface } from './model/response.base.dto';
import { LocalStoreManagerService } from '@shared-sm';
export enum Verbs {
    GET = 'GET',
    PUT = 'PUT',
    POST = 'POST',
    DELETE = 'DELETE'
}
export class HttpUrlEncodingCodec implements HttpParameterCodec {
    encodeKey(k: string): string { return standardEncoding(k); }
    encodeValue(v: string): string { return standardEncoding(v); }
    decodeKey(k: string): string { return decodeURIComponent(k); }
    decodeValue(v: string) { return decodeURIComponent(v); }
}
function standardEncoding(v: string): string {
    return encodeURIComponent(v);
}
@Injectable({ providedIn: 'root' })
export class HttpClientService {

    constructor(
        private http: HttpClient,
        private localStore: LocalStoreManagerService,
        private cache: CacheProcess,
        private router: Router,
        private dialogManagementService: DialogManagementService
    ) {
    }

    get<T>(options: any): Observable<T> {
        return this.httpCall(Verbs.GET, options);
    }

    delete<T>(options: HttpOptions): Observable<T> {
        return this.httpCall(Verbs.DELETE, options);
    }

    post<T>(options: HttpOptions): Observable<T> {
        return this.httpCall(Verbs.POST, options);
    }

    put<T>(options: HttpOptions): Observable<T> {
        return this.httpCall(Verbs.PUT, options);
    }

    httpCall<T>(verb: Verbs, options: HttpOptions): Observable<T> {
        if (this.tokenValid(options.path)) {
            this.navigateLogin();
            return of();
        }
        const user = this.localStore.getData(LocalStoreEnum.User_Infor);
        // Setup default values
        options.body = options.body ?? null;
        options.headers = options.headers ?? {};
        options.isAuthentication = options.isAuthentication ?? true;
        if (options.isAuthentication) {
            options.headers = {
                ...options.headers,
                Authorization: `Bearer ${this.localStore.getData(LocalStoreEnum.Token)}`
            };
        }
        return this.http.request<T>(verb, `${options.url ?? URL_BASE}/${options.path}`, {
            body: options.body,
            headers: options.headers,
            params: options.params ?? null
        });
    }

    download(verb: Verbs, options: HttpOptions) {
        if (this.tokenValid(options.path)) {
            this.navigateLogin();
            return of();
        }
        const user = this.localStore.getData(LocalStoreEnum.User_Infor);
        options.body = options.body ?? null;
        options.headers = options.headers ?? {};
        options.isAuthentication = options.isAuthentication ?? true;
        if (options.isAuthentication) {
            options.headers = {
                ...options.headers,
                processId: this.cache.getpId(),
                username: user?.user_name,
                Authorization: `Bearer ${this.localStore.getData(LocalStoreEnum.Token)}`
            };
        }
        return this.http.request(verb, `${options.url ?? URL_BASE}/${options.path}`, {
            headers: options.headers,
            observe: 'response',
            responseType: 'blob',
            params: options.params ?? null
        });
    }

    upload(options: HttpOptions) {
        if (this.tokenValid(options.path)) {
            this.navigateLogin();
            return of();
        }
        const user = this.localStore.getData(LocalStoreEnum.User_Infor);
        options.body = options.body ?? null;
        options.headers = options.headers ?? {};
        options.isAuthentication = options.isAuthentication ?? true;
        if (options.isAuthentication) {
            options.headers = {
                ...options.headers,
                processId: this.cache.getpId(),
                username: user?.user_name,
                Authorization: `Bearer ${this.localStore.getData(LocalStoreEnum.Token)}`
            };
        }
        return this.http.post<any>(`${options.url ?? URL_BASE}/${options.path}`, options.body, {
            headers: options.headers,
            observe: 'response',
            // responseType: 'blob' as 'json',
            params: options.params ?? null
        });
    }

    tokenValid(url): boolean {
        const token = this.localStore.getData(LocalStoreEnum.Token);
        return !token && url != PATH.GENERATE_TOKEN  && url != PATH.REVOKE_TOKEN
    }

    navigateLogin(){
        this.dialogManagementService.closeAll();
        this.router.navigate(['/auth/login']);
    }
}

export declare interface NgOnHttp {
    onSuccess(response: HttpInterface<any>): void;
    onError(error: HttpErrorInterface): void;
}


