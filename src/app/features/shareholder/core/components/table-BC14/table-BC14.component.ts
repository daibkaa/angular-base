import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import { ShareholderService } from '../../services/shareholder.service';

@Component({
  selector: 'app-table-BC14',
  templateUrl: './table-BC14.component.html',
  styleUrls: ['./table-BC14.component.scss'],
})
export class TableBC14Component implements OnInit {

  @Input() dataSource: MatTableDataSource<any>;
  @Input() hasDataSource = false;
  displayedColumns: string[] = ['stt', 'hoTen', 'soDksh', 'ngayCap', 'diaChiVsd', 'tyLeTraCotuc', 'cotucClkSauthue', 'action'];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;
  @Output() exportExcel = new EventEmitter();

  constructor(
    public _shareholderService: ShareholderService,
  ) {
  }

  // Xử lý dữ liệu đầu vào
  ngOnInit(): void {}

  destroyData() {
  }

  onExport(element, index) {
    this.exportExcel.emit(element)
  }

}