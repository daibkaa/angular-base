import { MatIconModule } from '@angular/material/icon';
import { SharedSMModule } from '@shared-sm';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ShareholderMajorManagementRoutingModule } from './shareholder-major-management-routing.module';
import { ShareholderManagementComponent } from './components/main-screens/shareholder-management.component';
import { MatTabsModule } from '@angular/material/tabs';
import { ShareholderMajorManagementService } from './services/shareholder-major-management.service';
import { ReportMmanagementComponent } from './components/report-management/report-management.component';
import { QueryInfoManagementComponent } from './components/query-info-management/query-info-management.component';
import { MatTableModule } from '@angular/material/table';
import { ShareHolderCoreModule } from '../core/core.module';
import { MatStepperModule } from '@angular/material/stepper';
import { OwnershipInformationComponent } from './components/ownership-information/ownership-information.component';
import { StockMbbComponent } from './components/stock-mbb/stock-mbb.component';
import { TableC2Component } from './components/table-C2/table-C2.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { EditC2Component } from './components/edit-c2/edit-c2.component';
import { InfoManagementComponent } from './components/info-management/info-management.component';
import { AddNewB13Component } from './components/add-new-b13/add-new-b13.component';
import { AddNewB14Component } from './components/add-new-b14/add-new-b14.component';
import { AddNewB15B16Component } from './components/add-new-b15-b16/add-new-b15-b16.component';
import { AddNewB13NNBNLQComponent } from './components/add-new-b13-nnb-nlq/add-new-b13-nnb-nlq.component';
import { AddNewB14NNBNLQComponent } from './components/add-new-b14-nnb-nlq/add-new-b14-nnb-nlq.component';
import { AddNewB15B16NNBNLQComponent } from './components/add-new-b15-b16-nnb-nlq/add-new-b15-b16-nnb-nlq.component';
import { AddNewDeMucB13Component } from './components/add-new-demuc-b13/add-new-demuc-b13.component';
import { AddNewDeMucB14Component } from './components/add-new-demuc-b14/add-new-demuc-b14.component';
import { EditChuthichNnb } from './components/edit-chuthich-nnb/edit-chuthich-nnb.component';
import { AddNewChiMucComponent } from './components/add-new-chimuc/add-new-chimuc.component';
import { TableC3Component } from './components/table-C3/table-C3.component';
import { TableC4Component } from './components/table-C4/table-C4.component';

const COMPONENTS_DYNAMIC = [];
const SERVICE = [ShareholderMajorManagementService];

@NgModule({
  imports: [
    CommonModule,
    SharedSMModule,
    ShareholderMajorManagementRoutingModule,
    MatIconModule,
    MatTabsModule,
    MatTableModule,
    ShareHolderCoreModule,
    MatStepperModule,
    MatTooltipModule
  ],
  declarations: [
    ShareholderManagementComponent,
    ReportMmanagementComponent,
    QueryInfoManagementComponent,
    OwnershipInformationComponent,
    StockMbbComponent,
    InfoManagementComponent,
    AddNewB13Component,
    AddNewB14Component,
    TableC2Component,
    EditC2Component,
    AddNewB15B16Component,
    AddNewB13NNBNLQComponent,
    AddNewB14NNBNLQComponent,
    AddNewB15B16NNBNLQComponent,
    AddNewDeMucB13Component,
    AddNewDeMucB14Component,
    EditChuthichNnb,
    AddNewChiMucComponent,
    TableC3Component,
    TableC4Component
  ],
  entryComponents: COMPONENTS_DYNAMIC,
  providers: [...SERVICE]
})
export class ShareholderMajorManagementModule {
}
