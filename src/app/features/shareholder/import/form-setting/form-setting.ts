import { DateTimeItem, NgSelectItem, TextboxItem } from "@shared-sm";
import * as moment from "moment";
import { getDataLocalStorageByKey } from "src/app/core/service/utils.service";

export const REPORT_TYPE = () => new NgSelectItem({
    key: 'reportType',
    placeholder: 'Loại báo cáo',
    label: 'Loại báo cáo',
    value: '',
    layout: '50',
    options: getDataLocalStorageByKey('listCategory').filter(f => f.screen == 'qlcd' && f.tab == 2).map(m => ({
        key: m.value,
        value: m.text
    })),
    required: true
});

export const REPORT_DATE = () => new DateTimeItem({
    key: 'reportDate',
    placeholder: 'Ngày báo cáo',
    label: 'Ngày báo cáo',
    value: moment().format('YYYY-MM-DD'),
    layout: '50',
    minDate: '1900-01-01',
});

export const OWNER_REGISTRATIO_NUMBER = () => new TextboxItem({
    key: 'soDksh',
    label: 'Số đăng ký sở hữu',
    placeholder: 'Số đăng ký sở hữu',
    value: '',
    layout: '50',
    directives: ''
});

export const FULL_NAME = () => new TextboxItem({
    key: 'hoTen',
    label: 'Họ tên',
    placeholder: 'Họ tên',
    value: '',  
    layout: '50',
    directives: ''
});

export const ADDRESS = () => new TextboxItem({
    key: 'diaChi',
    label: 'Địa chỉ',
    placeholder: 'Địa chỉ',
    value: '',
    layout: '50',
    directives: ''
});

export const GROUP_TYPE = () => new NgSelectItem({
    key: 'groupType',
    placeholder: 'Nhóm cổ đông',
    label: 'Nhóm cổ đông',
    value: '',
    layout: '50',
    options: getDataLocalStorageByKey('listCategory').filter(f => f.screen == 'qlttcd' && f.tab == 3).map(m => ({
        key: m.value,
        value: m.text
    })),
    // required: true
});