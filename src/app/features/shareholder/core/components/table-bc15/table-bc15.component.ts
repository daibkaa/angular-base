import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { COLUMN_DEFAULT } from '@core';
import { DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import * as moment from 'moment';
import { CDL_TYPE_ROMAN, CLD_TYPE_DEMUC } from '../../constant';
import { ShareholderService } from '../../services/shareholder.service';

@Component({
  selector: 'app-table-bc15',
  templateUrl: './table-bc15.component.html',
  styleUrls: ['./table-bc15.component.scss'],
})
export class TableBC15Component {

  @Input() displayedColumns;
  @Input() dataSource: MatTableDataSource<any>;
  @Input() hasDataSource = false;
  @Input() listColumnGroup;
  @Input() compareDate;
  @Input() reportDate;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  constructor(
    public _shareholderService: ShareholderService,
  ) {
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
  }

  _renderHeaderColums() {
    let baseColumn = ['header-row-stt-group', 'header-row-coDong-group', 'header-row-dkkd-group'];
    this.listColumnGroup.forEach((element, index) => {
      const columsItem = `header-row-${index}-group`;
      baseColumn = [...baseColumn, columsItem];
    });
    baseColumn = [...baseColumn, 'header-row-tyLeSoHuuThayDoi-group', 'header-row-ghiChu-group']
    return baseColumn;
  }

  _renderColumsIndex(index) {
    return `header-row-${index}-group`;
  }

  _renderItem(column: string, index) {
    return `${column}-${index}`
  }

  _renderTitle(item) {
    let title = null;
    const string = item.toString().split('-')[0];
    if(['stt', 'coDong', 'soDksh', 'tyLeSoHuuThayDoi', 'ghiChu'].includes(string)) {
      return null;
    }
    return this._shareholderService._renderColumsTitle(string);
  }

  _rendertitleMouth(index) {
    const title = {
      0: `Tháng ${moment(this.reportDate).format('MM/YYYY')}`,
      1: `Tháng ${moment(this.compareDate).format('MM/YYYY')}`
    }
    return title[index];
  }

  destroyData() {
  }

}