import { ChangeDetectorRef, Component, Inject, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Status } from '@core';
import { ComponentAbstract } from '@shared-sm';
import * as moment from 'moment';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { IS_ADD_NEW } from '../../form-setting/add-new';
import { STT, CHUCVUMOIQH, DIA_CHI, DIENTHOAI, EMAIL, GIOITINH, LOAIHINHID, NGAY_CAP, NGAY_HIEU_LUC, NOI_CAP, SO_DKSH, TENTCCN, TKGDCK, PARENTID, DEMUC, CHUTHICHDEMUCDDL, NGUOINOIBO, CHUTHICHNGUOINOIBO } from '../../form-setting/add-new-b14';

@Component({
  selector: 'app-add-new-b14-nnb-nlq',
  templateUrl: './add-new-b14-nnb-nlq.component.html',
  styleUrls: ['./add-new-b14-nnb-nlq.component.scss']
})
export class AddNewB14NNBNLQComponent extends ComponentAbstract {

  isAddNew = IS_ADD_NEW();

  PARENTID = PARENTID();
  DEMUC = DEMUC();
  CHUTHICHDEMUCDDL = CHUTHICHDEMUCDDL();
  NGUOINOIBO = NGUOINOIBO();
  CHUTHICHNGUOINOIBO = CHUTHICHNGUOINOIBO();
  STT = STT();
  TENTCCN = TENTCCN();
  TKGDCK = TKGDCK();
  CHUCVUMOIQH = CHUCVUMOIQH();
  GIOITINH = GIOITINH();
  DIA_CHI = DIA_CHI();
  LOAIHINHID = LOAIHINHID();
  SO_DKSH = SO_DKSH();
  NGAY_CAP = NGAY_CAP();
  NOI_CAP = NOI_CAP();
  DIENTHOAI = DIENTHOAI();
  EMAIL = EMAIL();
  NGAY_HIEU_LUC = NGAY_HIEU_LUC();

  listDemuc: Array<any> = [];
  showListDemuc: boolean = false;
  

  constructor(
    protected injector: Injector,
    public dialogRef: MatDialogRef<AddNewB14NNBNLQComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {
    super(injector);
  }
  checked = false;

  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.STT, this.TENTCCN, this.TKGDCK, this.CHUCVUMOIQH, this.GIOITINH, this.DIA_CHI, this.LOAIHINHID, this.SO_DKSH, this.NGAY_CAP, this.NOI_CAP, this.DIENTHOAI, this.EMAIL, this.PARENTID, this.DEMUC, this.CHUTHICHDEMUCDDL, this.NGUOINOIBO, this.CHUTHICHNGUOINOIBO]);

    this.PARENTID.options = this.data.listData.filter(f => !f.parentId).map(m => {
      return {
        key: m.id,
        value: m.tenTcCn
      }
    });
    if(this.data?.data?.id) {
      this.form.patchValue(this.data.data);
      this.form.get(this.PARENTID.key).disable();
      this.form.get(this.DEMUC.key).disable();
      this.form.get(this.CHUTHICHDEMUCDDL.key).disable();
      this.listDemuc = ([...this.data.listData] || []).filter(f => (f.parentId == this.data.data?.parentId) && !f.deMuc && !f.soDksh)
    }
    this.onChangeParentId();
  }

  onChangeParentId() {
    this.form.get(this.PARENTID.key).valueChanges.pipe(
      takeUntil(this.ngUnsubscribe),
      distinctUntilChanged()).subscribe(value => {
        if(value) {
          this.listDemuc = ([...this.data.listData] || []).filter(f => (f.parentId == value) && !f.deMuc && !f.soDksh)
        } else {
          this.listDemuc = [];
        }
        
      })
  }

  onSelect(item) {
    this.form.get(this.DEMUC.key).setValue(item.stt)
    this.form.get(this.CHUTHICHDEMUCDDL.key).setValue(item.tenTcCn)
  }

  focusEvent($event) {
    this.showListDemuc = true;
  }

  focusOutEvent($event) {
    setTimeout(() => {
      this.showListDemuc = false;
      const item = ([...this.listDemuc] || []).find(f => f.stt == this.form.get(this.DEMUC.key).value);
      if(item) {
        this.form.get(this.CHUTHICHDEMUCDDL.key).setValue(item.tenTcCn)
      } else {
        // do something
      } 
    }, 200)
  }

  saveData() {
    this.validateAllFields(this.form);
    if (!this.form.valid) {
      this.dialogService.error({
        title: 'dialog.notification',
        message: 'dialog.valid-error',
      }, res => {
        if (res) {
        }
      });
      return;
    }

    
    
    const formData = this.form.getRawValue()
    let paramDialog = {
      data: {
        ...formData,
        id: this.data.data?.id || null
      },
    }
    this.dialogRef.close(paramDialog)

  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  closeDialog() {
    if (this.dialogRef.close) { this.dialogRef.close(null); }
  }

  testData() {
    const data = {
      id: null,
      stt: 3,
      tenTcCn: "Nguyễn Văn A",
      tkgdck: "005C055145",
      chucVuMoiQh: "Chủ tịch HĐQT",
      gioiTinh: "Nam",
      diaChi: "Hà Nội",
      loaiHinhId: "CMND",
      soDksh: "01302856",
      ngayCap: "2012-12-06",
      noiCap: "118 Hưng Phú, P.8, Q.8, Tp.HCM",
      dienThoai: "0988866562",
      email: "vana@gmail.com",
    }
    this.form.patchValue(data)
  }

}
