// import { URL_TIMEOUT, URL_ERROR } from './../utils/constant';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { DformDialogService } from '@shared-sm';
// import { DformDialogService } from '@shared/components/dform-dialogs/dialog.service';
import { Observable, throwError } from 'rxjs';
import { catchError, timeout } from 'rxjs/operators';
import { AuthenticationService } from '../service/authentication.service';
import { URL_ERROR, URL_TIMEOUT } from '../utils/constant';


@Injectable()
export class JWTResponseInterceptor implements HttpInterceptor {
  private readonly REMOVE_TOKEN_AUTH = 401;
  private readonly ERROR_500 = 500;
  constructor(
    private dialogService: DformDialogService,
    private auth: AuthenticationService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Kiểm tra timeout nếu gửi duyệt all thì để 2 phút các trường hợp khác 30s
    if (this.checkUrlTimeOut(req?.url, URL_TIMEOUT)) {
      return next.handle(req).pipe(
        catchError((error: any) => this.handleErrorReq(error, req))
      );
    } else {
      const httpClientTimeOut = environment.timeOut;
      return next.handle(req).pipe(
        timeout(httpClientTimeOut),
        catchError((error: any) => this.handleErrorReq(error, req))
      );
    }
  }

  private handleErrorReq(error: any, req?: HttpRequest<any>): Observable<never> {
    // Bỏ qua lỗi của các api login,logout, refresh và get catalog tự handle
    if (this.checkUrlTimeOut(req.url, URL_ERROR)) {
      if (error?.status == this.REMOVE_TOKEN_AUTH || error?.status == 403) {
        this.auth.clearData(false);
      }
      return throwError(error);
    }

    // Hiển thị lỗi timeout
    if (error && error.name === "TimeoutError") {
      this.dialogService.error({
        title: 'Thông báo',
        message: 'error.timeout_message'
      }, dialog => {
        if (dialog) {
        }
      });
      return throwError(error);
    }

    // Kiểm tra mã lỗi
    switch (error.status) {
      // Lỗi hết token
      case this.REMOVE_TOKEN_AUTH:
        this.auth.clearData(false);
        break;
      case 403:
        this.auth.clearData(false);
          break;
      // Lỗi response trả về 500
      case this.ERROR_500:
        const error500 = error.error && error.error?.error ? error.error?.error : 'Có lỗi xảy ra. Vui lòng liên hệ để được hỗ trợ.';
        const requestId500 = error.error && error.error?.requestId ? error.error?.requestId : null;
        this.dialogService.error({
          title: 'Thông báo',
          innerHTML: requestId500 ? `${error500} <br> ${requestId500}` : `${error500}`
        }, dialog => {
          if (dialog) {
            // window.location.reload();
          }
        });
        break;
      // Các lỗi còn lại
      default:
        const message = error.error && error.error?.msg ? error.error?.msg : 'Có lỗi xảy ra. Vui lòng liên hệ để được hỗ trợ.';
        const requestId = error.error && error.error?.requestId ? error.error?.requestId : null;
        this.dialogService.error({
          title: 'Thông báo',
          innerHTML: requestId ? `${message} <br> ${requestId}` : `${message}`
        }, dialog => {
          if (dialog) {
          }
        });
        break;
    }
    return throwError(error);
  }

  // Xử lý check trùng url
  checkUrlTimeOut(req: string, listUrl) {
    for (let i = 0; i < listUrl.length; i++) {
      if (req.includes(listUrl[i])) {
        return true
      }
    }
    return false
  }

}