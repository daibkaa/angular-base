import { DateTimeItem, NgSelectItem, TextboxItem } from "@shared-sm";

export const PARENTID = () => new NgSelectItem({
   key: 'parentId',
   placeholder: 'Cổ đông lớn',
   label: 'Cổ đông lớn',
   value: '',
   layout: '50',
   options: [],
   required: true
});

export const DEMUC = () => new TextboxItem({
   key: 'deMuc',
   placeholder: 'Đề mục',
   label: 'Đề mục',
   value: '',
   layout: '50',
   required: true
});

export const CHUTHICHDEMUC = () => new NgSelectItem({
   key: 'chuThichDeMuc',
   placeholder: 'Chú thích đề mục',
   label: 'Chú thích đề mục',
   value: '',
   layout: '50',
   options: [],
   required: true
});

export const NGUOINOIBO = () => new TextboxItem({
   key: 'stt',
   placeholder: 'Người nội bộ',
   label: 'Người nội bộ',
   value: '',
   layout: '50',
   required: true
});

export const CHUTHICHNGUOINOIBO = () => new TextboxItem({
   key: 'cdLon',
   placeholder: 'Chú thich người nội bộ',
   label: 'Chú thích người nội bộ',
   value: '',
   layout: '50',
   required: true
});

export const NGAY_HIEU_LUC = () => new DateTimeItem({
   key: 'validDate',
   placeholder: '',
   label: 'Ngày hiệu lực',
   value: '',
   layout: '50',
});