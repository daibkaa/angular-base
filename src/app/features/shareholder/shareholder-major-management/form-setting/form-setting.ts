import { DateTimeItem, getDataLocalStorageByKey, NgSelectItem, TextboxItem } from "@shared-sm";
import * as moment from "moment";

export const REPORT_TYPE_MAJOR = () => new NgSelectItem({
    key: 'reportType',
    placeholder: 'Loại báo cáo',
    label: 'Loại báo cáo',
    value: '',
    layout: '50',
    options: getDataLocalStorageByKey('listCategory').filter(f => f.screen == 'nnb' && f.tab == 2).map(m => ({
        key: m.value,
        value: m.text
    })),
    required: true
});

export const REPORT_DATE_MAJOR = () => new DateTimeItem({
    key: 'reportDate',
    placeholder: 'Ngày báo cáo',
    label: 'Ngày báo cáo',
    value: moment().format('YYYY-MM-DD'),
    layout: '50',
    minDate: '1900-01-01',
    required: true
});

export const OWNER_REGISTRATIO_NUMBER = () => new TextboxItem({
    key: 'soDksh',
    label: 'Số đăng ký sở hữu',
    placeholder: 'Số đăng ký sở hữu',
    value: '',
    layout: '50',
    directives: ''
});

export const FULL_NAME = () => new TextboxItem({
    key: 'hoTen',
    label: 'Họ tên',
    placeholder: 'Họ tên',
    value: '',  
    layout: '50',
    directives: ''
});

export const GROUP_TYPE = () => new NgSelectItem({
    key: 'lawType',
    placeholder: 'Theo luật',
    label: 'Theo luật',
    value: '',
    layout: '50',
    options: getDataLocalStorageByKey('listCategory').filter(f => f.screen == 'nnb' && f.tab == 3).map(m => ({
        key: m.value,
        value: m.text
    })),
    required: true
});

export const RAW_TYPE = () => new NgSelectItem({
    key: 'rawType',
    placeholder: 'Theo luật',
    label: 'Theo luật',
    value: '',
    layout: '50',
    options: getDataLocalStorageByKey('listCategory').filter(f => f.screen == 'nnb' && f.tab == 3).map(m => ({
        key: m.value,
        value: m.text
    })),
    required: true
});

export const GROUP_TYPE_SEARCH_INFO = () => new NgSelectItem({
    key: 'lawType',
    placeholder: 'Theo luật',
    label: 'Theo luật',
    value: '',
    layout: '50',
    options: getDataLocalStorageByKey('listCategory').filter(f => f.screen == 'nnb' && f.tab == 4).map(m => ({
        key: m.value,
        value: m.text
    })),
    required: true
});

export const  ORIGINAL_PERIOD = () => new DateTimeItem({
    key: 'reportDate',
    placeholder: 'Kỳ báo cáo',
    label: 'Kỳ báo cáo',
    value: moment().format('YYYY-MM-DD'),
    layout: '50',
    minDate: '1900-01-01',
    required: true
});

export const COMPARISON_PERIOD = () => new DateTimeItem({
    key: 'compareDate',
    placeholder: 'Kỳ so sánh',
    label: 'Kỳ so sánh',
    value: moment().format('YYYY-MM-DD'),
    layout: '50',
    minDate: '1900-01-01',
    required: true
});

export const REPORT_TYPE_OWNERSHIP = () => new NgSelectItem({
    key: 'reportType',
    placeholder: 'Loại báo cáo',
    label: 'Loại báo cáo',
    value: '',
    layout: '50',
    options: getDataLocalStorageByKey('listCategory').filter(f => f.screen == 'nnb' && f.tab == 4).map(m => ({
        key: m.value,
        value: m.text
    })),
    required: true
});

export const TEN_TNCN = () => new TextboxItem({
    key: 'tenTcCn',
    label: 'Tên TNCN',
    placeholder: 'Tên TNCN',
    value: '',
    layout: '50',
    directives: ''
});

export const NLQHOTEN = () => new TextboxItem({
    key: 'nlqHoTen',
    label: 'Họ tên NLQ',
    placeholder: 'Tên TNCN',
    value: '',
    layout: '50',
    directives: ''
});

export const NLQSODKSH = () => new TextboxItem({
    key: 'nlqSoDksh',
    label: 'Số DKSH NLQ',
    placeholder: 'Số DKSH NLQ',
    value: '',
    layout: '50',
    directives: ''
});

export const NNBHOTEN = () => new TextboxItem({
    key: 'nnbHoTen',
    label: 'Họ tên NNB',
    placeholder: 'Họ tên NNB',
    value: '',
    layout: '50',
    directives: ''
});

export const NNBSODKSH = () => new TextboxItem({
    key: 'nnbSoDksh',
    label: 'Số DKSH NNB',
    placeholder: 'Số DKSH NNB',
    value: '',
    layout: '50',
    directives: ''
});