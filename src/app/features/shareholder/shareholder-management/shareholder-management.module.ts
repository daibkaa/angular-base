import { MatIconModule } from '@angular/material/icon';
import { SharedSMModule } from '@shared-sm';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ShareholderManagementRoutingModule } from './shareholder-management-routing.module';
import { ShareholderManagementComponent } from './components/main-screens/shareholder-management.component';
import { MatTabsModule } from '@angular/material/tabs';
import { ShareholderManagementService } from './services/shareholder-management.service';
import { ReportMmanagementComponent } from './components/report-management/report-management.component';
import { QueryInfoManagementComponent } from './components/query-info-management/query-info-management.component';
import { MatTableModule } from '@angular/material/table';
import { ShareHolderCoreModule } from '../core/core.module';

const COMPONENTS_DYNAMIC = [];
const SERVICE = [ShareholderManagementService];

@NgModule({
  imports: [
    CommonModule,
    SharedSMModule,
    ShareholderManagementRoutingModule,
    MatIconModule,
    MatTabsModule,
    MatTableModule,
    ShareHolderCoreModule
  ],
  declarations: [
    ShareholderManagementComponent,
    ReportMmanagementComponent,
    QueryInfoManagementComponent,
  ],
  entryComponents: COMPONENTS_DYNAMIC,
  providers: [...SERVICE]
})
export class ShareholderManagementModule {
}
