import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { COLUMN_DEFAULT } from '@core';
import { DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import { ShareholderService } from '../../services/shareholder.service';

@Component({
  selector: 'app-table-B15-B16',
  templateUrl: './table-B15-B16.component.html',
  styleUrls: ['./table-B15-B16.component.scss'],
})
export class TableB15B16Component implements OnInit {

  @Input() dataSource: MatTableDataSource<any>;
  @Input() hasDataSource = false;
  @Output() update = new EventEmitter();
  @Output() delete = new EventEmitter();
  displayedColumns: string[] = ['stt', 'tenTcCn', 'tkgdck', 'chucVuMoiQh', 'diaChi', 'soDksh', 'ngayCap', 'noiCap', 'dienThoai', 'email', 'action'];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;
  

  constructor(
    public _shareholderService: ShareholderService,
  ) {
  }

  // Xử lý dữ liệu đầu vào
  ngOnInit(): void {}

  onEdit(element) {
    console.log(element, 'element')
    this.update.emit(element)
  }

  onDelete(element) {
    this.delete.emit(element)
  }

  destroyData() {
  }
}