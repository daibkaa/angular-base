import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { COLUMN_DEFAULT } from '@core';
import { DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import { ShareholderService } from '../../services/shareholder.service';

@Component({
  selector: 'app-table-BC9-BC11',
  templateUrl: './table-BC9-BC11.component.html',
  styleUrls: ['./table-BC9-BC11.component.scss'],
})
export class TableBC9BC11Component implements OnInit {

  @Input() dataSource: MatTableDataSource<any>;
  @Input() hasDataSource = false;
  @Output() edit = new EventEmitter();
  @Output() delete = new EventEmitter();

  displayedColumns: string[] = ['stt', 'tenTcCn', 'tkgdck', 'chucVuMoiQh', 'diaChi', 'soDksh', 'ngayCap', 'noiCap', 'dienThoai', 'email', 'soLuong', 'tyLe', 'gdTrongThang'];

  constructor(
    public _shareholderService: ShareholderService,
  ) {
  }

  // Xử lý dữ liệu đầu vào
  ngOnInit(): void {}

  destroyData() {
  }

  onEdit(element) {
    this.edit.emit(element)
  }

  onDelete(element) {
    this.delete.emit(element)
  }

}