import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';

@Injectable()
/****************************************************
*** description: route guard to check permission to an url
*****************************************************/
export class PermissionCatalogUtilitiesGuard implements CanActivate {

  constructor(
    private auth: AuthenticationService
  ) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return this.auth.loadCatalogByConfig(next.data?.catalog);
  }
}
