import { CheckboxItem, DateTimeItem, getDataLocalStorageByKey, NgSelectItem, TextboxItem } from "@shared-sm";

export const CDL = () => new NgSelectItem({
    key: 'parentId',
    placeholder: 'Cổ đông lớn',
    label: 'Cổ đông lớn',
    value: '',
    layout: '50',
    options: [],
    required: true
});

export const PARENTID = () => new NgSelectItem({
    key: 'parentId',
    placeholder: 'Người nội bộ',
    label: 'Người nội bộ',
    value: '',
    layout: '50',
    options: [],
    required: true
});

export const TEN_TCCN = () => new TextboxItem({
    key: 'tenTcCn',
    placeholder: 'Tên TCCN',
    label: 'Tên TCCN',
    value: '',
    layout: '50',
    required: true
});

export const TKGDCK = () => new TextboxItem({
    key: 'tkgdck',
    placeholder: 'tkgdck',
    label: 'Tkgdck',
    value: '',
    layout: '50',
});

export const TEN_TNCN = () => new NgSelectItem({
    key: 'parentId',
    placeholder: 'Tên TNCN',
    label: 'Tên TNCN',
    value: '',
    layout: '50',
    options: [],
    required: true
});


export const IS_ADD_NEW = () => new CheckboxItem({
    key: 'isAddNew',
    layout: '25',
    checked: true,
    value: '',
    options: [
        {
            key: "Y",
            value: 'Thêm mới đề mục'
        }
    ]
})

export const CHUTHICHDEMUCDDL = () => new NgSelectItem({
    key: 'chuThichDeMuc',
    placeholder: 'Đề mục',
    label: 'Đề Mục',
    value: '',
    layout: '50',
    required: true
});

export const DEMUC = () => new TextboxItem({
    key: 'deMuc',
    placeholder: 'Đề mục',
    label: 'Đề mục',
    value: '',
    layout: '50',
    required: true
});

export const CHUTHICHDEMUCTEXT = () => new TextboxItem({
    key: 'chuThichDeMuc',
    placeholder: 'Chú thich đề mục',
    label: 'Chú thích đề mục',
    value: '',
    layout: '50',
    required: true
});

export const nguoiNoiBo = () => new TextboxItem({
    key: 'nguoiNoiBo',
    placeholder: 'Người nội bộ',
    label: 'Người nội bộ',
    value: '',
    layout: '50',
});

export const chuThichNguoiNoiBo = () => new TextboxItem({
    key: 'chuThichNguoiNoiBo',
    placeholder: 'Chú thich người nội bộ',
    label: 'Chú thích người nội bộ',
    value: '',
    layout: '50',
});

export const STT = () => new TextboxItem({
    key: 'stt',
    placeholder: 'stt',
    label: 'STT',
    value: '',
    layout: '50',
    required: true
});

export const nguoiLienQuan = () => new TextboxItem({
    key: 'nguoiLienQuan',
    placeholder: '',
    label: 'Người liên quan',
    value: '',
    layout: '50',
    required: true
});

export const soDksh = () => new TextboxItem({
    key: 'soDksh',
    placeholder: '',
    label: 'Số DKSH',
    value: '',
    layout: '50',
    required: true
});

export const ngayCap = () => new DateTimeItem({
    key: 'ngayCap',
    placeholder: '',
    label: 'Ngày cấp',
    value: '',
    layout: '50',
});

export const noiCap = () => new TextboxItem({
    key: 'noiCap',
    placeholder: '',
    label: 'Nơi cấp',
    value: '',
    layout: '50',
});

export const diaChi = () => new TextboxItem({
    key: 'diaChi',
    placeholder: '',
    label: 'Địa chỉ',
    value: '',
    layout: '50',
});

export const tenNguoiDaiDienVon = () => new TextboxItem({
    key: 'tenNguoiDaiDienVon',
    placeholder: '',
    label: 'Tên người đại diện vốn',
    value: '',
    layout: '50',
});

export const chucVuTaiDonVi = () => new TextboxItem({
    key: 'chucVuTaiDonVi',
    placeholder: '',
    label: 'Chức vụ tại đơn vị',
    value: '',
    layout: '50',
});

export const moiQuanHe = () => new TextboxItem({
    key: 'moiQuanHe',
    placeholder: '',
    label: 'Mối quan hệ',
    value: '',
    layout: '50',
});

export const nganhNghe = () => new TextboxItem({
    key: 'nganhNghe',
    placeholder: '',
    label: 'Ngành nghề',
    value: '',
    layout: '50',
});

export const tyLeSoHuuCdlonTaiDonVi = () => new TextboxItem({
    key: 'tyLeSoHuuCdlonTaiDonVi',
    placeholder: '',
    label: 'Tỷ lệ sở hữu CDL tại đơn vị (%)',
    value: '',
    layout: '50',
    readOnly: true
});

export const cmtNguoiDaiDien = () => new TextboxItem({
    key: 'cmtNguoiDaiDien',
    placeholder: '',
    label: 'CMT người đại diện',
    value: '',
    layout: '50',
});

export const GDTRONGTHANG = () => new TextboxItem({
    key: 'gdTrongThang',
    placeholder: 'GD trong tháng',
    label: 'GD trong tháng',
    value: '',
    layout: '50',
    readOnly: true
});

export const SOLUONG = () => new TextboxItem({
    key: 'soLuong',
    placeholder: 'Số lượng',
    label: 'Số lượng',
    value: '',
    layout: '50',
    readOnly: true
});

export const chucVuMoiQh = () => new TextboxItem({
    key: 'chucVuMoiQh',
    placeholder: 'Chức vụ / Mỗi QH',
    label: 'Chức vụ / Mỗi QH',
    value: '',
    layout: '50',
    // readOnly: true
});

export const dienThoai = () => new TextboxItem({
    key: 'dienThoai',
    placeholder: 'Số điện thoại',
    label: 'Số điện thoại',
    value: '',
    layout: '50',
    // required: true
});

export const email = () => new TextboxItem({
    key: 'email',
    placeholder: 'Email',
    label: 'Email',
    value: '',
    layout: '50',
    // required: true
});

export const tyLe = () => new TextboxItem({
    key: 'tyLe',
    placeholder: 'Tỷ lệ (%)',
    label: 'Tỷ lệ (%)',
    value: '',
    layout: '50',
    readOnly: true
});


// chuThichDeMuc: "Công ty hoặc tổ chức tín dụng mà Ông Nguyễn Văn A là người quản lý (chủ tịch HĐQT, Thành viên HĐQT, TGĐ, các chức danh quản lý khác quy định tại Điều lệ), thành viên Ban kiểm soát;",
// chuThichNguoiNoiBo: null,
// chucVuMoiQh: "qh test",
// deMuc: "E",
// diaChi: "abc acb",
// dienThoai: null,
// email: null,
// gdTrongThang: 0,
// gioiTinh: null,
// id: null,
// loaiHinhId: "Giấy ĐKKD",
// ngayCap: "30.09.1994",
// nguoiNoiBo: null,
// noiCap: "noi cap",
// parentId: "38275183-8381-456b-8390-8b904a7c6056",
// reportDate: "2022-06-07",
// soDksh: "868748454",
// soLuong: 0,
// stt: "1.2",
// tenTcCn: "Ngân hàng test created",
// tkgdck: null,
// tyLe: 0

