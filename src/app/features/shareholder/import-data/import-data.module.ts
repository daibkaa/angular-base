import { MatIconModule } from '@angular/material/icon';
import { SharedSMModule } from '@shared-sm';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ImportRoutingModule } from './import-data-routing.module';
import { MatTabsModule } from '@angular/material/tabs';
import { ImportInfoService } from './services/import-info.service';
import { MatTableModule } from '@angular/material/table';
import { ShareHolderCoreModule } from '../core/core.module';
import { ImportInfoComponent } from './import-data.component';
import { MatTooltipModule } from '@angular/material/tooltip';

const COMPONENTS_DYNAMIC = [];
const SERVICE = [ImportInfoService];

@NgModule({
  imports: [
    CommonModule,
    SharedSMModule,
    ImportRoutingModule,
    MatIconModule,
    MatTabsModule,
    MatTableModule,
    ShareHolderCoreModule,
    MatTooltipModule
  ],
  declarations: [
    ImportInfoComponent
  ],
  entryComponents: COMPONENTS_DYNAMIC,
  providers: [...SERVICE]
})
export class ImportDataModule {
}
