import { environment } from '@env/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, ElementRef, Injector, isDevMode, ViewChild } from '@angular/core';
import { distinctUntilChanged, finalize, takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { HttpHeaders } from '@angular/common/http';
// import { AuthenticationService, MessageSeverity, ToastService } from 'src/app/core';
import Utils from 'src/app/shared/utils/utils';
import { ActivityIndicatorSingletonService } from '@shared-sm';
import { AuthenticationService, MessageSeverity, ToastService } from '@core';
// import { MessageSeverity } from 'src/app/core/service/toast.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})

export class LoginComponent {

  form: FormGroup = new FormGroup({
    username: new FormControl('admin', [Validators.required]),
    password: new FormControl(1, Validators.required),
  });
  errorData;
  isHide: boolean = true;
  authentication;

  constructor(
    protected injector: Injector,
    private route: ActivatedRoute,
    private auth: AuthenticationService,
    private router: Router,
    protected indicator: ActivityIndicatorSingletonService,
    private toastr: ToastService
  ) {
  }

  protected initData(): void {
    // this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  loginAction = () => {
    if (!this.form.valid) {
      return
    }
    this.errorData = '';
    let { username, password } = this.form.value;
    this.authentication = Utils.convertObjectToBase64(
      `${username}:${password}`
    );
    this.indicator.showActivityIndicator();
    this.auth.login(this.authentication).pipe(
      // takeUntil(this.ngUnsubscribe)
      finalize(() => this.indicator.hideActivityIndicator()),
    ).subscribe((res: any) => {
      console.log(res, 'rrrrr')
      if (res?.data?.accessToken) {
        if (this.auth.tokenValid()) {
          const config = {
            positionClass: 'toast-bottom-right',
            timeOut: 3000,
            extendedTimeOut: 3000
          };
          this.toastr.showToastr(
            `Đăng nhập thành công`,
            'Thông báo!',
            MessageSeverity.success,
            config
          );
          this.router.navigate(['ati/shareholder-management']);
        } else {
          this.errorData = 'error.login_permission';
        }
      } else {

      }
    }, (error) => {
      // this.indicator.hideActivityIndicator();
      const message = error.error && error?.error?.msg ? error?.error?.msg : 'Có lỗi xảy ra. Vui lòng liên hệ CNTT để được hỗ trợ.';
      this.errorData = message;
      const uuid = error.error && error.error?.uuid ? error.error?.uuid : null;
      // this.dialogService.error({
      //   title: 'dialog.notification',
      //   innerHTML: uuid ? `${message} <br> ${uuid}` : `${message}`
      // });
    });
  }

}
