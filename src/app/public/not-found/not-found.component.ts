import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'mbb-page-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent {

  public title = '';
  public button = '';

  constructor(private translate: TranslateService, private route: ActivatedRoute, private router: Router) {
    const error = route.snapshot.data['error'];
    this.title = error ? error :  this.translate.instant('error.title');
    this.button = this.translate.instant('error.button');
  }


  gotoLogin() {
    this.router.navigate(['/login'])
  }

  gotoHome() {
    this.router.navigate(['/'])
  }
}
