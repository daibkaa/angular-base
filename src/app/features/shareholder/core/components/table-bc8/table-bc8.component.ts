import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { COLUMN_DEFAULT } from '@core';
import { DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import { CDL_TYPE_ROMAN, CLD_TYPE_DEMUC } from '../../constant';
import { ShareholderService } from '../../services/shareholder.service';

@Component({
  selector: 'app-table-bc8',
  templateUrl: './table-bc8.component.html',
  styleUrls: ['./table-bc8.component.scss'],
})
export class TableBC8Component {

  @Input() displayedColumns;
  @Input() dataSource: MatTableDataSource<any>;
  @Input() hasDataSource = false;
  @Input() listColumnGroup;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  constructor(
    public _shareholderService: ShareholderService,
  ) {
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
  }

  _renderHeaderColums() {
    let baseColumn = ['header-row-stt-group', 'header-row-nhomCoDong-group', 'header-row-dkkd-group', 'header-row-cpDaiDien-group', 'header-row-cpThamDu-group', 'header-row-tyLeThamDu-group'];
    this.listColumnGroup.forEach((element, index) => {
      const columsItem = `header-row-${index}-group`;
      baseColumn = [...baseColumn, columsItem];
    });
    baseColumn = [...baseColumn]
    return baseColumn;
  } 

  _renderColumsIndex(index) {
    return `header-row-${index}-group`;
  }

  _renderItem(column: string, index) {
    return `${column}-${index}`
  }

  _renderTitle(item) {
    let title = null;
    const string = item.toString().split('-')[0];
    if(['stt', 'nhomCoDong', 'soDksh', 'cpDaiDien', 'cpThamDu', 'tyLeThamDu'].includes(string)) {
      return null;
    }
    return this._shareholderService._renderColumsTitle(string);
  }

  destroyData() {
  }

}