import { DateTimeItem, NgSelectItem, TextboxItem } from "@shared-sm";

export const PARENTID = () => new NgSelectItem({
   key: 'parentId',
   placeholder: 'Người nội bộ',
   label: 'Người nội bộ',
   value: '',
   layout: '50',
   options: [],
   required: true
});

export const STT = () => new TextboxItem({
   key: 'stt',
   placeholder: 'Đề mục',
   label: 'Đề mục',
   value: '',
   layout: '50',
   required: true
});

export const TENTCCN = () => new NgSelectItem({
   key: 'tenTcCn',
   placeholder: 'Chú thích đề mục',
   label: 'Chú thích đề mục',
   value: '',
   layout: '50',
   options: [],
   required: true
});

export const NGUOINOIBO = () => new TextboxItem({
   key: 'nguoiNoiBo',
   placeholder: 'Người nội bộ',
   label: 'Người nội bộ',
   value: '',
   layout: '50',
});

export const CHUTHICHNGUOINOIBO = () => new TextboxItem({
   key: 'chuThichNguoiNoiBo',
   placeholder: 'Chú thich người nội bộ',
   label: 'Chú thích người nội bộ',
   value: '',
   layout: '50',
});

export const NGAY_HIEU_LUC = () => new DateTimeItem({
   key: 'validDate',
   placeholder: '',
   label: 'Ngày hiệu lực',
   value: '',
   layout: '50',
});