import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ComponentAbstract, ComponentBaseAbstract, DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize, map, mergeMap, takeUntil } from 'rxjs/operators';
import { BC_TYPE, EXCEL_XLS, EXCEL_XLSX, HttpResponse, Status } from 'src/app/core';
import { checkFileType, dateNoneSeparation, removeEmptyObject } from 'src/app/core/utils/utils.function';
import { REJECT_COLUMN_DISPLAYS } from '../../../core/constant';
import { FULL_NAME, GROUP_TYPE, GROUP_TYPE_SEARCH_INFO, NLQHOTEN, NLQSODKSH, NNBHOTEN, NNBSODKSH, OWNER_REGISTRATIO_NUMBER, REPORT_DATE_MAJOR, REPORT_TYPE_MAJOR } from '../../form-setting/form-setting';
import { ShareholderMajorManagementService } from '../../services/shareholder-major-management.service';

@Component({
  selector: 'app-query-info-management',
  templateUrl: './query-info-management.component.html',
  styleUrls: ['./query-info-management.component.scss'],
})
export class QueryInfoManagementComponent extends ComponentAbstract {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  groupType = GROUP_TYPE();
  reportDate = REPORT_DATE_MAJOR();
  NLQHOTEN = NLQHOTEN();
  NLQSODKSH = NLQSODKSH();
  NNBHOTEN = NNBHOTEN();
  NNBSODKSH = NNBSODKSH();

  public dataSource: MatTableDataSource<any>;
  public hasDataSource = false;
  displayedColumns: string[];
  showtable;

  readonly BC_TYPE = BC_TYPE;
  
  constructor(
    protected _injector: Injector,
    private _shareholderMajorManagementService: ShareholderMajorManagementService,
  ) {
    super(_injector);
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.groupType, this.reportDate, this.NLQHOTEN, this.NLQSODKSH, this.NNBHOTEN, this.NNBSODKSH]);
  }

  onChangeGroupType() {
    this.form.get(this.groupType.key).valueChanges.pipe(
      takeUntil(this.ngUnsubscribe),
      ).subscribe(value => {

      })
  }

  destroyData() {
  }

  searchReport(): void {
    this.validateAllFields(this.form);
    if(!this.form.valid) {
      this.dialogService.error({
        title: 'Thông báo',
        innerHTML: 'Vui lòng nhập thông tin tìm kiếm'
      });
      return;
    }
    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue()
    let params: any = removeEmptyObject(formData)
    this.showtable = params.lawType
    if(formData?.reportDate  && formData.reportDate != '') {
      params.reportDate = moment(formData.reportDate).format('YYYY-MM-DD')
    }
    this._shareholderMajorManagementService.getInfo(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
      if (res && res.code === Status.SUCCESS && res?.data?.length) {
        this.hasDataSource = true;
        this.dataSource = new MatTableDataSource(res.data);
      } else {
        this.hasDataSource = false;
        this.dataSource = new MatTableDataSource([]);
      }
    }, error => {
      this.hasDataSource = false;
      this.dataSource = new MatTableDataSource([]);
    })
  }

  exportExcel() {
    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue()
    let params: any = removeEmptyObject(formData);
    params.lawType = this.showtable
    if(formData?.reportDate && formData?.reportDate != '') {
      params.reportDate = moment(formData.reportDate).format('YYYY-MM-DD')
    }
    this._shareholderMajorManagementService.exportInfo(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
        // this.openPDF(res.body)
        const blob = new Blob([res.body], { type: 'application/vnd.ms-excel' });
        const url= window.URL.createObjectURL(blob);
        let anchor = document.createElement("a");
        let downloadFile = '';
        switch(formData.reportType) {
          case BC_TYPE.NNB_NLQ_TT22: {
            downloadFile = `BC9.NNB_NLQ_TT22_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;
          }; break;
          case BC_TYPE.NNB_NLQ_LUAT_TCTD: {
            downloadFile = `BC10.NNB_NLQ_LUAT_TCTD_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;
          }; break;
          case BC_TYPE.NNB_NLQ_LUAT_CK: {
            downloadFile = `BC11.MBB_COTUC_TM_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;
          }; break;
          case BC_TYPE.CDLON_NLQ: {
            downloadFile = `BC12.CDLON_NLQ_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;
          }; break;
        }

        // const nameFile = formData?.saoKe == '1' ? `BC14.SAO_KE_COTUC_TM.xlsx` : `BC13.SAO_KE_CP.xlsx`

        anchor.download = downloadFile;
        anchor.href = url;
        anchor.click();
    })
  }

  /**
   * Event load data phân trang ((page)="ChanePageIndex($event);")
   */
  ChanePageIndex($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex;
  }
  

}