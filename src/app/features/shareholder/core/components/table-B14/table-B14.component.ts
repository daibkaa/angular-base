import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { COLUMN_DEFAULT } from '@core';
import { DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import { REJECT_COLUMN_DISPLAYS } from '../../constant';
import { ShareholderService } from '../../services/shareholder.service';

@Component({
  selector: 'app-table-B14',
  templateUrl: './table-B14.component.html',
  styleUrls: ['./table-B14.component.scss'],
})
export class TableB14Component implements OnInit {

  @Input() dataSource: MatTableDataSource<any>;
  @Input() hasDataSource = false;
  @Output() update = new EventEmitter();
  @Output() delete = new EventEmitter();

  displayedColumns: string[] = ['stt', 'tenTcCn', 'tkgdck', 'chucVuMoiQh', 'gioiTinh', 'diaChi', 'loaiHinhId', 'soDksh', 'ngayCap', 'noiCap', 'dienThoai', 'email', 'action'];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  constructor(
    public _shareholderService: ShareholderService,
  ) {
  }

  // Xử lý dữ liệu đầu vào
  ngOnInit(): void {}

  onUpdate(element) {
    this.update.emit(element)
  }

  onDelete(element) {
    this.delete.emit(element)
  }

  destroyData() {
  }

}