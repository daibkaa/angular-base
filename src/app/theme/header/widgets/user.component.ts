import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { DformDialogService, HttpClientService, LocalStoreEnum, LocalStoreManagerService } from '@shared-sm';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/core';

@Component({
  selector: 'app-user',
  template: `
    <div fxLayout="row" fxLayoutAlign="space-around center">
      <button
        mat-button
        class="dform-avatar-button mrl-2"
        href="javascript:void(0)"
        [matMenuTriggerFor]="menu"
      >
        <span class="dform-username" fxHide.lt-sm>{{ user?.user_name || 'admin_dai'}}</span>
        <mat-icon>arrow_drop_down</mat-icon>
      </button>
    </div>

    <mat-menu #menu="matMenu">
      <button mat-menu-item (click)="logout()">
        <mat-icon>exit_to_app</mat-icon>
        <span>{{ 'Đăng xuất' | translate }}</span>
      </button>
    </mat-menu>
  `,
})

export class UserComponent implements OnInit {
  user;
  constructor(
    private auth: AuthenticationService,
    private localStore: LocalStoreManagerService,

  ) {
    this.user = this.localStore.getData(LocalStoreEnum.User_Infor);
  }

  ngOnInit() {
    this.user = this.localStore.getData(LocalStoreEnum.User_Infor);
  }

  /**
   * Xử lý đăng xuất
   */
  logout() {
    this.auth.logout();
  }

}
