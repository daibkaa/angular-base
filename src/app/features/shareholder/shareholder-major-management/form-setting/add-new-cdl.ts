import { CheckboxItem, DateTimeItem, getDataLocalStorageByKey, NgSelectItem, TextboxItem } from "@shared-sm";

export const STT = () => new TextboxItem({
    key: 'stt',
    placeholder: 'stt',
    label: 'STT',
    value: '',
    layout: '50',
    required: true
});

export const CDL = () => new NgSelectItem({
    key: 'cdLon',
    placeholder: 'Cổ đông lớn',
    label: 'Cổ đông lớn',
    value: '',
    layout: '50',
    options: [],
    required: true
});

export const SO_DKSH = () => new TextboxItem({
    key: 'soDksh',
    placeholder: '',
    label: 'Số DKSH',
    value: '',
    layout: '50',
    required: true
});

export const NGAY_CAP = () => new DateTimeItem({
    key: 'ngayCap',
    placeholder: '',
    label: 'Ngày cấp',
    value: '',
    layout: '50',
});

export const NOI_CAP = () => new TextboxItem({
    key: 'noiCap',
    placeholder: '',
    label: 'Nơi cấp',
    value: '',
    layout: '50',
});

export const DIA_CHI = () => new TextboxItem({
    key: 'diaChi',
    placeholder: '',
    label: 'Địa chỉ',
    value: '',
    layout: '50',
});

export const TEN_NGUOI_DAI_DIEN_VON = () => new TextboxItem({
    key: 'tenNguoiDaiDienVon',
    placeholder: '',
    label: 'Tên người đại diện vốn',
    value: '',
    layout: '50',
});

export const CHUC_VU_TAI_DON_VI = () => new TextboxItem({
    key: 'chucVuTaiDonVi',
    placeholder: '',
    label: 'Chức vụ tại đơn vị',
    value: '',
    layout: '50',
});

export const CMT_NGUOI_DAI_DIEN = () => new TextboxItem({
    key: 'cmtNguoiDaiDien',
    placeholder: '',
    label: 'CMT người đại diện',
    value: '',
    layout: '50',
});

export const MOIQUANHE = () => new TextboxItem({
    key: 'moiQuanHe',
    placeholder: '',
    label: 'Mối quan hệ',
    value: '',
    layout: '50',
});

export const NGANH_NGHE = () => new TextboxItem({
    key: 'nganhNghe',
    placeholder: '',
    label: 'Ngành nghề',
    value: '',
    layout: '50',
});

export const NGAY_HIEU_LUC = () => new DateTimeItem({
    key: 'validDate',
    placeholder: '',
    label: 'Ngày hiệu lực',
    value: '',
    layout: '50',
});

export const PARENTID = () => new NgSelectItem({
    key: 'parentId',
    placeholder: 'Cổ đông lớn',
    label: 'Cổ đông lớn',
    value: '',
    layout: '50',
    options: [],
    required: true
 });
 
 export const DEMUC = () => new TextboxItem({
    key: 'deMuc',
    placeholder: 'Đề mục',
    label: 'Đề mục',
    value: '',
    layout: '50',
    required: true
 });
 
 export const CHUTHICHDEMUCDDL = () => new NgSelectItem({
    key: 'chuThichDeMuc',
    placeholder: 'Chú thích đề mục',
    label: 'Chú thích đề mục',
    value: '',
    layout: '50',
    required: true
 });
 
 export const NGUOINOIBO = () => new TextboxItem({
    key: 'nguoiNoiBo',
    placeholder: 'Người nội bộ',
    label: 'Người nội bộ',
    value: '',
    layout: '50',
    // required: true
 });
 
 export const CHUTHICHNGUOINOIBO = () => new TextboxItem({
    key: 'chuThichNguoiNoiBo',
    placeholder: 'Chú thich người nội bộ',
    label: 'Chú thích người nội bộ',
    value: '',
    layout: '50',
    // required: true
 });

export const NGUOILIENQUAN = () => new TextboxItem({
    key: 'nguoiLienQuan',
    placeholder: '',
    label: 'Người liên quan',
    value: '',
    layout: '50',
    required: true
});