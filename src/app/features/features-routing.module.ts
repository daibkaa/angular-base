import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../core/auth/auth.guard';
import { AtiLayoutComponent } from '../theme/ati-layout/ati-layout.component';

const routes: Routes = [
  { path: '', redirectTo: 'ati', pathMatch: 'full' },
  {
    path: 'ati',
    // component: AtiLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'shareholder-management', pathMatch: 'full' },
      {
        path: 'import',
        loadChildren: () => import('./shareholder/import/import.module').then(m => m.ImportModule),
      },
      {
        path: 'shareholder-management',
        loadChildren: () => import('./shareholder/shareholder-management/shareholder-management.module').then(m => m.ShareholderManagementModule),
      },
      {
        path: 'shareholder-major-management',
        loadChildren: () => import('./shareholder/shareholder-major-management/shareholder-major-management.module').then(m => m.ShareholderMajorManagementModule),
      },
      {
        path: 'shareholder-congress',
        loadChildren: () => import('./shareholder/shareholder-congress/shareholder-congress.module').then(m => m.ShareholderCongressModule),
      },
      {
        path: 'copy-share-dividend',
        loadChildren: () => import('./shareholder/copy-share-dividend/copy-share-dividend.module').then(m => m.CopyShareDividendModule),
      },
      {
        path: 'import-info',
        loadChildren: () => import('./shareholder/import-data/import-data.module').then(m => m.ImportDataModule),
      },
    ],
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
})
export class FeaturesRoutingModule { }
