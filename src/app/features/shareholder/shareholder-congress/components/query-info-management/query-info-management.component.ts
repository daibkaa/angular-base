import { Component, Injector, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ComponentAbstract, DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize, takeUntil } from 'rxjs/operators';
import { Status } from 'src/app/core';
import { dateNoneSeparation, removeEmptyObject } from 'src/app/core/utils/utils.function';
import { NHOM_CD, OWNER_REGISTRATIO_NUMBER, REPORT_DATE, TEN } from '../../form-setting/form-setting';
import { ShareholderCongressService } from '../../services/shareholder-congress.service';

@Component({
  selector: 'app-query-info-management',
  templateUrl: './query-info-management.component.html',
  styleUrls: ['./query-info-management.component.scss'],
})
export class QueryInfoManagementComponent extends ComponentAbstract {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  ownerRegisterNumber = OWNER_REGISTRATIO_NUMBER();
  fullName = TEN();
  groupType = NHOM_CD();
  reportDate = REPORT_DATE();

  displayedColumns: string[];
  listColumnGroupVande;
  listColumnGroupBauCu;

  public dataSource: MatTableDataSource<any>;
  public hasDataSource = false;
  
  constructor(
    protected _injector: Injector,
    private _shareholderManagementService: ShareholderCongressService,
  ) {
    super(_injector);
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.ownerRegisterNumber, this.fullName, this.groupType, this.reportDate]);
  }

  destroyData() {
  }

  search() {
    this.pageIndex = 0;
    this.pageSize = 10;
    this.searchReport();
  }

  searchReport(): void {
    this.validateAllFields(this.form);
    if(!this.form.valid) {
      this.dialogService.error({
        title: 'Thông báo',
        innerHTML: 'Vui lòng nhập thông tin tìm kiếm'
      });
      return;
    }
    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue()
    let params: any = {...removeEmptyObject(formData), pageNo: this.pageIndex,
      pageSize: this.pageSize}
    if(formData?.reportDate && formData?.reportDate != '') {
      params.reportDate = moment(formData.reportDate).format('YYYY-MM-DD')
    }
    this._shareholderManagementService.getInfo(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
      if (res?.code === Status.SUCCESS) {
        this.hasDataSource = true;
        const data = res.data.items;
        this.totalItem = res?.data?.totalCount
        let indexMax = 0;
        data.forEach((e, i) => {
          if(i > 0 && data[(i)] != {} && (Object.keys(data[(i)])?.length > Object.keys(data[(i - 1)])?.length)) {
            indexMax = i;
          }
        })
        this.dataSource = new MatTableDataSource(data);
        this.listColumnGroupVande = data[indexMax].thongTinVanDeDtos
        this.listColumnGroupBauCu = data[indexMax].bauCuDtoList
      }
    }, error => {
      this.totalItem = 0;
      this.displayedColumns = [];
      this.hasDataSource = false;
      this.dataSource = new MatTableDataSource([]);
    })
  }

  _renderItem(column: string, index) {
    return `${column}-${index}`
  }

  exportExcel() {
    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue()
    let params: any = {...removeEmptyObject(formData), pageNo: this.pageIndex,
      pageSize: this.pageSize}
    if(formData?.reportDate && formData?.reportDate != '') {
      params.reportDate = moment(formData.reportDate).format('YYYY-MM-DD')
    }
    this._shareholderManagementService.exportQueryInfo(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
        // this.openPDF(res.body)
        const blob = new Blob([res.body], { type: 'application/vnd.ms-excel' });
        const url= window.URL.createObjectURL(blob);
        let anchor = document.createElement("a");
        const downloadFile = `C5.TT_THAMDU_DHCD_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;

        anchor.download = downloadFile;
        anchor.href = url;
        anchor.click();
    })
  }

  /**
   * Event load data phân trang ((page)="ChanePageIndex($event);")
   */
  ChanePageIndex($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex;
    this.searchReport();
  }
  

}