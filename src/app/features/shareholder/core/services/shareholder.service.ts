import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { MbbCurrencyPipe } from '@shared-sm';
import { HttpOptions, KEY_VALUE_COLUMN, PATH } from 'src/app/core';
import { HttpClientService } from 'src/app/core/service/httpclient.service';
import { COLUMN_CURRENCY } from '../constant';

@Injectable()

export class ShareholderService {
  constructor(
    private httpClient: HttpClientService,
    private _mbbCurrencyPipe: MbbCurrencyPipe
    ) {
  }

  importValidateEvent(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.IMPORT.IMPORT_VALIDATE,
      body: data
    };
    return this.httpClient.put(options);
  }

  importDataEvent(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.IMPORT.IMPORT_DATA,
      body: data
    };
    return this.httpClient.post(options);
  }

  warningataEvent(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.IMPORT.WARNING,
      params: data
    };
    return this.httpClient.get(options);
  }

  _renderColumsTitle(title) {
    if(Object.keys(KEY_VALUE_COLUMN).includes(title)) {
      return KEY_VALUE_COLUMN[title]
    }
    return title;
  }

  _renderValueCurrency(value, column) {
    if(value) {
      if(COLUMN_CURRENCY.find(f => column.includes(f))) {
        return this._mbbCurrencyPipe.transform(value);
      }
      return value;
    }
    return null;
  }

}
