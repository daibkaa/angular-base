import { materialProviders } from './material-config';
import { TranslateLangService } from './translate-lang.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injector, NgModule, APP_INITIALIZER } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ActivityIndicatorModule, SharedSMModule } from '@shared-sm';
import { AppComponent } from './app.component';
import { ThemeModule } from './theme/theme.module';
import { AtiLayoutComponent } from './theme/ati-layout/ati-layout.component';
import { AuthGuard } from './core/auth/auth.guard';
import { MatTabsModule } from '@angular/material/tabs';
import { httpInterceptorProviders } from './core/interceptors';
import { ToastrModule } from 'ngx-toastr';
import { IframeComponent } from './public/iframe/iframe.component';

export function TranslateLangServiceFactory(translateLangService: TranslateLangService) {
  return () => translateLangService.load();
}

export function TranslateHttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json?nocache=' + (new Date()).getTime());
}

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./public/auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'form',
    component: IframeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: '',
    component: AtiLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Ati'
    },
    children: [{
      path: '',
        loadChildren: () => import('./features/features.module').then(m => m.FeaturesModule),
      },
    ]
  },
  
];

@NgModule({
  imports: [
    SharedSMModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ActivityIndicatorModule,
    ThemeModule,
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: TranslateHttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    ToastrModule.forRoot(),
  ],
  declarations: [
    AppComponent,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: TranslateLangServiceFactory,
      deps: [TranslateLangService],
      multi: true,
    },
    materialProviders,
    httpInterceptorProviders
  ],
  bootstrap: []
})
export class AppModule {

  constructor(private injector: Injector) {
  }

  ngDoBootstrap() {
    const ce = createCustomElement(AppComponent, { injector: this.injector });
    customElements.define('base-app-element', ce);
  }

}
