// import { APPROVER } from './../utils/constant';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { APPROVER, LocalStoreEnum, LocalStoreManagerService } from '@shared-sm';
// import { LocalStoreEnum } from '@shared/models/enum/local-store.enum';
import * as _ from 'lodash';
import { BehaviorSubject, Observable } from 'rxjs';
import { MENU_LIST } from '../utils/constant';
// import { LocalStoreManagerService } from './../service/local-store-manager.service';
export interface MenuTag {
  color: string; // Background Color
  value: string;
}

export interface MenuChildrenItem {
  route: string;
  name: string;
  type: 'link' | 'sub' | 'extLink' | 'extTabLink';
  icon?: string;
  children?: MenuChildrenItem[];
}

export interface Menu {
  route: string;
  name: string;
  type: 'link' | 'sub' | 'extLink' | 'extTabLink';
  icon?: string;
  label?: MenuTag;
  badge?: MenuTag;
  children?: MenuChildrenItem[];
}

export interface MenuParam {
  pId: number;
  customerCode: number;
  menuActive: MenuActive[];
}
export interface MenuActive {
  key: string;
  number: number;
  menuParam?: any[]
}

@Injectable({
  providedIn: 'root',
})
export class MenuService {
  private menu$: BehaviorSubject<Menu[]> = new BehaviorSubject<Menu[]>([]);


  menusDform: any;
  menusEsb: any;

  constructor(private localStore: LocalStoreManagerService, private router: Router) {

  }

  getAll(): Observable<Menu[]> {
    return this.menu$.asObservable();
  }

  set(menu: Menu[]): Observable<Menu[]> {
    this.menu$.next(menu);
    return this.menu$.asObservable();
  }

  add(menu: Menu) {
    const tmpMenu = this.menu$.value;
    tmpMenu.push(menu);
    this.menu$.next(tmpMenu);
  }

  get menu() {
    return this.menu$.value ? this.menu$.value : [];
  }

  reset() {
    this.menu$.next([]);
  }

  /**
   * Convert menu show html
   * @param menuServer;
   * @param parentId;
   */
  mapMenu(menuServer, parentId = 0) {
    const menusTree: any = [];
    let menuChild = menuServer.filter(x => x.parentId === parentId && x.active);
    menuChild = _.orderBy(menuChild, ['position'], ['asc']);
    (menuChild || []).forEach(element => {
      const menuSubChild = this.mapMenu(menuServer, element.id);
      if (!element.typeLink || element.typeLink === 'angularLink') {
        const menu: any = {
          route: '/' + element.route?.trim(),
          name: element.name,
          type: menuSubChild.length > 0 ? 'sub' : 'link',
          icon: element.icon,
          children: menuSubChild
        };
        menusTree.push(menu);
      } else {
        const menu: any = {
          route: element.route?.trim(),
          name: element.name,
          type: element.typeLink,
          icon: element.icon,
          children: menuSubChild
        };
        menusTree.push(menu);
      }
    });
    return menusTree;
  }

}
