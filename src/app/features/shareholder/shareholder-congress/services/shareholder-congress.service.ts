import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { HttpOptions, PATH } from 'src/app/core';
import { HttpClientService, Verbs } from 'src/app/core/service/httpclient.service';

@Injectable()

export class ShareholderCongressService {
  constructor(
    private httpClient: HttpClientService) {
  }

  importValidateEvent(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.IMPORT.IMPORT_VALIDATE,
      body: data
    };
    return this.httpClient.put(options);
  }

  importDataEvent(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.IMPORT.IMPORT_DATA,
      body: data
    };
    return this.httpClient.post(options);
  }

  getDataReport(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_CONGRESS.INFO_REPORT,
      params: data
    };
    return this.httpClient.get(options);
  }

  getInfo(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_CONGRESS.QUERY_INFO,
      params: data
    };
    return this.httpClient.get(options);
  }

  export(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_CONGRESS.EXPORT,
      params: data
    };
    return this.httpClient.download(Verbs.GET, options);
  }

  exportQueryInfo(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.SHARE_HOLDER_CONGRESS.EXPORT_INFO,
      params: data
    };
    return this.httpClient.download(Verbs.GET, options);
  }

}
