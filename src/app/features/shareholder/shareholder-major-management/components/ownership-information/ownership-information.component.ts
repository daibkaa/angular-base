import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ComponentAbstract, ComponentBaseAbstract, DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize, map, mergeMap, takeUntil } from 'rxjs/operators';
import { BC_TYPE, EXCEL_XLS, EXCEL_XLSX, HttpResponse, MessageSeverity, Status } from 'src/app/core';
import { checkFileType, dateNoneSeparation, removeEmptyObject } from 'src/app/core/utils/utils.function';
import { CDL_TYPE_ROMAN, COLUMN_BC_12, COLUMN_BC_8, REJECT_COLUMN_DISPLAYS } from '../../../core/constant';
import { REPORT_TYPE_MAJOR, REPORT_DATE_MAJOR, ORIGINAL_PERIOD, COMPARISON_PERIOD, REPORT_TYPE_OWNERSHIP } from '../../form-setting/form-setting';
import { ShareholderMajorManagementService } from '../../services/shareholder-major-management.service';

@Component({
  selector: 'app-ownership-information',
  templateUrl: './ownership-information.component.html',
  styleUrls: ['./ownership-information.component.scss'],
})
export class OwnershipInformationComponent extends ComponentAbstract {

  public dataSource: MatTableDataSource<any>;
  public hasDataSource = false;
  displayedColumns: string[];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  reportType = REPORT_TYPE_OWNERSHIP();
  reportDate = ORIGINAL_PERIOD();
  compareDate = COMPARISON_PERIOD();

  readonly BC_TYPE = BC_TYPE;

  compareDateSearch;
  reportDateSearch;
  reportTypeSearch;
  
  constructor(
    protected _injector: Injector,
    private _shareholderMajorManagementService: ShareholderMajorManagementService,
  ) {
    super(_injector);
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.reportType, this.reportDate, this.compareDate]);

  }

  searchReport(): void {
    this.validateAllFields(this.form);
    if(!this.form.valid) {
      this.dialogService.error({
        title: 'Thông báo',
        innerHTML: 'Vui lòng nhập thông tin tìm kiếm'
      });
      return;
    }
    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue();
    this.reportTypeSearch = formData?.reportType;
    let params: any = {
      reportType: formData?.reportType
    }
    if(formData?.reportDate && formData?.reportDate != '') {
      params.reportDate = moment(formData.reportDate).format('YYYY-MM-DD')
    }
    if(formData?.compareDate && formData?.compareDate != '') {
      params.compareDate = moment(formData.compareDate).format('YYYY-MM-DD')
    }
    this._shareholderMajorManagementService.getTransactionChange(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
      if (res && res.code === Status.SUCCESS) {
        const data = res?.data;
        if(data?.length) {
          this.hasDataSource = true;
          this.compareDateSearch = moment(formData.compareDate).format('DD/MM/YYYY');
          this.reportDateSearch = moment(formData.reportDate).format('DD/MM/YYYY');
        } else {
          this.hasDataSource = false;
          this.displayedColumns = [];
        }
        this.dataSource = new MatTableDataSource(data);
      } else {
        this.hasDataSource = false;
        this.displayedColumns = [];
        this.dataSource = new MatTableDataSource([]);
      }
    }, error => {
      this.displayedColumns = [];
      this.hasDataSource = false;
      this.dataSource = new MatTableDataSource([]);
    })
  }

  exportExcel() {
    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue()
    let params: any = {
      reportType: this.reportTypeSearch,
      compareDate: moment(this.compareDateSearch, 'DD/MM/YYYY').format('YYYY-MM-DD'),
      reportDate: moment(this.reportDateSearch, 'DD/MM/YYYY').format('YYYY-MM-DD')
    }
    this._shareholderMajorManagementService.exportC3C4(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
        // this.openPDF(res.body)
        const blob = new Blob([res.body], { type: 'application/vnd.ms-excel' });
        const url= window.URL.createObjectURL(blob);
        let anchor = document.createElement("a");
        let downloadFile = '';
        switch(formData.reportType) {
          case BC_TYPE.CD_LON: {
            downloadFile = `C4.THONG_TIN_SO_HUU_CĐLON_NLQ_${this.compareDateSearch}_${this.reportDateSearch}.xlsx`;
          }; break;
          default: {
            downloadFile = `C3.THONG_TIN_SO_HUU_${this.compareDateSearch}_${this.reportDateSearch}.xlsx`;
          }; break;
        }

        anchor.download = downloadFile;
        anchor.href = url;
        anchor.click();
    })
  }

  destroyData() {
  }

}