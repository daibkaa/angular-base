import { DateTimeItem, NgSelectItem, TextboxItem } from "@shared-sm";

export const STT = () => new TextboxItem({
   key: 'stt',
   placeholder: 'Chỉ mục',
   label: 'Chỉ mục',
   value: '',
   layout: '50',
   required: true
});

export const NGAY_HIEU_LUC = () => new DateTimeItem({
   key: 'validDate',
   placeholder: '',
   label: 'Ngày hiệu lực',
   value: '',
   layout: '50',
});