import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { HttpOptions, PATH } from 'src/app/core';
import { HttpClientService, Verbs } from 'src/app/core/service/httpclient.service';

@Injectable()

export class CopyShareDividendService {
  constructor(
    private httpClient: HttpClientService) {
  }

  export(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.EXPORT.EXPORT_EXCEL,
      params: data
    };
    return this.httpClient.download(Verbs.GET, options);
  }

  search(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.EXPORT.SEARCH,
      params: data
    };
    return this.httpClient.get(options);
  }

}
