import { ChangeDetectorRef, Component, Inject, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ComponentAbstract } from '@shared-sm';
import * as moment from 'moment';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { IS_ADD_NEW } from '../../form-setting/add-new';
import { CDL, CHUC_VU_TAI_DON_VI, CMT_NGUOI_DAI_DIEN, DIA_CHI, NGANH_NGHE, NGAY_CAP, NGAY_HIEU_LUC, NOI_CAP, SO_DKSH, STT, TEN_NGUOI_DAI_DIEN_VON } from '../../form-setting/add-new-cdl';

@Component({
  selector: 'app-add-new-b13',
  templateUrl: './add-new-b13.component.html',
  styleUrls: ['./add-new-b13.component.scss']
})
export class AddNewB13Component extends ComponentAbstract {

  isAddNew = IS_ADD_NEW();
  
  stt = STT();
  cdl = CDL();
  soDksh = SO_DKSH();
  ngayCap= NGAY_CAP();
  noiCap = NOI_CAP();
  diaChi = DIA_CHI();
  tenNguoiDaiDienVon = TEN_NGUOI_DAI_DIEN_VON();
  chucVuTaiDonVi = CHUC_VU_TAI_DON_VI();
  cmtNguoiDaiDien = CMT_NGUOI_DAI_DIEN();
  nganhNghe = NGANH_NGHE();
  ngayHieuLuc = NGAY_HIEU_LUC();

  constructor(
    protected injector: Injector,
    public dialogRef: MatDialogRef<AddNewB13Component>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {
    super(injector);
  }
  checked = false;

  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.stt, this.cdl, this.soDksh, this.ngayCap, this.noiCap, this.diaChi, this.tenNguoiDaiDienVon, this.chucVuTaiDonVi, this.cmtNguoiDaiDien, this.nganhNghe]);
    
    if(this.data?.data?.id) {
      this.form.patchValue(this.data.data)
    }
  }

  saveData() {
    this.validateAllFields(this.form);
    if (!this.form.valid) {
      this.dialogService.error({
        title: 'dialog.notification',
        message: 'dialog.valid-error',
      }, res => {
        if (res) {
        }
      });
      return;
    }

    
    const formData = this.form.getRawValue()
    let paramDialog = {
      data: {
        ...formData,
        id: this.data.data?.id || null
      },
    }
    this.dialogRef.close(paramDialog)

  }

  closeDialog() {
    if (this.dialogRef.close) { this.dialogRef.close(null); }
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  testData() {
    const data = {
      cdLon: "CDL test",
      diaChi: 'Dia chi test',
      id: null,
      moiQuanHe: null,
      nganhNghe: 'Nganh nghe test',
      ngayCap: "2004-10-07",
      noiCap: "118 Hưng Phú, P.8, Q.8, Tp.HCM",
      parentId: null,
      reportDate: "2022-05-09",
      soDksh: "0300450962",
      stt: "III",
      tenNguoiDaiDienVon: 'Nguoi dai dien test',
      cmtNguoiDaiDien: '142723338',
      validDate: '2022-06-01'
    }
    this.form.patchValue(data)
  }

}
