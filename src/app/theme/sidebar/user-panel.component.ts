import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-panel',
  template: `
    <div fxLayout="column" fxLayoutAlign="start start">
      <img
        class="dform-user-panel-avatar"
        src="assets/images/logo.png"
        alt="avatar"
      />
      <img
        class="dform-item-panel-avatar"
        src="assets/images/logo.png"
        alt="avatar"
      />
    </div>
  `,
  styleUrls: ['./user-panel.component.scss'],
})
export class UserPanelComponent {
  constructor(private router: Router) {
  }
}
