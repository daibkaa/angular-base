import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivityIndicatorSingletonService } from '@shared-sm';

declare const require: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'ati_qldt';

  ngVersion = require('../../package.json').dependencies['@angular/core'];

  constructor(private router: Router,
              private indicator: ActivityIndicatorSingletonService,
  ) {
  }

  ngOnInit(): void {
    this.router.navigateByUrl(location.pathname.substr(1));
    window.addEventListener('popstate', () => {
      this.router.navigateByUrl(location.pathname.substr(1));
    });
  }

  sellClick(): void {
    this.indicator.showActivityIndicator();
  }
}
