import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { AuthGuard } from '@core';
import { NotFoundComponent } from './not-found/not-found.component';
import { IframeComponent } from './iframe/iframe.component';
import { AuthGuard } from '../core/auth/auth.guard';

const routes: Routes = [
  { path: 'login', redirectTo: 'auth', pathMatch: 'full' },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'form',
    component: IframeComponent,
    canActivate: [AuthGuard],
  },
  // {
  //   path: 'print-qr',
  //   component: PrintQrComponent,
  //   canActivate: [AuthGuard],
  // },
  {
    path: 'error',
    component: NotFoundComponent,
    data: { error: 'Đã xảy ra lỗi' }
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: false, enableTracing: false, onSameUrlNavigation: 'reload' }),
  ],
  exports: [RouterModule],
})

export class PublicRoutingModule { }
