import { ChangeDetectorRef, Component, Inject, Injector } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { ComponentAbstract } from '@shared-sm';
import * as moment from 'moment';
import { CHUTHICHNGUOINOIBO, NGAY_HIEU_LUC, NGUOINOIBO, PARENTID, STT, TENTCCN } from '../../form-setting/demuc-b14';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-add-new-demuc-b14',
  templateUrl: './add-new-demuc-b14.component.html',
  styleUrls: ['./add-new-demuc-b14.component.scss']
})
export class AddNewDeMucB14Component extends ComponentAbstract {
  
  PARENTID = PARENTID();
  STT = STT();
  TENTCCN = TENTCCN();
  NGUOINOIBO = NGUOINOIBO();
  CHUTHICHNGUOINOIBO = CHUTHICHNGUOINOIBO();
  NGAY_HIEU_LUC = NGAY_HIEU_LUC();
  chiMuc;

  constructor(
    protected injector: Injector,
    public dialogRef: MatDialogRef<AddNewDeMucB14Component>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {
    super(injector);
  }
  checked = false;

  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.PARENTID, this.STT, this.TENTCCN, this.NGUOINOIBO, this.CHUTHICHNGUOINOIBO]);

    this.PARENTID.options = ([...this.data.listData] || []).filter(f => (!f?.parentId && f?.soDksh)).map(m => {
      return {
        key: m.id,
        value: m.tenTcCn
      }
    });
    if(this.data?.data?.id) {
      this.form.patchValue(this.data.data)
      this.form.get(this.PARENTID.key).disable();
    }
    this.onChangeParentId();
  }

  onChangeParentId() {
    this.form.get(this.PARENTID.key).valueChanges.pipe(
      takeUntil(this.ngUnsubscribe),
      distinctUntilChanged()).subscribe(value => {
        if(value) {
          this.chiMuc = this.data.listData.find(f => f.id == value).chiMuc;
        } else {
          this.chiMuc = null;
        }
        
      })
  }

  saveData() {
    this.validateAllFields(this.form);
    if (!this.form.valid) {
      this.dialogService.error({
        title: 'dialog.notification',
        message: 'dialog.valid-error',
      }, res => {
        if (res) {
        }
      });
      return;
    }

    
    const formData = this.form.getRawValue()
    let paramDialog = {
      data: {
        ...formData,
        id: this.data.data?.id || null,
        chiMuc: this.data.data?.id ? this.data.data?.chiMuc : this.chiMuc
      },
    }
    this.dialogRef.close(paramDialog)

  }

  closeDialog() {
    if (this.dialogRef.close) { this.dialogRef.close(null); }
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

}
