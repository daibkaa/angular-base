import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MbbCurrencyPipe, SharedSMModule } from '@shared-sm';
import { PermissionCatalogUtilitiesGuard } from 'src/app/core/auth/permission-catalog-utilities.guard';
import { DragDirective } from 'src/app/shared/directives/dragDrop.directive';

import { AppImportDataComponent } from './components/import-data/import-data.component';
import { TableB13Component } from './components/table-B13/table-B13.component';
import { TableB14Component } from './components/table-B14/table-B14.component';
import { TableB15B16Component } from './components/table-B15-B16/table-B15-B16.component';
import { TableBC12Component } from './components/table-BC12/table-BC12.component';
import { TableBC13Component } from './components/table-BC13/table-BC13.component';
import { TableBC14Component } from './components/table-BC14/table-BC14.component';
import { TableBC15Component } from './components/table-bc15/table-bc15.component';
import { TableBC7Component } from './components/table-BC7/table-BC7.component';
import { TableBC8Component } from './components/table-bc8/table-bc8.component';
import { TableBC9BC11Component } from './components/table-BC9-BC11/table-BC9-BC11.component';
import { TableC5Component } from './components/table-C5/table-C5.component';
import { TableInfoShareholdeComponentMajor } from './components/table-info-shareholder-major/table-info-shareholder-major.component';
import { TableInfoShareholdeComponent } from './components/table-info-shareholder/table-info-shareholder.component';
import { TableWarningComponent } from './components/table-warning/table-warning.component';

import { ShareholderService } from './services/shareholder.service';
// #component

const COMPONENTS = [
  TableInfoShareholdeComponent,
  AppImportDataComponent,
  TableInfoShareholdeComponentMajor,
  TableBC15Component,
  TableBC8Component,
  TableWarningComponent,
  TableB13Component,
  TableB14Component,
  TableB15B16Component,
  TableBC14Component,
  TableBC13Component,
  TableBC9BC11Component,
  TableBC12Component,
  TableBC7Component,
  TableC5Component
];

const COMPONENTS_DYNAMIC = [];
const SERVICE = [ShareholderService];
const PIPES = [MbbCurrencyPipe];
const DIRECTIVES = [DragDirective];

@NgModule({
  imports: [MatTableModule, SharedSMModule, CommonModule, MatIconModule, MatTooltipModule],
  declarations: [...COMPONENTS, ...COMPONENTS_DYNAMIC, ...PIPES, ...DIRECTIVES],
  entryComponents: COMPONENTS_DYNAMIC,
  providers: [...SERVICE, ...PIPES, PermissionCatalogUtilitiesGuard],
  exports:  [...COMPONENTS, ...COMPONENTS_DYNAMIC, ...PIPES, ...DIRECTIVES],
})
export class ShareHolderCoreModule { }
