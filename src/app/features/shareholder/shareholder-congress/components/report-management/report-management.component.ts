import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ComponentAbstract, DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize, takeUntil, distinctUntilChanged } from 'rxjs/operators';
import { BC_TYPE, Status } from 'src/app/core';
import { dateNoneSeparation } from 'src/app/core/utils/utils.function';
import { REJECT_COLUMN_DISPLAYS } from '../../../core/constant';
import { DATA_BC8 } from '../../constanst';
import { COMPARE_DATE, REPORT_DATE, REPORT_TYPE } from '../../form-setting/form-setting';
import { ShareholderCongressService } from '../../services/shareholder-congress.service';

@Component({
  selector: 'app-report-management',
  templateUrl: './report-management.component.html',
  styleUrls: ['./report-management.component.scss'],
})
export class ReportManagementComponent extends ComponentAbstract {

  public dataSource: MatTableDataSource<any>;
  public hasDataSource = false;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  reportType = REPORT_TYPE();
  reportDate = REPORT_DATE();
  compareDate = COMPARE_DATE();

  displayedColumns: string[];
  showtable;
  listColumnGroup;
  isShowCompareDate: boolean = false;

  readonly BC_TYPE = BC_TYPE;
  
  constructor(
    protected _injector: Injector,
    private _shareholderManagementService: ShareholderCongressService,
  ) {
    super(_injector);
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.reportType, this.reportDate, this.compareDate]);
    this.onChangeReportType();

  }

  onChangeReportType() {
    this.form.get(this.reportType.key).valueChanges.pipe(
      takeUntil(this.ngUnsubscribe),
      distinctUntilChanged()).subscribe(value => {
        if(value == BC_TYPE.BC7) {
          this.isShowCompareDate = true;
        } else {
          this.isShowCompareDate = false;
        }
      })
  }

  destroyData() {
  }

  search() {
    this.pageIndex = 0;
    this.pageSize = 10;
    this.searchReport();
  }

  searchReport(): void {
    
    this.validateAllFields(this.form);
    if(!this.form.valid) {
      this.dialogService.error({
        title: 'Thông báo',
        innerHTML: 'Vui lòng nhập thông tin tìm kiếm'
      });
      return;
    }

    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue()
    let params: any = {
      reportType: formData?.reportType,
      pageNo: this.pageIndex,
      pageSize: this.pageSize
    }
    if(formData?.reportDate && formData?.reportDate != '') {
      params.reportDate = moment(formData.reportDate).format('YYYY-MM-DD')
    }
    if(formData?.compareDate && formData.compareDate != '' && this.isShowCompareDate) {
      params.compareDate = moment(formData.compareDate).format('YYYY-MM-DD')
    }

    this._shareholderManagementService.getDataReport(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
      if (res?.code === Status.SUCCESS) {
        this.showtable = formData.reportType;
        const tmpData = formData?.reportType == BC_TYPE.BC5 ? res.data.items.sort((a,b) => Number(a.stt) - Number(b.stt)) : res.data;
        const data = tmpData;
        if(formData.reportType == BC_TYPE.BC8) {
          const colums = data.reduce((a, o) => {
            let tmpItem = {};
            o.vanDeDtoList.reduce((obj, item, index) => {
              Object.keys(item).forEach(m => {
                if(m != 'loaiVd') {
                  tmpItem[this._renderItem(m, index)] = item[m];
                }
              })
              obj = {...tmpItem}
              return obj;
            }, {})
            let item = {...o};
            delete item.vanDeDtoList;
            const newItem = {
              stt: item.stt,
              nhomCoDong: item.nhomCoDong,
              soDksh: item.soDksh,
              cpDaiDien: item.cpDaiDien,
              cpThamDu: item.cpThamDu,
              tyLeThamDu: item.tyLeThamDu,
              ...tmpItem,
            }
            a.push(newItem)
            return a
          }, [])

          let indexMax = 0;
          colums.forEach((e, i) => {
            if(i > 0 && colums[(i)] != {} && (Object.keys(colums[(i)])?.length > Object.keys(colums[(i - 1)])?.length)) {
              indexMax = i;
            }
          })
          this.dataSource = new MatTableDataSource(colums);
          this.listColumnGroup = data[indexMax].vanDeDtoList
          this.displayedColumns = colums.length ? Object.keys(colums[indexMax]) : [];
          this.displayedColumns = (this.displayedColumns || []).filter(f => !REJECT_COLUMN_DISPLAYS.includes(f));

        } else {
          this.dataSource = new MatTableDataSource(data);
          this.displayedColumns = data.length ? Object.keys(data[0]) : [];
          this.displayedColumns = (this.displayedColumns || []).filter(f => !REJECT_COLUMN_DISPLAYS.includes(f));
        }
        this.hasDataSource = data.length ? true : false;
        this.totalItem = res.data.totalCount
        
      }
    }, error => {
      this.showtable = null;
      this.displayedColumns = [];
      this.hasDataSource = false;
      this.dataSource = new MatTableDataSource([]);
      this.totalItem = 0;
    })
  }


  /**
 * Event load data phân trang ((page)="ChanePageIndex($event);")
 */
  ChanePageIndex($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex;
    this.searchReport();
  }

  exportExcel() {
    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue()
    let params: any = {
      reportType: formData?.reportType,
      pageNo: this.pageIndex,
      pageSize: this.pageSize
    }
    if(formData?.reportDate && formData?.reportDate != '') {
      params.reportDate = moment(formData.reportDate).format('YYYY-MM-DD')
    }
    if(formData?.compareDate && formData.compareDate != '' && this.isShowCompareDate) {
      params.compareDate = moment(formData.compareDate).format('YYYY-MM-DD')
    }
    this._shareholderManagementService.export(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
        // this.openPDF(res.body)
        const blob = new Blob([res.body], { type: 'application/vnd.ms-excel' });
        const url= window.URL.createObjectURL(blob);
        let anchor = document.createElement("a");
        let downloadFile = '';
        switch(formData.reportType) {
          case BC_TYPE.BC5: {
            downloadFile = `BC5.MBB_DHCD_SAU_UQ_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;
          }; break;
          case BC_TYPE.BC6: {
            downloadFile = `BC6.TONG_HOP_DHCD_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;
          }; break;
          case BC_TYPE.BC7: {
            downloadFile = `BC7.THAMDU_DHCD_${dateNoneSeparation(params.reportDate, '-')}_${dateNoneSeparation(params.compareDate, '-')}.xlsx`;
          }; break;
          case BC_TYPE.BC8: {
            downloadFile = `BC8.CHI_TIET_BQVD_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;
          }; break;
        }

        // const nameFile = formData?.saoKe == '1' ? `BC14.SAO_KE_COTUC_TM.xlsx` : `BC13.SAO_KE_CP.xlsx`

        anchor.download = downloadFile;
        anchor.href = url;
        anchor.click();
    })
  }

  _renderItem(column: string, index) {
    return `${column}-${index}`
  }

}