import { Component, Injector, ViewChild } from '@angular/core';
import { ComponentBaseAbstract } from '@shared-sm';
import { Observable } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-shareholder-management',
  templateUrl: './shareholder-management.component.html',
  styleUrls: ['./shareholder-management.component.scss']
})
export class ShareholderManagementComponent extends ComponentBaseAbstract {

  activeTab = 0;
  constructor(
    protected injector: Injector,
  ) {
    super(injector);
  }

  protected componentInit() {
  }

  onTabChanged($event) {
  }

}
