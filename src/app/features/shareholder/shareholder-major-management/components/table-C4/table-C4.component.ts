import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';

@Component({
  selector: 'app-table-C4',
  templateUrl: './table-C4.component.html',
  styleUrls: ['./table-C4.component.scss'],
})
export class TableC4Component implements OnInit {

  @Input() dataSource: MatTableDataSource<any>;
  @Input() hasDataSource = false;
  displayedColumns: string[] = ['stt', 'cdLon', 'nguoiLienQuan', 'soDksh', 'ngayCap', 'noiCap', 'diaChi', 'moiQuanHe', 'soLuongCompareDate', 'tyLeCompareDate', 'soLuongReportDate', 'tyLeReportDate', 'thayDoiTrongKy'];

  @Input() compareDate;
  @Input() reportDate;

  constructor(
  ) {
  }

  // Xử lý dữ liệu đầu vào
  ngOnInit(): void {}

  destroyData() {
  }

}