import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { HttpOptions, PATH } from 'src/app/core';
import { HttpClientService, Verbs } from 'src/app/core/service/httpclient.service';

@Injectable()

export class ImportInfoService {
  constructor(
    private httpClient: HttpClientService) {
  }

  getDataReportInfo(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.IMPORT.IMPORT_DATA,
      params: data
    };
    return this.httpClient.get(options);
  }

  downloadImport(data: any) {
    const options: HttpOptions = {
      url: environment.urlAuth,
      path: PATH.IMPORT.DOWNLOAD,
      params: data
    };
    return this.httpClient.download(Verbs.GET, options);
  }

}
