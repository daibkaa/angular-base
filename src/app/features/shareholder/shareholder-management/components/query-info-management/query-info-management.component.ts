import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ComponentAbstract, ComponentBaseAbstract, DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import * as moment from 'moment';
import { distinctUntilChanged, finalize, map, mergeMap, takeUntil } from 'rxjs/operators';
import { EXCEL_XLS, EXCEL_XLSX, HttpResponse, Status } from 'src/app/core';
import { checkFileType, dateNoneSeparation, removeEmptyObject } from 'src/app/core/utils/utils.function';
import { REJECT_COLUMN_DISPLAYS } from '../../../core/constant';
import { ADDRESS, COMPARE_DATE, FULL_NAME, GROUP_TYPE, OWNER_REGISTRATIO_NUMBER, REPORT_DATE, REPORT_TYPE } from '../../form-setting/form-setting';
import { ShareholderManagementService } from '../../services/shareholder-management.service';

@Component({
  selector: 'app-query-info-management',
  templateUrl: './query-info-management.component.html',
  styleUrls: ['./query-info-management.component.scss'],
})
export class QueryInfoManagementComponent extends ComponentAbstract {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  ownerRegisterNumber = OWNER_REGISTRATIO_NUMBER();
  fullName = FULL_NAME();
  address = ADDRESS();
  groupType = GROUP_TYPE();
  reportDate = REPORT_DATE();
  compareDate = COMPARE_DATE();

  displayedColumns: string[];

  isShowCompareDate: boolean = false;

  public dataSource: MatTableDataSource<any>;
  public hasDataSource = false;
  
  constructor(
    protected _injector: Injector,
    private _shareholderManagementService: ShareholderManagementService,
  ) {
    super(_injector);
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.ownerRegisterNumber, this.fullName, this.address, this.groupType, this.reportDate, this.compareDate]);
    this.onChangeReportType();
  }

  onChangeReportType() {
    this.form.get(this.groupType.key).valueChanges.pipe(
      takeUntil(this.ngUnsubscribe),
      distinctUntilChanged()).subscribe(value => {
        console.log(value, 'value')
        if(value?.includes('CO_DONG_CO_GIAO_DICH_LON')) {
          this.isShowCompareDate = true;
        } else {
          this.isShowCompareDate = false;
        }
      })
  }

  destroyData() {
  }

  searchReport(): void {
    this.validateAllFields(this.form);
    if(!this.form.valid) {
      this.dialogService.error({
        title: 'Thông báo',
        innerHTML: 'Vui lòng nhập thông tin tìm kiếm'
      });
      return;
    }
    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue()
    let params: any = {
      ...removeEmptyObject(formData),
      pageNo: this.pageIndex,
      pageSize: this.pageSize
    }

    if(formData?.reportDate && formData?.reportDate != '') {
      params.reportDate = moment(formData.reportDate).format('YYYY-MM-DD')
    }
    if(formData?.compareDat && formData?.compareDate != '' && this.isShowCompareDate) {
      params.compareDate = moment(formData.compareDate).format('YYYY-MM-DD')
    }
    this._shareholderManagementService.getInfo(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
      if (res?.code === Status.SUCCESS) {
        this.hasDataSource = true;
        const data = res.data.items.map(m => {
          return {
            ...m,
            stt: (this.pageIndex * this.pageSize) + Number(m.stt)
          }
        });
        this.displayedColumns = data.length ? Object.keys(data[0]) : [];
        this.displayedColumns = (this.displayedColumns || []).filter(f => !REJECT_COLUMN_DISPLAYS.includes(f));
        this.dataSource = new MatTableDataSource(data);
        this.totalItem = res.data.totalCount
      }
    }, error => {
      this.displayedColumns = [];
      this.hasDataSource = false;
      this.dataSource = new MatTableDataSource([]);
    })
  }

  /**
   * Event load data phân trang ((page)="ChanePageIndex($event);")
   */
  ChanePageIndex($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex;
    this.searchReport();
  }

  exportExcel() {
    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue()
    let params: any = {
      ...removeEmptyObject(formData),
      pageNo: this.pageIndex,
      pageSize: this.pageSize
    }

    if(formData?.reportDate && formData?.reportDate != '') {
      params.reportDate = moment(formData.reportDate).format('YYYY-MM-DD')
    }
    if(formData?.compareDate && formData?.compareDate != '' && this.isShowCompareDate) {
      params.compareDate = moment(formData.compareDate).format('YYYY-MM-DD')
    }
    this._shareholderManagementService.exportInfo(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
        // this.openPDF(res.body)
        const blob = new Blob([res.body], { type: 'application/vnd.ms-excel' });
        const url= window.URL.createObjectURL(blob);
        let anchor = document.createElement("a");

        const nameFile = formData?.groupType.includes('CO_DONG_CO_GIAO_DICH_LON') ? `BC2.GDLON_${dateNoneSeparation(params.reportDate, '-')}_${dateNoneSeparation(params.compareDate, '-')}.xlsx` : `BC1.MBB_TONG_HOP_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;
        const downloadFile = nameFile

        anchor.download = downloadFile;
        anchor.href = url;
        anchor.click();
    })
  }
  

}