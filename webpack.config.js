const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
    module.exports = {
      output: {
        publicPath: "auto",
        uniqueName: "base_app"
      },
      optimization: {
        // Only needed to bypass a temporary bug
        runtimeChunk: false
      },
      plugins: [
        new ModuleFederationPlugin({
            // For remotes (please adjust)
            name: "base_app",
            library: { type: "var", name: "base_app" },
            filename: "remoteEntry.js",
            exposes: {
                './web-components': './src/bootstrap.ts',
            },
            shared: {
              "@angular/core":  {
                singleton: true
              },
              "@angular/common": {
                singleton: true
              },
              "@angular/router": {
                singleton: true
              }
            }
          })
      ],
};
