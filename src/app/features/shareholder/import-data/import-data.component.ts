import { Component, Injector, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Status } from '@core';
import { ComponentAbstract } from '@shared-sm';
import * as moment from 'moment';
import { finalize, takeUntil } from 'rxjs/operators';
import { CREATED_BY, FILE_NAME, GROUP_TYPE, IMPORT_DATE_FROM, IMPORT_DATE_TO } from './form-setting/form-setting';
import { ImportInfoService } from './services/import-info.service';

@Component({
  selector: 'app-import-info',
  templateUrl: './import-data.component.html',
  styleUrls: ['./import-data.component.scss']
})
export class ImportInfoComponent extends ComponentAbstract {

  createdBy = CREATED_BY();
  fileName = FILE_NAME();
  importDateFrom = IMPORT_DATE_FROM();
  importDateTo = IMPORT_DATE_TO();
  groupType = GROUP_TYPE();

  displayedColumns: string[] = ['stt', 'fileName', 'importDate', 'createdBy', 'isWarning', 'note', 'action'];
  dataSource: MatTableDataSource<any>;
  hasDataSource = false;

  constructor(
    protected injector: Injector,
    private _importInfoService: ImportInfoService
  ) {
    super(injector);
  }

  protected componentInit() {
    this.form = this.itemControl.toFormGroup([this.createdBy, this.fileName, this.importDateFrom, this.importDateTo, this.groupType]);
    this.searchReport();
  }

  search() {
    this.pageIndex = 0;
    this.searchReport();
  }

  searchReport() {
    this.validateAllFields(this.form);
    if(!this.form.valid) {
      this.dialogService.error({
        title: 'Thông báo',
        innerHTML: 'Vui lòng nhập thông tin tìm kiếm'
      });
      return;
    }

    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue()
    let params: any = {
      pageNo: this.pageIndex,
      pageSize: this.pageSize
    }
    if(formData?.createdBy && formData.createdBy != '') {
      params.createdBy = formData?.createdBy
    }
    if(formData?.fileName && formData.fileName != '') {
      params.fileName = formData.fileName
    }
    if(formData?.isWarning && formData.isWarning != '') {
      params.isWarning = formData.isWarning
    }
    if(formData?.importDateTo && formData.importDateTo != '') {
      params.importDateTo = moment(formData.importDateTo).format('YYYY-MM-DD')
    }
    if(formData?.importDateFrom && formData.importDateFrom != '') {
      params.importDateFrom = moment(formData.importDateFrom).format('YYYY-MM-DD')
    } else {
      params.importDateFrom = ''
    }
    if(formData?.importDateTo && formData.importDateTo != '') {
      params.importDateTo = moment(formData.importDateTo).format('YYYY-MM-DD')
    }
    this._importInfoService.getDataReportInfo(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
        if (res?.code === Status.SUCCESS && res?.data?.items?.length) {
          this.hasDataSource = true;
          this.totalItem = res.data.totalCount
          const page = this.pageIndex * this.pageSize;
          const data = res.data.items.map((item, index) => {
            return {
              ...item,
              stt: page + index + 1
            }
          });
          this.dataSource = new MatTableDataSource(data);
        } else {
          this.hasDataSource = false;
          this.dataSource = new MatTableDataSource([]);
          this.totalItem = 0;
        }
    }, error => {
      this.hasDataSource = false;
      this.dataSource = new MatTableDataSource([]);
      this.totalItem = 0;
    })
  }

    /**
   * Event load data phân trang ((page)="ChanePageIndex($event);")
   */
  ChanePageIndex($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex;
    this.searchReport();
  }

  onDownload(element) {
    const param = {
      id: element.id
    }
    this._importInfoService.downloadImport(param).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
        const blob = new Blob([res.body], { type: 'application/vnd.ms-excel' });
        const url= window.URL.createObjectURL(blob);
        let anchor = document.createElement("a");
        anchor.download = element.fileName;
        anchor.href = url;
        anchor.click();
      }, error => {
        console.log(error, 'err')
      })
  }

}
