import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class CacheProcess {
    processId: number | string = 0;
    customerCode: number | string = 0;

    constructor() {
    }
    getpId(): number | string {
        return this.processId;
    }

    setpId(processId: number | string) {
        this.processId = processId;
    }

    clearpId(){
        this.processId = 0;
    }
}