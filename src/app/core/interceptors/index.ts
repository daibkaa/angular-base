import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { JWTResponseInterceptor } from './jwt-response.interceptor';

/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: JWTResponseInterceptor, multi: true }
];
