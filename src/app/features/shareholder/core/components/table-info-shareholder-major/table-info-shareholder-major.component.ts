import { Component, Input, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DformPaginationPlusComponent } from '@shared-sm';
import { ShareholderService } from '../../services/shareholder.service';

@Component({
  selector: 'app-table-info-shareholder-major',
  templateUrl: './table-info-shareholder-major.component.html',
  styleUrls: ['./table-info-shareholder-major.component.scss'],
})
export class TableInfoShareholdeComponentMajor {

  @Input() dataSource: MatTableDataSource<any>;
  @Input() hasDataSource = false;
  @Input() displayedColumns: string[];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  constructor(
    public _shareholderService: ShareholderService,
  ) {
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
  }

  destroyData() {
  }


}