// import { URL_GET_MANAGE, RETAIL_MENU } from './../utils/constant';
import { HttpHeaders } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '@env/environment';
import { ActivityIndicatorSingletonService } from '@shared-sm';
// import { Store } from '@ngrx/store';
import * as _ from 'lodash';
import * as moment from 'moment';
import { forkJoin, of } from 'rxjs';
import { catchError, finalize, map, take, tap, timeout } from 'rxjs/operators';
import { MenuService } from '../bootstrap/menu.service';
import { LocalStoreEnum, PATH, Status, URL_GET_ALL, URL_LOGIN } from '../utils/constant';
import { DialogManagementService } from './dialog-management.service';
import { HttpClientService } from './httpclient.service';
import { JwtHelper } from './jwt-helper';
import { LocalStoreManagerService } from './local-store-manager.service';
import { Login } from './model/login.dto';
import { HttpOptions } from './model/request.base.dto';
import { HttpInterface } from './model/response.base.dto';
// import { MessageSeverity, ToastService } from './toast.service';
// import { CATALOG, reloadCatalog } from './utils.service';
// import { Idle } from 'idlejs';

/**
 * description: Xử lý thông tin token và catalog
 */

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    private readonly STORAGE: string[] = ['JWT', 'RJWT', 'TE'];
    private timeoutExporation: any;
    private timeoutCatalog: any;
    readonly timeoutScreens = 600000;
    currentLang = 'vi';
    private idleJobIsRunning = false;
    constructor(
        private router: Router,
        private httpClient: HttpClientService,
        private localStore: LocalStoreManagerService,
        private dialogManagementService: DialogManagementService,
        private menu: MenuService,
        // private dialogService: DformDialogService,
        private indicator: ActivityIndicatorSingletonService,
        // private toastr: ToastService,
        private ngZone: NgZone,
    ) {
        // this.idleJob = this.setSessionLocalExpired(new Idle());
        if (this.tokenValid()) {
            this.startIdle();
        }
    }

    /**
     * Xử lý login vào ứng dụng lưu token
     * @param authentication;
     */
    public login(authentication: string) {
        if (authentication) {
            const headers = new HttpHeaders({
                Authorization: `Basic ${authentication}`,
            });
            const opstions: HttpOptions = {
                path: PATH.GENERATE_TOKEN,
                url: URL_LOGIN,
                headers,
                params: {
                    grantType: 'PASSWORD_GRANT',
                },
                isAuthentication: false
            };
            return this.httpClient
                .get(opstions)
                .pipe(
                    take(1),
                    tap((response: HttpInterface<Login>) => this.storeSession(response.data)),
                    map((response: HttpInterface<Login>) => {
                        this.startRefreshTokenTimer();
                        return response;
                    })
                );
        }
    }

    /**
     * @description Lưu chữ thông tin liên quan đến session của người dùng
     * Lưu thông tin người dùng
     * Lưu thông tin sessionId
     * Lưu thông tin của menulist trả về khi login
     * Lưu thông tin catalog
     * Lưu thời gian token
     */
    storeSession(data: Login, isRefresh = true) {
        // console.log(data, 'data')
        if (data?.accessToken) {
            const jwtHelper = new JwtHelper();
            const decodedAccessToken = jwtHelper.decodeToken(data.accessToken);
            this.setToken(data.accessToken);
            this.setRefreshToken(data.refreshToken);
            this.setTimeExpiration(data.timeExpiration);
            if (isRefresh) {
                this.setUserToLocalStore(decodedAccessToken);
                this.loadCacheCatalog();
            }
        }
    }

    // mapMenu(menuServer, parentId, namespace, node = NODE_BASE) {
    //     let menusEsb: any = [];
    //     const menuChild = menuServer.filter(x => x.parentId === parentId);
    //     (menuChild || []).forEach(element => {
    //         if (element.parentId == null && element.url && element.url.includes(EMB_MENU)) {
    //             const menusESBData = this.mapMenu(menuServer, element.id, '', NODE_PARENT);
    //             this.setMenuListToLocalStore(menusESBData, LocalStoreEnum.Menu_List_EMB);
    //             return;
    //         }
    //         if (element.parentId == null && element.url && element.url.includes(RETAIL_MENU)) {
    //             const menusRetail = this.mapMenu(menuServer, element.id, '', NODE_PARENT);
    //             this.setMenuListToLocalStore(menusRetail, LocalStoreEnum.Menu_List_RETAIL);
    //             return;
    //         }
    //         if (element.parentId == null && element.url && element.url.includes(DFORM_MENU)) {
    //             const menusDform = this.mapMenu(menuServer, element.id, DFORM, NODE_PARENT);
    //             this.setMenuListToLocalStore(menusDform, LocalStoreEnum.Menu_List);
    //             return;
    //         }
    //         const url = namespace === '' ? element.url : `${namespace}/${element.url}`;
    //         const menu: any = {
    //             id: element.id,
    //             route: !element.typeLink || element.typeLink === 'angularLink' ? url : element.url,
    //             name: element.name,
    //             parentId: node === NODE_PARENT ? 0 : element.parentId,
    //             position: element.position,
    //             icon: element.icon,
    //             active: element.active,
    //             activeService: element.activeService,
    //             type: element.type,
    //             typeLink: element.typeLink
    //         };
    //         const children = this.mapMenu(menuServer, element.id, url, NODE_CHILD);
    //         if (children && children.length > 0) {
    //             const menuSubChild = menuServer.filter(x => x.parentId === element.id && x.active);
    //             if (menuSubChild.length > 0) {
    //                 menu.route = null;
    //             }
    //             menusEsb = [...menusEsb, ...children];
    //         }
    //         menusEsb.push(menu);
    //     });
    //     return menusEsb;
    // }

    private setUserToLocalStore(user: any) {
        this.localStore.savePermanentData(user, LocalStoreEnum.User_Infor);
    }

    private setToken(token: string) {
        this.localStore.savePermanentData(token, LocalStoreEnum.Token);
    }

    private setRefreshToken(token: string) {
        if (token) {
            this.localStore.savePermanentData(token, LocalStoreEnum.RefreshToken);
        }
    }

    private setTimeExpiration(time: number) {
        this.localStore.savePermanentData(time / 1000 - 15, LocalStoreEnum.Time_Expiration);
        this.localStore.savePermanentData(new Date(), LocalStoreEnum.Date_Expiration);
    }

    /**
     * Kiểm tra tồn tại localStore
     */
    private isStoreDataValid(): boolean {

        // Kiểm tra các biến môi trường token, refreshtoken, user_info
        for (const x of this.STORAGE) {
            if (!this.localStore.exists(x)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determines whether login is
     * @returns true if logged in with unexpired EA
     */
    tokenValid(flagToken = false): boolean {
        // check tab cuối thì add lại token khi reload
        const token = this.localStore.getData(LocalStoreEnum.Key_Reload);
        if (token && flagToken) {
            this.setToken(token);
            this.localStore.deleteData(LocalStoreEnum.Key_Reload);
        }
        return this.isStoreDataValid();
    }


    /**
     * Xử lý refresh tocken theo thời gian
     */
    private refreshToken = () => {
        const refreshToken = this.localStore.getData(LocalStoreEnum.RefreshToken);
        if (refreshToken) {
            const opstions: HttpOptions = {
                path: PATH.GENERATE_TOKEN,
                url: URL_LOGIN,
                params: {
                    grantType: 'REFRESH_TOKEN',
                    token: refreshToken
                },
                isAuthentication: false
            };
            return this.httpClient.get(opstions)
                .pipe(
                    take(1),
                    tap((response: any) => this.storeSession(response.data, false)),
                    map((response: any) => {
                        this.startRefreshTokenTimer();
                        return response;
                    }),
                    catchError(_ => of({ status: 500 }))
                );
        }
    };

    /**
     * Xử lý đăng xuất tài khoản
     */
    public logout() {
        if (!this.tokenValid()) {
            this.navigateLogin()
            return;
        }
        this.clearData();
        // const opstions: HttpOptions = {
        //     path: PATH.REVOKE_TOKEN,
        //     url: URL_LOGIN,
        //     params: {
        //         token: this.localStore.getData(LocalStoreEnum.RefreshToken)
        //     }
        // };
        // this.indicator.showActivityIndicator()
        // this.httpClient.delete(opstions)
        //     .pipe(
        //         take(1),
        //         finalize(() => this.indicator.hideActivityIndicator())
        //     ).subscribe(res => {
        //         this.clearData();
        //     }, (error) => {
        //         if (error?.status == 401) {
        //             this.clearData(false);
        //         } else {
        //             const message = error.error && error?.error?.message ? error?.error?.message : 'Có lỗi xảy ra. Vui lòng liên hệ CNTT để được hỗ trợ.';
        //             const uuid = error.error && error.error?.uuid ? error.error?.uuid : null;
        //             // this.dialogService.error({
        //             //     title: 'dialog.notification',
        //             //     innerHTML: uuid ? `${message} <br> ${uuid}` : `${message}`
        //             // }, result => { if (result) { } });
        //         }
        //     });
    }

    public clearData(isClearStorage = true) {
        this.stopRefreshTokenTimer();
        this.dialogManagementService.closeAll();
        this.menu.reset();
        // Object.keys(CATALOG).forEach(v => CATALOG[v] = [])
        if (isClearStorage) {
            this.clearClientStorage();
            this.stopRefreshCatalogTimer();
        } else {
            this.clearClientToken();
        }
        this.navigateLogin();

    }

    /**
     * clear thông tin lưu trữ ở local
     */
    clearClientStorage() {
        this.localStore.clearAllStorage();
    }

    clearClientToken() {
        this.localStore.deleteData(LocalStoreEnum.RefreshToken);
        this.localStore.deleteData(LocalStoreEnum.Token);
    }


    /**
     * Xử lý refresh lại token khi hết time
     */
    private startRefreshTokenTimer() {
        const expires = moment(this.localStore.getData(LocalStoreEnum.Date_Expiration)).add(this.localStore.getData(LocalStoreEnum.Time_Expiration), 'seconds');
        const timeData = expires.diff(moment(new Date()), 'seconds');
        if (timeData && timeData > 0) {
            this.timeoutExporation = setTimeout(() => this.refreshToken().subscribe(res => {
                if (res && res.status === Status.ERROR) {
                }
            }), timeData * 1000);
        }
    }

    /**
     * Xử lý refresh lại thông tin catalog khi hết time
     */
    private startRefreshCatalogTimer() {
        this.timeoutCatalog = setTimeout(() => this.refreshCacheCatalog().subscribe((result: Array<any>) => {
            result.map((val, index) => {
                if (val && val.data) {
                    this.localStore.savePermanentData(val.data, URL_GET_ALL[index].value);
                }
            });
        }), environment.expriedCatalog);
    }

    private stopRefreshTokenTimer() {
        clearTimeout(this.timeoutExporation);
    }

    private stopRefreshCatalogTimer() {
        clearTimeout(this.timeoutCatalog);
    }

    /**
     * Lấy lại thông tin catalog
     * Một số catalog lấy theo param, case TH catalog
     */
    private refreshCacheCatalog() {
        this.startRefreshCatalogTimer();
        return forkJoin(URL_GET_ALL.map(data => {
            let paramsCatalog = {};
            return this.httpClient.get({ url: data.url, path: PATH.URL_CATEGORY[data.key], params: paramsCatalog }).pipe(take(1), timeout(2000), catchError(_ => of(null)));
        }));
    }

    /**
     * Load thông tin catalog lưu vào localstore
     */
    private loadCacheCatalog() {
        if (this.isStoreCaltalogValid()) {
            console.log(2333)
            return;
        } else {
            // Xử lý lấy thông tin danh mục.
            this.refreshCacheCatalog().subscribe((result: Array<any>) => {
                result.map((val, index) => {
                    // this.localStore.savePermanentData(val, URL_GET_ALL[index].value);
                    if (val && val.data) {
                        console.log(val, 'vvvv')
                        this.localStore.savePermanentData(val.data, URL_GET_ALL[index].value);
                    }
                });
            });
        }
    }

    /**
     * Kiểm tra tồn tại localStore Catalog
     */
    private isStoreCaltalogValid(): boolean {
        for (const x of URL_GET_ALL) {
            if (!this.localStore.exists(x.value)) {
                return false;
            }
        }
        return true;
    }

    startToken() {
        if (this.tokenValid(true)) {
            const dateExpiration = this.localStore.getData(LocalStoreEnum.Date_Expiration);
            if (!moment().isSame(moment(dateExpiration), 'day')) {
                this.clearData();
            }
            if (!this.timeoutExporation && this.localStore.exists(LocalStoreEnum.RefreshToken)) {
                this.startRefreshTokenTimer();
            }
            if (!this.timeoutCatalog) {
                this.startRefreshCatalogTimer();
            }
        }
    }

    // Kiểm tra xem đủ localstore chưa. Nếu chưa có thì gọi lại
    public loadCatalogByConfig(config: any[]): Promise<boolean> {
        return new Promise(
            (resolve, reject) => {
                const listCatalog = (config || []).filter(x => !this.localStore.exists(x.value));

                if (listCatalog.length > 0) {
                    this.indicator.showActivityIndicator()
                    this.reloadCacheCatalog(listCatalog).pipe(
                        finalize(() => this.indicator.hideActivityIndicator()),
                    ).subscribe((result: Array<any>) => {
                        result.map((val, index) => {
                            if (val && val.data) {
                                this.localStore.savePermanentData(val.data, listCatalog[index].value);
                                // reloadCatalog(listCatalog[index].value);
                            }
                        });
                        resolve(true);
                    }, (err) => {
                        // const config = {
                        //     positionClass: 'toast-bottom-right',
                        //     timeOut: 3000,
                        //     extendedTimeOut: 3000
                        // };
                        // this.toastr.showToastr(
                        //     `Không lấy được danh sách danh mục. Vui lòng liên hệ CNTT để được hỗ trợ`,
                        //     'Thông báo!',
                        //     MessageSeverity.error,
                        //     config
                        // );
                        this.navigateLogin();
                        resolve(false);
                    });
                } else {
                    resolve(true);
                }
            }
        );
    }

    // Gọi lại api load catalog chưa có trên localstore
    private reloadCacheCatalog(listCatalog) {
        return forkJoin(listCatalog.map(data => {
            let paramsCatalog = {};
            return this.httpClient.get({ url: data.url, path: PATH.URL_CATEGORY[data.key], params: paramsCatalog }).pipe(map(res => res));
        }));
    }

    /**
     * Kiểm tra phiên làm việc trên trình duyệt
     */
    sectionIDE() {
        // const tab = this.localStore.getData(Tab_number);
        // this.localStore.savePermanentData(tab >= 0 ? tab + 1 : 1, Tab_number);
    }


    /**
     * Xóa thông tin khi đóng tab cuối cùng
     */
    clearSectionIDE() {
        // const tab = this.localStore.getData(Tab_number) - 1;
        // this.localStore.savePermanentData(tab, Tab_number);
        // if (tab == 0) {
        //     const token = this.localStore.getData(LocalStoreEnum.Token);
        //     this.deleteCatalog();
        //     this.localStore.saveSessionData(token, LocalStoreEnum.Key_Reload);
        // }
    }

    startIdle() {
        this.ngZone.runOutsideAngular(() => {
            if (this.idleJobIsRunning) {
                this.restartIdleJob();
            } else {
                this.startIdleJob();
            }
        });
    }

    /**
     * 
     * @param idleJob 
     */
    // private setSessionLocalExpired(idleJob: Idle): Idle {
    //     return idleJob
    //         .whenNotInteractive()
    //         .within(environment.sessionTimeout, 1000)
    //         .do(() => {
    //             if (this.tokenValid()) {
    //                 this.ngZone.run(() => {
    //                     this.deleteCatalog();
    //                     this.localStore.deleteData(LocalStoreEnum.RefreshToken);
    //                     this.stopRefreshTokenTimer();
    //                     this.stopRefreshCatalogTimer();
    //                 });
    //             }
    //             // del idle job
    //             this.stopIdleJob();
    //         });
    // }

    /**
     * Tắt idle check trạng thái nhàn rỗi
     */
    stopIdleJob() {
        // if (this.idleJob !== undefined && this.idleJob instanceof Idle) {
        //     this.idleJob.stop();
        //     this.idleJobIsRunning = false;
        // }
    }

    /**
     * Bật idle check trạng thái nhàn rỗi
     */
    startIdleJob() {
        // if (this.idleJob !== undefined && this.idleJob instanceof Idle) {
        //     this.idleJob.start();
        //     this.idleJobIsRunning = true;
        // }
    }

    /**
     * Restart idle check trạng thái nhàn rỗi
     */
    restartIdleJob() {
        // if (this.idleJob !== undefined && this.idleJob instanceof Idle) {
        //     this.idleJob.restart();
        //     this.idleJobIsRunning = true;
        // }
    }

    navigateLogin() {
        this.dialogManagementService.closeAll();
        this.router.navigate(['/auth/login']);
    }

    private deleteCatalog() {
        this.localStore.deleteData(LocalStoreEnum.Token);
        this.localStore.deleteData('listRM');
        this.localStore.deleteData('listMaKiemNgan');
    }
}
