import { ChangeDetectorRef, Component, Inject, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Status } from '@core';
import { ComponentAbstract } from '@shared-sm';
import * as moment from 'moment';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { IS_ADD_NEW } from '../../form-setting/add-new';
import { STT, CHUCVUMOIQH, DIA_CHI, DIENTHOAI, EMAIL, GIOITINH, LOAIHINHID, NGAY_CAP, NGAY_HIEU_LUC, NOI_CAP, SO_DKSH, TENTCCN, TKGDCK } from '../../form-setting/add-new-b14';

@Component({
  selector: 'app-add-new-b14',
  templateUrl: './add-new-b14.component.html',
  styleUrls: ['./add-new-b14.component.scss']
})
export class AddNewB14Component extends ComponentAbstract {

  isAddNew = IS_ADD_NEW();
  
  STT = STT();
  TENTCCN = TENTCCN();
  TKGDCK = TKGDCK();
  CHUCVUMOIQH = CHUCVUMOIQH();
  GIOITINH = GIOITINH();
  DIA_CHI = DIA_CHI();
  LOAIHINHID = LOAIHINHID();
  SO_DKSH = SO_DKSH();
  NGAY_CAP = NGAY_CAP();
  NOI_CAP = NOI_CAP();
  DIENTHOAI = DIENTHOAI();
  EMAIL = EMAIL();
  NGAY_HIEU_LUC = NGAY_HIEU_LUC();

  constructor(
    protected injector: Injector,
    public dialogRef: MatDialogRef<AddNewB14Component>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {
    super(injector);
  }
  checked = false;

  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.STT, this.TENTCCN, this.TKGDCK, this.CHUCVUMOIQH, this.GIOITINH, this.DIA_CHI, this.LOAIHINHID, this.SO_DKSH, this.NGAY_CAP, this.NOI_CAP, this.DIENTHOAI, this.EMAIL]);
    if(this.data?.data?.id) {
      this.form.patchValue(this.data.data)
    }
  }

  saveData() {
    this.validateAllFields(this.form);
    if (!this.form.valid) {
      this.dialogService.error({
        title: 'dialog.notification',
        message: 'dialog.valid-error',
      }, res => {
        if (res) {
        }
      });
      return;
    }

    
    
    const formData = this.form.getRawValue()
    let paramDialog = {
      data: {
        ...formData,
        id: this.data?.data?.id || null
      },
    }
    this.dialogRef.close(paramDialog)

  }

  closeDialog() {
    if (this.dialogRef.close) { this.dialogRef.close(null); }
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  testData() {
    const data = {
      id: null,
      stt: 3,
      tenTcCn: "Nguyễn Văn A",
      tkgdck: "005C055145",
      chucVuMoiQh: "Chủ tịch HĐQT",
      gioiTinh: "Nam",
      diaChi: "Hà Nội",
      loaiHinhId: "CMND",
      soDksh: "01302856",
      ngayCap: "2012-12-06",
      noiCap: "118 Hưng Phú, P.8, Q.8, Tp.HCM",
      dienThoai: "0988866562",
      email: "vana@gmail.com",
    }
    this.form.patchValue(data)
  }

}
