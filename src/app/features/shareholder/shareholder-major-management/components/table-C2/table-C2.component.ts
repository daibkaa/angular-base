import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MessageSeverity, Status } from '@core';
import { ComponentAbstract, DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import { finalize, takeUntil } from 'rxjs/operators';
import { ShareholderMajorManagementService } from '../../services/shareholder-major-management.service';
import { EditC2Component } from '../edit-c2/edit-c2.component';

@Component({
  selector: 'app-table-C2',
  templateUrl: './table-C2.component.html',
  styleUrls: ['./table-C2.component.scss'],
})
export class TableC2Component extends ComponentAbstract {

  @Input() dataSource: MatTableDataSource<any>;
  @Input() hasDataSource = false;
  displayedColumns: string[] = ['stt', 'tenTcCn', 'soDksh', 'batDau', 'ketThuc', 'soLuongDkGd', 'thucTeGd', 'cbttDkGd', 'cbttKqGd', 'giaiToaMbs', 'email', 'ghiChu', 'action'];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;
  @Output() emitUpdate = new EventEmitter();

  constructor(
    protected injector: Injector,
    private _shareholderMajorManagementService: ShareholderMajorManagementService
  ) {
    super(injector);
  } 
  // Xử lý dữ liệu đầu vào
  componentInit(): void {}

  destroyData() {
  }

  onEdit(element, index) {
    this.dialogService.componentDialog(EditC2Component, {
      width: '70%',
      height: '93%',
      data: {
        data: element,
      }
    }, (res) => {
      if (res) {
        this.indicator.showActivityIndicator();
        this._shareholderMajorManagementService.updateStockMBB(res.data).pipe(
          takeUntil(this.ngUnsubscribe),
          finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
          if(res?.code == Status.SUCCESS) {
            this.emitUpdate.emit(true)
            const config = {
              positionClass: 'toast-bottom-right',
              timeOut: 3000,
              extendedTimeOut: 3000
            };
            this.toastr.showToastr(
              `Chỉnh sửa thông tin thành công`,
              'Thông báo!',
              MessageSeverity.success,
              config
            );
          }
        })
      }
    });
  }

}