import { CheckboxItem, DateTimeItem, getDataLocalStorageByKey, NgSelectItem, TextboxItem } from "@shared-sm";
import { GIOI_TINH } from "../../core/constant";

export const STT = () => new TextboxItem({
   key: 'stt',
   placeholder: 'stt',
   label: 'STT',
   value: '',
   layout: '50',
   required: true
});

export const TENTCCN = () => new NgSelectItem({
   key: 'tenTcCn',
   placeholder: 'Tên TCCN',
   label: 'Tên TCCN',
   value: '',
   layout: '50',
   options: [],
   required: true
});

export const TKGDCK = () => new TextboxItem({
   key: 'tkgdck',
   placeholder: 'tkgdck',
   label: 'Tkgdck',
   value: '',
   layout: '50',
});

export const CHUCVUMOIQH = () => new TextboxItem({
   key: 'chucVuMoiQh',
   placeholder: 'Chức vụ / Mỗi QH',
   label: 'Chức vụ / Mỗi QH',
   value: '',
   layout: '50',
});

export const GIOITINH = () => new NgSelectItem({
   key: 'gioiTinh',
   placeholder: 'Gioi Tinh',
   label: 'Giới tính',
   value: '',
   layout: '50',
   options: GIOI_TINH,
});

export const DIA_CHI = () => new TextboxItem({
   key: 'diaChi',
   placeholder: '',
   label: 'Địa chỉ',
   value: '',
   layout: '50',
});

export const LOAIHINHID = () => new TextboxItem({
   key: 'loaiHinhId',
   placeholder: '',
   label: 'Loai hình ID',
   value: '',
   layout: '50',
});

export const SO_DKSH = () => new TextboxItem({
   key: 'soDksh',
   placeholder: '',
   label: 'Số DKSH',
   value: '',
   layout: '50',
   required: true
});

export const NGAY_CAP = () => new DateTimeItem({
   key: 'ngayCap',
   placeholder: '',
   label: 'Ngày cấp',
   value: '',
   layout: '50',
});

export const NOI_CAP = () => new TextboxItem({
   key: 'noiCap',
   placeholder: '',
   label: 'Nơi cấp',
   value: '',
   layout: '50',
});

export const DIENTHOAI = () => new TextboxItem({
   key: 'dienThoai',
   placeholder: 'Số điện thoại',
   label: 'Số điện thoại',
   value: '',
   layout: '50',
});

export const EMAIL = () => new TextboxItem({
   key: 'email',
   placeholder: 'Email',
   label: 'Email',
   value: '',
   layout: '50',
});

export const NGAY_HIEU_LUC = () => new DateTimeItem({
   key: 'validDate',
   placeholder: '',
   label: 'Ngày hiệu lực',
   value: '',
   layout: '50',
});

export const PARENTID = () => new NgSelectItem({
   key: 'parentId',
   placeholder: 'Người nội bộ',
   label: 'Người nội bộ',
   value: '',
   layout: '50',
   options: [],
   required: true
});

export const DEMUC = () => new TextboxItem({
   key: 'deMuc',
   placeholder: 'Đề mục',
   label: 'Đề mục',
   value: '',
   layout: '50',
   required: true
});

export const CHUTHICHDEMUCDDL = () => new NgSelectItem({
   key: 'chuThichDeMuc',
   placeholder: 'Đề mục',
   label: 'Đề Mục',
   value: '',
   layout: '50',
   required: true
});

export const NGUOINOIBO = () => new TextboxItem({
   key: 'nguoiNoiBo',
   placeholder: 'Người nội bộ',
   label: 'Người nội bộ',
   value: '',
   layout: '50',
});

export const CHUTHICHNGUOINOIBO = () => new TextboxItem({
   key: 'chuThichNguoiNoiBo',
   placeholder: 'Chú thich người nội bộ',
   label: 'Chú thích người nội bộ',
   value: '',
   layout: '50',
});

export const CHIMUC = () => new NgSelectItem({
   key: 'chiMuc',
   placeholder: 'Chỉ mục',
   label: 'Chỉ mục',
   value: '',
   layout: '50',
   options: [],
   required: true
});