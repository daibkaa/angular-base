import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/auth/auth.guard';
import { PermissionCatalogUtilitiesGuard } from 'src/app/core/auth/permission-catalog-utilities.guard';
import { ShareholderManagementComponent } from './components/main-screens/shareholder-management.component';

const routes: Routes = [
  // { path: '', redirectTo: 'shareholder-management', pathMatch: 'full' },
  { 
    path: '', 
    component: ShareholderManagementComponent, 
    data: { title: 'Quản lý cổ đông' },
    canActivate: [AuthGuard, PermissionCatalogUtilitiesGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ShareholderManagementRoutingModule {
}
