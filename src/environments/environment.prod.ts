export const environment = {
  production: true,
  urlAuth: 'https://qlcd-app-1.herokuapp.com/api',
  timeOut: 6000,
  expriedCatalog: 14400000
};
