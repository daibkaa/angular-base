import { ChangeDetectorRef, Component, Inject, Injector } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { ComponentAbstract } from '@shared-sm';
import * as moment from 'moment';
import { CHUC_VU_TAI_DON_VI, CHUTHICHNGUOINOIBO, CMT_NGUOI_DAI_DIEN, DIA_CHI, NGANH_NGHE, NGAY_CAP, NGAY_HIEU_LUC, NOI_CAP, SO_DKSH, STT, TEN_NGUOI_DAI_DIEN_VON, NGUOINOIBO, NGUOILIENQUAN, MOIQUANHE, PARENTID, DEMUC, CHUTHICHDEMUCDDL } from '../../form-setting/add-new-cdl';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-add-new-b13-nnb-nlq',
  templateUrl: './add-new-b13-nnb-nlq.component.html',
  styleUrls: ['./add-new-b13-nnb-nlq.component.scss']
})
export class AddNewB13NNBNLQComponent extends ComponentAbstract {
  
  PARENTID = PARENTID();
  DEMUC = DEMUC();
  CHUTHICHDEMUCDDL = CHUTHICHDEMUCDDL();
  NGUOINOIBO = NGUOINOIBO();
  CHUTHICHNGUOINOIBO = CHUTHICHNGUOINOIBO();
  STT = STT();
  NGUOILIENQUAN = NGUOILIENQUAN();
  SO_DKSH = SO_DKSH();
  NGAY_CAP= NGAY_CAP();
  NOI_CAP = NOI_CAP();
  DIA_CHI = DIA_CHI();
  TEN_NGUOI_DAI_DIEN_VON = TEN_NGUOI_DAI_DIEN_VON();
  CHUC_VU_TAI_DON_VI = CHUC_VU_TAI_DON_VI();
  CMT_NGUOI_DAI_DIEN = CMT_NGUOI_DAI_DIEN();
  MOIQUANHE = MOIQUANHE();
  NGANH_NGHE = NGANH_NGHE();
  NGAY_HIEU_LUC = NGAY_HIEU_LUC();

  listDemuc: Array<any> = [];
  showListDemuc: boolean = false;
  listNNB: Array<any> = [];
  showListNNB: boolean = false;

  constructor(
    protected injector: Injector,
    public dialogRef: MatDialogRef<AddNewB13NNBNLQComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {
    super(injector);
  }
  checked = false;

  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.PARENTID, this.DEMUC, this.CHUTHICHDEMUCDDL, this.NGUOINOIBO, this.CHUTHICHNGUOINOIBO, this.STT, this.NGUOILIENQUAN, this.SO_DKSH, this.NGAY_CAP, this.NOI_CAP, this.DIA_CHI, this.TEN_NGUOI_DAI_DIEN_VON, this.CHUC_VU_TAI_DON_VI, this.CMT_NGUOI_DAI_DIEN, this.MOIQUANHE, this.NGANH_NGHE]);

    this.PARENTID.options = ([...this.data.listData] || []).filter(f => !f.parentId).map(m => {
      return {
        key: m.id,
        value: m.cdLon
      }
    });
    if(this.data?.data?.id) {
      this.form.patchValue(this.data.data)
      this.form.get(this.PARENTID.key).disable();
      this.form.get(this.DEMUC.key).disable();
      this.form.get(this.CHUTHICHDEMUCDDL.key).disable();
      this.form.get(this.NGUOINOIBO.key).disable();
      this.form.get(this.CHUTHICHNGUOINOIBO.key).disable();
      this.listDemuc = ([...this.data.listData] || []).filter(f => (f.parentId == this.data.data?.parentId) && !f.deMuc && !f.soDksh);
    }

    this.onChangeParentId();
    this.onChangeDemuc();
  }

  onChangeParentId() {
    this.form.get(this.PARENTID.key).valueChanges.pipe(
      takeUntil(this.ngUnsubscribe),
      distinctUntilChanged()).subscribe(value => {
        if(value) {
          this.listDemuc = ([...this.data.listData] || []).filter(f => (f.parentId == value) && !f.deMuc && !f.soDksh)
        } else {
          this.listDemuc = [];
        }
      })
  }

  onSelect(item) {
    this.form.get(this.DEMUC.key).setValue(item.stt)
    this.form.get(this.CHUTHICHDEMUCDDL.key).setValue(item.cdLon)
  }

  focusEvent($event) {
    this.showListDemuc = true;
  }

  focusOutEvent($event) {
    setTimeout(() => {
      this.showListDemuc = false;
      const item = ([...this.listDemuc] || []).find(f => f.stt == this.form.get(this.DEMUC.key).value);
      if(item) {
        this.form.get(this.CHUTHICHDEMUCDDL.key).setValue(item.cdLon)
      } else {
        // do something
      } 
    }, 200)
  }

  onChangeDemuc() {
    this.form.get(this.DEMUC.key).valueChanges.pipe(
      takeUntil(this.ngUnsubscribe),
      distinctUntilChanged()).subscribe(value => {
        if(value) {
          const item = ([...this.listDemuc] || []).find(f => f.stt == value);
          this.listNNB = ([...this.data.listData] || []).filter(f => (f?.parentId == item?.parentId) && f?.deMuc == item.stt && !f.nguoiNoiBo && f.cdLon)
        } else {
          this.listNNB = [];
        }
        
      })
  }

  onSelectNBB(item) {
    this.form.get(this.NGUOINOIBO.key).setValue(item.stt)
    this.form.get(this.CHUTHICHNGUOINOIBO.key).setValue(item.cdLon)
  }

  focusEventNBB($event) {
    this.showListNNB = true;
  }

  focusOutEventNBB($event) {
    setTimeout(() => {
      this.showListNNB = false;
      const item = ([...this.listNNB] || []).find(f => f.stt == this.form.get(this.NGUOINOIBO.key).value);
      if(item) {
        this.form.get(this.CHUTHICHNGUOINOIBO.key).setValue(item.cdLon)
      } else {
        // do something
      } 
    }, 200)
  }

  saveData() {
    this.validateAllFields(this.form);
    if (!this.form.valid) {
      this.dialogService.error({
        title: 'dialog.notification',
        message: 'dialog.valid-error',
      }, res => {
        if (res) {
        }
      });
      return;
    }

    
    const formData = this.form.getRawValue()
    let paramDialog = {
      data: {
        ...formData,
        id: this.data.data?.id || null
      },
    }
    this.dialogRef.close(paramDialog)

  }

  closeDialog() {
    if (this.dialogRef.close) { this.dialogRef.close(null); }
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  testData() {
    const data = {
      cdLon: "CDL test",
      diaChi: 'Dia chi test',
      id: null,
      moiQuanHe: null,
      nganhNghe: 'Nganh nghe test',
      ngayCap: "2004-10-07",
      noiCap: "118 Hưng Phú, P.8, Q.8, Tp.HCM",
      parentId: null,
      reportDate: "2022-05-09",
      soDksh: "0300450962",
      stt: "",
      tenNguoiDaiDienVon: 'Nguoi dai dien test',
      cmtNguoiDaiDien: '142723338',
      validDate: '2022-06-01'
    }
    this.form.patchValue(data)
  }

}
