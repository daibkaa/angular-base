import { NgModule } from '@angular/core';
import { ThemeModule } from '../theme/theme.module';
import { FeaturesRoutingModule } from './features-routing.module';
  
const COMPONENTS = [];
const COMPONENTS_DYNAMIC = [];
const PROVIDERS = [];

@NgModule({
  imports: [ThemeModule, FeaturesRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_DYNAMIC ],
  entryComponents: COMPONENTS_DYNAMIC,
  providers: PROVIDERS
})
export class FeaturesModule { }
