export const COLUMN_BC_12 = ['stt', 'cdLon', 'nguoiLienQuan', 'soDksh', 'ngayCap', 'noiCap', 'diaChi', 'tenNguoiDaiDienVon', 'chucVuTaiDonVi', 'cmtNguoiDaiDien', 'moiQuanHe', 'nganhNghe', 'tyLeSoHuuCdlonTaiDonVi', 'soHuuMbb', 'tyLe', 'action'];

export const COLUMN_BC_8 = ['stt', 'tenTcCn', 'tkgdck', 'chucVuMoiQh', 'diaChi', 'soDksh', 'ngayCap', 'noiCap', 'dienThoai', 'email', 'soLuong', 'tyLe', 'gdTrongThang'];

export const CDL_TYPE_ROMAN = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII', 'XIII'];

export const CLD_TYPE_DEMUC = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

export const REJECT_COLUMN_DISPLAYS = ['reportDate', 'id', 'parentId', 'ids', 'createdBy', 'createdDate', 'updatedBy', 'updatedDate', 'deleted', 'createBy', 'createDate', 'updateBy', 'updateDate'];

export const GIOI_TINH = [
    {
        key: 'Nam',
        value: 'Nam'
    },
    {
        key: 'Nữ',
        value: 'Nữ'
    },
    {
        key: 'Khác',
        value: 'Khác'
    }
];

export const COLUMN_CURRENCY = ['chuaLk', 'daLk', 'tongCp', 'ptClk', 'ptDlk', 'tongPt', 'cpDaiDien', 'cpThamDu', 'coPhan', 'soLuongCoDong', 'soCoDong', 'soLuongCoPhan', 'cpSauUQ', 'coPhanSoHuu', 'slgdtongky', 'thuctemuaphclk', 'thuctemuaphdlk', 'tongthucteph', 'cotucclk', 'cotucdlk', 'tongcotuc', 'thuechualk', 'thuedalk', 'tongthuetncn', 'cotucclksauthue', 'cotucdlksauthue', 'tongcotucsauthue', 'cophansohuu', 'phieuBQ']