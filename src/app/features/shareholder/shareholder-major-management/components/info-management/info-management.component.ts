import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ComponentAbstract, ComponentBaseAbstract, DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize, map, mergeMap, takeUntil, distinctUntilChanged } from 'rxjs/operators';
import { BC_TYPE, BLANK_STRING, MessageSeverity, Status } from 'src/app/core';
import { checkFileType, removeEmptyObject } from 'src/app/core/utils/utils.function';
import { REJECT_COLUMN_DISPLAYS } from '../../../core/constant';
import { FULL_NAME, GROUP_TYPE, NLQHOTEN, NLQSODKSH, NNBHOTEN, NNBSODKSH, OWNER_REGISTRATIO_NUMBER, RAW_TYPE, REPORT_DATE_MAJOR, REPORT_TYPE_MAJOR } from '../../form-setting/form-setting';
import { ShareholderMajorManagementService } from '../../services/shareholder-major-management.service';
import { AddNewB13NNBNLQComponent } from '../add-new-b13-nnb-nlq/add-new-b13-nnb-nlq.component';
import { AddNewB13Component } from '../add-new-b13/add-new-b13.component';
import { AddNewB14NNBNLQComponent } from '../add-new-b14-nnb-nlq/add-new-b14-nnb-nlq.component';
import { AddNewB14Component } from '../add-new-b14/add-new-b14.component';
import { AddNewB15B16NNBNLQComponent } from '../add-new-b15-b16-nnb-nlq/add-new-b15-b16-nnb-nlq.component';
import { AddNewB15B16Component } from '../add-new-b15-b16/add-new-b15-b16.component';
import { AddNewChiMucComponent } from '../add-new-chimuc/add-new-chimuc.component';
import { AddNewDeMucB13Component } from '../add-new-demuc-b13/add-new-demuc-b13.component';
import { AddNewDeMucB14Component } from '../add-new-demuc-b14/add-new-demuc-b14.component';
import { EditChuthichNnb } from '../edit-chuthich-nnb/edit-chuthich-nnb.component';

@Component({
  selector: 'app-info-management',
  templateUrl: './info-management.component.html',
  styleUrls: ['./info-management.component.scss'],
})
export class InfoManagementComponent extends ComponentAbstract {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  ownerRegisterNumber = OWNER_REGISTRATIO_NUMBER();
  fullName = FULL_NAME();
  groupType = RAW_TYPE();
  NLQHOTEN = NLQHOTEN();
  NLQSODKSH = NLQSODKSH();
  NNBHOTEN = NNBHOTEN();
  NNBSODKSH = NNBSODKSH();

  public dataSource: MatTableDataSource<any>;
  public hasDataSource = false;
  displayedColumns: string[];
  reportType;

  titleParent;
  titleChild;

  readonly BC_TYPE = BC_TYPE;
  
  constructor(
    protected _injector: Injector,
    public _shareholderMajorManagementService: ShareholderMajorManagementService,
  ) {
    super(_injector);
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.groupType, this.NLQHOTEN, this.NLQSODKSH, this.NNBHOTEN, this.NNBSODKSH]);
  }

  searchReport(): void {
    this.validateAllFields(this.form);
    if(!this.form.valid) {
      this.dialogService.error({
        title: 'Thông báo',
        innerHTML: 'Vui lòng nhập thông tin tìm kiếm'
      });
      return;
    }
    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue()
    let params: any = removeEmptyObject(formData)
    this.reportType = formData.rawType;
    switch(this.reportType) {
      case BC_TYPE.TT22: {
        this.titleParent = 'Thêm thông tin NNB T22';
        this.titleChild = 'Thêm thông tin NLQ T22';
      }; break;
      case BC_TYPE.LUAT_TCTD: {
        this.titleParent = 'Thêm thông tin NNB LUAT TCTD';
        this.titleChild = 'Thêm thông tin NLQ LUAT TCTD';
      }; break;
      case BC_TYPE.LUAT_CK: {
        this.titleParent = 'Thêm thông tin NNB LUAT CK';
        this.titleChild = 'Thêm thông tin NLQ LUAT CK';
      }; break
      default: {
        this.titleParent = 'Thêm thông tin CĐL';
        this.titleChild = 'Thêm thông tin NLQ CĐL';
      }; break;
    }
    this._shareholderMajorManagementService.getInfoManage(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
      if (res && res.code === Status.SUCCESS && res?.data?.length) {
        this.hasDataSource = true;
        this.dataSource = new MatTableDataSource(res.data);
      } else {
        this.hasDataSource = false;
        this.dataSource = new MatTableDataSource([]);
      }
    }, error => {
      this.hasDataSource = false;
      this.dataSource = new MatTableDataSource([]);
    })
  }

  createdCommand() {
    const formData = this.form.getRawValue();
    switch(this.reportType) {
      case BC_TYPE.TT22: {
      this.dialogService.componentDialog(AddNewB14Component, {
        width: '70%',
        height: '93%',
        data: {
          listData: this.dataSource.data,
        }
      }, (res) => {
        if (res) {
          this.handleDialogEvent(res, formData);
        }
      });
      }; break;

      case BC_TYPE.LUAT_CK:
      case BC_TYPE.LUAT_TCTD: {
        this.indicator.showActivityIndicator();
        const params = {
          rawType: this.reportType
        }
        this._shareholderMajorManagementService.getListChimuc(params).pipe(
          takeUntil(this.ngUnsubscribe),
          finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
          if(res?.code == Status.SUCCESS) {
             this.dialogService.componentDialog(AddNewB15B16Component, {
                width: '70%',
                height: '93%',
                data: {
                  listData: this.dataSource.data,
                  reportType: this.reportType,
                  listChiMuc: res.data.map(m => ({key: m, value: m}))
                }
              }, (res) => {
                if (res) {
                  this.handleDialogEvent(res, formData);
                }
              });
          }
        })
        }; break;

        default: {
          this.dialogService.componentDialog(AddNewB13Component, {
            width: '70%',
            height: '93%',
            data: {
              listData: this.dataSource.data,
            }
          }, (res) => {
            if (res) {
              this.handleDialogEvent(res, formData);
            }
          });
        }; break;
    }
  }

  update(event) {
    const formData = this.form.getRawValue();
    switch(this.reportType) {
      case BC_TYPE.TT22: {
        if(event?.parentId) {
          if(event?.soDksh) {
            this.dialogService.componentDialog(AddNewB14NNBNLQComponent, {
              width: '70%',
              height: '93%',
              data: {
                data: event,
                listData: this.dataSource.data
              }
            }, (res) => {
              if (res) {
                this.handleDialogEvent(res, formData);
              }
            });
          } else {
            this.dialogService.componentDialog(AddNewDeMucB14Component, {
              width: '70%',
              height: '70%',
              data: {
                data: event,
                listData: this.dataSource.data,
              }
            }, (res) => {
              if (res) {
                this.handleDialogEvent(res, formData);
              }
            });
          }
          
        } else {
          this.dialogService.componentDialog(AddNewB14Component, {
            width: '70%',
            height: '93%',
            data: {
              data: event,
              listData: this.dataSource.data
            }
          }, (res) => {
            if (res) {
              this.handleDialogEvent(res, formData);
            }
          });
        }
     
      }; break;

      case BC_TYPE.LUAT_CK:
      case BC_TYPE.LUAT_TCTD: {
            if(event?.parentId) {
              if(event?.soDksh) {
                this.dialogService.componentDialog(AddNewB15B16NNBNLQComponent, {
                  width: '70%',
                  height: '93%',
                  data: {
                    data: event,
                    listData: this.dataSource.data,
                    reportType: this.reportType
                  }
                }, (res) => {
                  if (res) {
                    this.handleDialogEvent(res, formData);
                  }
                });
              } else {
                this.dialogService.componentDialog(AddNewDeMucB14Component, {
                  width: '70%',
                  height: '70%',
                  data: {
                    data: event,
                    listData: this.dataSource.data,
                    reportType: this.reportType
                  }
                }, (res) => {
                  if (res) {
                    this.handleDialogEvent(res, formData);
                  }
                });
              }
            } else {
              if(event?.soDksh) {
                this.indicator.showActivityIndicator();
                const params = {
                  rawType: this.reportType
                }
                this._shareholderMajorManagementService.getListChimuc(params).pipe(
                  takeUntil(this.ngUnsubscribe),
                  finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
                  if(res?.code == Status.SUCCESS) {
                    console.log(res, 'rrrr')
                    this.dialogService.componentDialog(AddNewB15B16Component, {
                        width: '70%',
                        height: '93%',
                        data: {
                          data: event,
                          listData: this.dataSource.data,
                          reportType: this.reportType,
                          listChiMuc: res.data.map(m => ({key: m, value: m}))
                        }
                      }, (res) => {
                        if (res) {
                          this.handleDialogEvent(res, formData);
                        }
                      });
                  }
                })
              } else {
                this.dialogService.componentDialog(AddNewChiMucComponent, {
                  width: '50%',
                  height: '60%',
                  data: {
                    data: event,
                    listData: this.dataSource.data,
                    reportType: this.reportType
                  }
                }, (res) => {
                  if (res) {
                    this.handleDialogEvent(res, formData);
                  }
                });
              }
              
              };
            } break;
      default: {
        if(event?.parentId) {
          if(event?.soDksh) {
            this.dialogService.componentDialog(AddNewB13NNBNLQComponent, {
              width: '70%',
              height: '93%',
              data: {
                data: event,
                listData: this.dataSource.data
              }
            }, (res) => {
              if (res) {
                this.handleDialogEvent(res, formData);
              }
            });
          } else {
            if(event?.deMuc) {
              this.dialogService.componentDialog(EditChuthichNnb, {
                width: '70%',
                height: '93%',
                data: {
                  data: event,
                  listData: this.dataSource.data,
                }
              }, (res) => {
                if (res) {
                  this.handleDialogEvent(res, formData);
                }
              });
            } else {
              this.dialogService.componentDialog(AddNewDeMucB13Component, {
                width: '70%',
                height: '80%',
                data: {
                  data: event,
                  listData: this.dataSource.data,
                }
              }, (res) => {
                if (res) {
                  this.handleDialogEvent(res, formData);
                }
              });
            }
            
          }
          
        } else {
          this.dialogService.componentDialog(AddNewB13Component, {
            width: '70%',
            height: '93%',
            data: {
              data: event,
              listData: this.dataSource.data
            }
          }, (res) => {
            if (res) {
              this.handleDialogEvent(res, formData);
            }
          });
        }
      }; break;
    }
  }

  handleDialogEvent(res, formData) {
    this.indicator.showActivityIndicator();
    const body = {
      ...res.data,
    }
    console.log(body, 'bddđ')
    this._shareholderMajorManagementService.inSertOrUpdateB13B16(body, this.reportType).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((response: any) => {
      if(response?.code == Status.SUCCESS) {
        this.searchReport();
        const config = {
          positionClass: 'toast-bottom-right',
          timeOut: 3000,
          extendedTimeOut: 3000
        };
        this.toastr.showToastr(
          res?.data?.id ? `Cập nhật thông tin thành công` : `Tạo mới thông tin thành công`,
          'Thông báo!',
          MessageSeverity.success,
          config
        );
      }
    })
  }

  createdNNBNLQ() {
    const formData = this.form.getRawValue();
    switch(this.reportType) {
      case BC_TYPE.TT22: {
      this.dialogService.componentDialog(AddNewB14NNBNLQComponent, {
        width: '70%',
        height: '93%',
        data: {
          listData: this.dataSource.data,
        }
      }, (res) => {
        if (res) {
          this.handleDialogEvent(res, formData);
        }
      });
      }; break;

      case BC_TYPE.LUAT_CK:
      case BC_TYPE.LUAT_TCTD: {
        this.dialogService.componentDialog(AddNewB15B16NNBNLQComponent, {
          width: '70%',
          height: '93%',
          data: {
            listData: this.dataSource.data,
            reportType: this.reportType
          }
        }, (res) => {
          if (res) {
            this.handleDialogEvent(res, formData);
          }
        });
        }; break;

        default: {
          this.dialogService.componentDialog(AddNewB13NNBNLQComponent, {
            width: '70%',
            height: '93%',
            data: {
              listData: this.dataSource.data,
            }
          }, (res) => {
            if (res) {
              this.handleDialogEvent(res, formData);
            }
          });
        }; break;
    }
  }

  onDeleteData(element) {
    this.dialogService.dformconfirm({
      title: 'Thông báo',
      innerHTML: `Bạn chắc chắn muốn xóa thông tin?`,
      closeBtn: 'Hủy bỏ',
      acceptBtn: 'Xác nhận',
    }, (result: any) => {
      if (result) {
        if(result.status == 2) {
          this.indicator.showActivityIndicator();
          const body = {
            ...element,
          }
          this._shareholderMajorManagementService.deleteNLQCDL(body, this.reportType).pipe(
            takeUntil(this.ngUnsubscribe),
            finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
            if (res && res.code === Status.SUCCESS) {
              this.searchReport();
              const config = {
                positionClass: 'toast-bottom-right',
                timeOut: 3000,
                extendedTimeOut: 3000
              };
              this.toastr.showToastr(
                `Xóa thông tin thành công`,
                'Thông báo!',
                MessageSeverity.success,
                config
              );
            }
          })
        }
      }
    });  
  }

  createdDemuc() {
    const formData = this.form.getRawValue();
    switch(this.reportType) {
      case BC_TYPE.TT22: {
      this.dialogService.componentDialog(AddNewDeMucB14Component, {
        width: '70%',
        height: '70%',
        data: {
          listData: this.dataSource.data,
        }
      }, (res) => {
        if (res) {
          this.handleDialogEvent(res, formData);
        }
      });
      }; break;

      case BC_TYPE.LUAT_CK:
      case BC_TYPE.LUAT_TCTD: {
        this.dialogService.componentDialog(AddNewDeMucB14Component, {
          width: '70%',
          height: '70%',
          data: {
            listData: this.dataSource.data,
            reportType: this.reportType
          }
        }, (res) => {
          if (res) {
            this.handleDialogEvent(res, formData);
          }
        });
        }; break;

        default: {
          this.dialogService.componentDialog(AddNewDeMucB13Component, {
            width: '70%',
            height: '80%',
            data: {
              listData: this.dataSource.data,
            }
          }, (res) => {
            if (res) {
              this.handleDialogEvent(res, formData);
            }
          });
        }; break;
    }
  }

  createdChimuc() {
    const formData = this.form.getRawValue();
    this.dialogService.componentDialog(AddNewChiMucComponent, {
      width: '50%',
      height: '60%',
      data: {
        listData: this.dataSource.data,
        reportType: this.reportType
      }
    }, (res) => {
      if (res) {
        this.handleDialogEvent(res, formData);
      }
    });
  }

  createdChuThichNNB() {
    const formData = this.form.getRawValue();
    this.dialogService.componentDialog(EditChuthichNnb, {
      width: '70%',
      height: '93%',
      data: {
        listData: this.dataSource.data,
      }
    }, (res) => {
      if (res) {
        this.handleDialogEvent(res, formData);
      }
    });
  }

  /**
   * Event load data phân trang ((page)="ChanePageIndex($event);")
   */
  ChanePageIndex($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex;
  }
  

  destroyData() {
  }
}