import { environment } from '@env/environment';
import { HttpClientService } from './../../core/service/httpclient.service';
import { Component, Input, ViewEncapsulation } from '@angular/core';
// import { MenuService } from '@core';
import { HttpClient } from '@angular/common/http';
import { MENU_LIST } from 'src/app/core';
import { MenuService } from 'src/app/core/bootstrap/menu.service';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SidemenuComponent {
  // NOTE: Ripple effect make page flashing on mobile
  @Input() ripple = true;
  menus = this.menu.getAll();
  winData;
  route;
  isLoad = false;
  
  constructor(private menu: MenuService) {
  }

  // Delete empty values and rebuild route
  buildRoute(routes: string[]) {
    let route = '';
    routes.forEach(item => {
      if (item && item.trim()) {
        route += '/' + item.replace(/^\/+|\/+$/g, '');
      }
    });
    return route;
  }


  loadUrl(route) {
    this.isLoad = true;
    this.route = route;
    // this.winData = window.open(this.url, "_blank");
    window.addEventListener('message', this.load, false);
  }

  load = (event) => {
    if (this.isLoad && event.data && (typeof event.data === 'string' || event.data instanceof String) && event.data === 'ping') {
      // const token = this.localStore.getData(LocalStoreEnum.Token)
      // this.winData.postMessage({ token, refUrl: this.route }, this.url);
      this.isLoad = false;
    }
  }

}
