import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { QRCodeModule } from 'angularx-qrcode';
import { NotFoundComponent } from './not-found/not-found.component';
import { PrintQrComponent } from './print-qr/print-qr.component';
import { PublicRoutingModule } from './public-routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DformDialogModule } from '@shared/components/dform-dialogs/dialog.module';
import { IframeComponent} from './iframe/iframe.component';

@NgModule({
  declarations: [NotFoundComponent, PrintQrComponent, IframeComponent ],
  imports: [
    PublicRoutingModule,
    QRCodeModule,
    MatButtonModule,
    FlexLayoutModule,
    TranslateModule,
    CommonModule,
    DformDialogModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
  ],
  exports: [],
  providers: []
})
export class PubblicModule { }
