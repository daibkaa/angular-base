import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import * as _ from 'lodash';

@Component({
  selector: 'app-table-C3',
  templateUrl: './table-C3.component.html',
  styleUrls: ['./table-C3.component.scss'],
})
export class TableC3Component implements OnInit {

  @Input() dataSource: MatTableDataSource<any>;
  @Input() hasDataSource = false;
  @Input() compareDate;
  @Input() reportDate;

  displayedColumns: string[] = ['stt', 'tenTcCn', 'tkgdck', 'chucVuMoiQh', 'diaChi', 'soDksh', 'ngayCap', 'noiCap', 'dienThoai', 'email', 'soLuongCompareDate', 'tyLeCompareDate', 'soLuongReportDate', 'tyLeReportDate', 'thayDoiTrongKy'];

  constructor(
  ) {
  }

  // Xử lý dữ liệu đầu vào
  ngOnInit(): void {}

  destroyData() {
  }

}