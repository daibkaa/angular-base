import { ChangeDetectorRef, Component, Inject, Injector } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { ComponentAbstract } from '@shared-sm';
import * as moment from 'moment';
import { CHUTHICHDEMUC, CHUTHICHNGUOINOIBO, DEMUC, NGAY_HIEU_LUC, NGUOINOIBO, PARENTID } from '../../form-setting/chuthich-nnb';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-chuthich-nnb',
  templateUrl: './edit-chuthich-nnb.component.html',
  styleUrls: ['./edit-chuthich-nnb.component.scss']
})
export class EditChuthichNnb extends ComponentAbstract {
  
  PARENTID = PARENTID();
  DEMUC = DEMUC();
  CHUTHICHDEMUC= CHUTHICHDEMUC();
  NGUOINOIBO = NGUOINOIBO();
  CHUTHICHNGUOINOIBO = CHUTHICHNGUOINOIBO();

  listDemuc: Array<any> = [];
  showListDemuc: boolean = false;
  listNNB: Array<any> = [];
  showListNNB: boolean = false;

  constructor(
    protected injector: Injector,
    public dialogRef: MatDialogRef<EditChuthichNnb>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {
    super(injector);
  }
  checked = false;

  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.PARENTID, this.DEMUC, this.CHUTHICHDEMUC, this.NGUOINOIBO, this.CHUTHICHNGUOINOIBO]);

    this.PARENTID.options = ([...this.data.listData] || []).filter(f => !f.parentId).map(m => {
      return {
        key: m.id,
        value: m.cdLon
      }
    });
    
    if(this.data?.data?.id) {
      this.DEMUC.options = ([...this.data.listData] || []).filter(f => (f.parentId == this.data?.data.parentId) && !f.deMuc && !f.soDksh).map(m => {
        return {
          key: m.stt,
          value: m.stt
        }
      });
      this.form.patchValue(this.data.data);
      this.form.get(this.PARENTID.key).disable();
      this.form.get(this.DEMUC.key).disable();
    }
    this.form.get(this.CHUTHICHDEMUC.key).disable();

    this.onChangeParentId();
    this.onChangeDemuc();
  }

  onChangeParentId() {
    this.form.get(this.PARENTID.key).valueChanges.pipe(
      takeUntil(this.ngUnsubscribe),
      distinctUntilChanged()).subscribe(value => {
        if(value) {
          this.listDemuc = ([...this.data.listData] || []).filter(f => (f.parentId == value) && !f.deMuc && !f.soDksh);
          console.log(this.listDemuc, 'this.listDemuc')
          this.DEMUC.options = ([...this.data.listData] || []).filter(f => (f.parentId == value) && !f.deMuc && !f.soDksh).map(m => {
            return {
              key: m.stt,
              value: m.stt
            }
          });
        } else {
          this.listDemuc = [];
        }
        
      })
  }

  onChangeDemuc() {
    this.form.get(this.DEMUC.key).valueChanges.pipe(
      takeUntil(this.ngUnsubscribe),
      distinctUntilChanged()).subscribe(value => {
        if(value) {
          const demuc = this.listDemuc.find(f => f.stt == value);
          this.form.get(this.CHUTHICHDEMUC.key).setValue(demuc.cdLon);
        } else {
          this.form.get(this.CHUTHICHDEMUC.key).setValue(null);
        }
        
      })
  }

  saveData() {
    this.validateAllFields(this.form);
    if (!this.form.valid) {
      this.dialogService.error({
        title: 'dialog.notification',
        message: 'dialog.valid-error',
      }, res => {
        if (res) {
        }
      });
      return;
    }

    
    const formData = this.form.getRawValue()
    let paramDialog = {
      data: {
        ...formData,
        id: this.data.data?.id || null
      },
    }
    this.dialogRef.close(paramDialog)

  }

  closeDialog() {
    if (this.dialogRef.close) { this.dialogRef.close(null); }
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  testData() {
    const data = {
      cdLon: "CDL test",
      diaChi: 'Dia chi test',
      id: null,
      moiQuanHe: null,
      nganhNghe: 'Nganh nghe test',
      ngayCap: "2004-10-07",
      noiCap: "118 Hưng Phú, P.8, Q.8, Tp.HCM",
      parentId: null,
      reportDate: "2022-05-09",
      soDksh: "0300450962",
      stt: "III",
      tenNguoiDaiDienVon: 'Nguoi dai dien test',
      cmtNguoiDaiDien: '142723338',
      validDate: '2022-06-01'
    }
    this.form.patchValue(data)
  }

}
