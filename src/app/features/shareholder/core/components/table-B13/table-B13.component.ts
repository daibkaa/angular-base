import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ShareholderService } from '../../services/shareholder.service';

@Component({
  selector: 'app-table-B13',
  templateUrl: './table-B13.component.html',
  styleUrls: ['./table-B13.component.scss'],
})
export class TableB13Component implements OnInit {

  @Input() dataSource: MatTableDataSource<any>;
  @Input() hasDataSource = false;
  @Output() update = new EventEmitter();
  @Output() delete = new EventEmitter();

  displayedColumns: string[] = ['stt', 'cdLon', 'nguoiLienQuan', 'soDksh', 'ngayCap', 'noiCap', 'diaChi', 'tenNguoiDaiDienVon', 'chucVuTaiDonVi', 'cmtNguoiDaiDien', 'moiQuanHe', 'nganhNghe', 'tyLeSoHuuCdlonTaiDonVi', 'action'];

  constructor(
    public _shareholderService: ShareholderService,
  ) {
  }

  // Xử lý dữ liệu đầu vào
  ngOnInit(): void {}

  onUpdate(element) {
    this.update.emit(element)
  }

  onDelete(element) {
    this.delete.emit(element)
  }


  destroyData() {
  }

}