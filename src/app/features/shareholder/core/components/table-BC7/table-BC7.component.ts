import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { COLUMN_DEFAULT } from '@core';
import { DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import * as moment from 'moment';
import { CDL_TYPE_ROMAN, CLD_TYPE_DEMUC } from '../../constant';
import { ShareholderService } from '../../services/shareholder.service';

@Component({
  selector: 'app-table-BC7',
  templateUrl: './table-BC7.component.html',
  styleUrls: ['./table-BC7.component.scss'],
})
export class TableBC7Component {

  @Input() dataSource: MatTableDataSource<any>;
  @Input() hasDataSource = false;
  @Input() listColumnGroup;
  @Input() compareDate;
  @Input() reportDate;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  displayedColumns: string[] = ['stt', 'nhomCD', 'after', 'now', 'chenhLechTyLe'];

  constructor(
    public _shareholderService: ShareholderService,
  ) {
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
  }

  _rendertitleMouth(index) {
    const title = {
      0: `Tháng ${moment(this.reportDate).format('MM/YYYY')}`,
      1: `Tháng ${moment(this.compareDate).format('MM/YYYY')}`
    }
    return title[index];
  }

  destroyData() {
  }

  formatDate(date) {
    return moment(date).format('DD/MM/YYYY')
  }

}