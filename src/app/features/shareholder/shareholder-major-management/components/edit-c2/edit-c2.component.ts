import { Component, Inject, Injector } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ComponentAbstract } from '@shared-sm';
import * as moment from 'moment';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { IS_ADD_NEW } from '../../form-setting/add-new';
import { BATDAU, CBTTDKGD, CBTTKQGD, EMAIL, GHICHU, GIAITOAMBS, KETTHUC, SODKSH, SOLUONGDKGD, TENTCCN, THUCTEGD } from '../../form-setting/edit-c2';


@Component({
  selector: 'app-edit-c2',
  templateUrl: './edit-c2.component.html',
  styleUrls: ['./edit-c2.component.scss']
})
export class EditC2Component extends ComponentAbstract {

  isAddNew = IS_ADD_NEW();
  
  TENTCCN = TENTCCN();
  SODKSH = SODKSH();
  BATDAU = BATDAU();
  KETTHUC= KETTHUC();
  SOLUONGDKGD = SOLUONGDKGD();
  THUCTEGD = THUCTEGD();
  CBTTDKGD = CBTTDKGD();
  CBTTKQGD = CBTTKQGD();
  GIAITOAMBS = GIAITOAMBS();
  EMAIL = EMAIL();
  GHICHU = GHICHU();

  constructor(
    protected injector: Injector,
    public dialogRef: MatDialogRef<EditC2Component>,
    @Inject(MAT_DIALOG_DATA) private data: any,
  ) {
    super(injector);
  }
  checked = false;

  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.TENTCCN, this.SODKSH, this.BATDAU, this.KETTHUC, this.SOLUONGDKGD, this.THUCTEGD, this.CBTTDKGD, this.CBTTKQGD, this.GIAITOAMBS, this.EMAIL, this.GHICHU]);
    this.form.patchValue(this.data.data);
    // this.onChangeIsAddNew();
  }

  select() {
    this.checked = !this.checked;
  }
  
  onChangeIsAddNew() {
    this.form.get(this.isAddNew.key).valueChanges.pipe(
      takeUntil(this.ngUnsubscribe),
      distinctUntilChanged(),
    ).subscribe(val => {
      if(val) {
        this.checked = true
      } else {
        this.checked = false
      }
    })
  }

  saveData() {
    this.validateAllFields(this.form);
    if (!this.form.valid) {
      this.dialogService.error({
        title: 'dialog.notification',
        message: 'dialog.valid-error',
      }, res => {
        if (res) {
        }
      });
      return;
    }
    const formData = this.form.getRawValue()
    
    let paramDialog = {
      data: {
        ...formData,
        batDau: formData.batDau ? moment(formData.batDau).format('YYYY-MM-DD') : null,
        ketThuc: formData.ketThuc ? moment(formData.ketThuc).format('YYYY-MM-DD') : null,
        cbttDkGd: formData.cbttDkGd ? moment(formData.cbttDkGd).format('YYYY-MM-DD') : null,
        cbttKqGd: formData.cbttKqGd ? moment(formData.cbttKqGd).format('YYYY-MM-DD') : null,
        giaiToaMbs: formData.giaiToaMbs ? moment(formData.giaiToaMbs).format('YYYY-MM-DD') : null,
        id: this.data.data.id
      },
    }
    this.dialogRef.close(paramDialog)

  }

  closeDialog() {
    if (this.dialogRef.close) { this.dialogRef.close(null); }
  }

}
