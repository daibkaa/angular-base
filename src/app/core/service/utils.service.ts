import * as _ from 'lodash';

export function getDataLocalStorageByKey(key: string) {
  if (localStorage.getItem(key) && localStorage.getItem(key) !== 'null') {
    return JSON.parse(localStorage.getItem(key));
  } else {
    return [];
  }
}


export function getBlobUrl(data: any) {
  var blob = new Blob([data], { type: 'application/vnd.ms-excel' });
  var blobURL = URL.createObjectURL(blob);
  const fileHash = blobURL.split('/');
  const objectUrl = fileHash[fileHash.length - 1];
  const urlForm = `/form?fileId=${objectUrl}`;
  return urlForm;
}