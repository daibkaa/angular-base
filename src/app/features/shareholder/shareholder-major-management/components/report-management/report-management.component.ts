import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ComponentAbstract, ComponentBaseAbstract, DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize, map, mergeMap, takeUntil } from 'rxjs/operators';
import { BC_TYPE, MessageSeverity, Status } from 'src/app/core';
import { dateNoneSeparation } from 'src/app/core/utils/utils.function';
import { REPORT_TYPE_MAJOR, REPORT_DATE_MAJOR } from '../../form-setting/form-setting';
import { ShareholderMajorManagementService } from '../../services/shareholder-major-management.service';

@Component({
  selector: 'app-report-management',
  templateUrl: './report-management.component.html',
  styleUrls: ['./report-management.component.scss'],
})
export class ReportMmanagementComponent extends ComponentAbstract {

  public dataSource: MatTableDataSource<any>;
  public hasDataSource = false;
  displayedColumns: string[];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  reportType = REPORT_TYPE_MAJOR();
  reportDate = REPORT_DATE_MAJOR();

  listCdl;
  showtable;

  readonly BC_TYPE = BC_TYPE;
  
  constructor(
    protected _injector: Injector,
    private _shareholderMajorManagementService: ShareholderMajorManagementService,
  ) {
    super(_injector);
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.reportType, this.reportDate]);
  }

  searchReport(): void {
    this.validateAllFields(this.form);
    if(!this.form.valid) {
      this.dialogService.error({
        title: 'Thông báo',
        innerHTML: 'Vui lòng nhập thông tin tìm kiếm'
      });
      return;
    }
    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue();
    this.showtable = formData.reportType;
    let params: any = {
      reportType: formData?.reportType
    }
    if(formData?.reportDate && formData?.reportDate != '') {
      params.reportDate = moment(formData.reportDate).format('YYYY-MM-DD')
    }
    this._shareholderMajorManagementService.getDataReport(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
      if (res && res.code === Status.SUCCESS) {
        if(res.data?.length) {
          this.hasDataSource = true;
          this.dataSource = new MatTableDataSource(res.data);
        } else {
          this.hasDataSource = false;
          this.dataSource = new MatTableDataSource([]);
        }
      }
    }, error => {
      this.displayedColumns = [];
      this.hasDataSource = false;
      this.dataSource = new MatTableDataSource([]);
    })
  }

  destroyData() {
  }

  /**
 * Event load data phân trang ((page)="ChanePageIndex($event);")
 */
  ChanePageIndex($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex;
  }

  exportExcel() {
    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue()
    let params: any = {
      reportType: this.showtable
    }
    if(formData?.reportDate && formData?.reportDate != '') {
      params.reportDate = moment(formData.reportDate).format('YYYY-MM-DD')
    }
    this._shareholderMajorManagementService.export(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
        // this.openPDF(res.body)
        const blob = new Blob([res.body], { type: 'application/vnd.ms-excel' });
        const url= window.URL.createObjectURL(blob);
        let anchor = document.createElement("a");
        let downloadFile = '';
        switch(formData.reportType) {
          case BC_TYPE.NNB_NLQ_TT22: {
            downloadFile = `BC9.NNB_NLQ_TT22_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;
          }; break;
          case BC_TYPE.NNB_NLQ_LUAT_TCTD: {
            downloadFile = `BC10.NNB_NLQ_LUAT_TCTD_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;
          }; break;
          case BC_TYPE.NNB_NLQ_LUAT_CK: {
            downloadFile = `BC11.MBB_COTUC_TM_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;
          }; break;
          case BC_TYPE.CDLON_NLQ: {
            downloadFile = `BC12.CDLON_NLQ_${dateNoneSeparation(params.reportDate, '-')}.xlsx`;
          }; break;
        }

        // const nameFile = formData?.saoKe == '1' ? `BC14.SAO_KE_COTUC_TM.xlsx` : `BC13.SAO_KE_CP.xlsx`

        anchor.download = downloadFile;
        anchor.href = url;
        anchor.click();
    })
  }

}