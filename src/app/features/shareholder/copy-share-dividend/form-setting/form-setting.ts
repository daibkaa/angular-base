import { DateTimeItem, NgSelectItem, TextboxItem } from "@shared-sm";
import * as moment from "moment";
import { getDataLocalStorageByKey } from "src/app/core/service/utils.service";

const tmp = [
    {
        key: '0',
        value: 'CO_PHIEU'
    },
    {
        key: '1',
        value: 'TIEN_CO_TUC'
    }
]

export const SAOKE = () => new NgSelectItem({
    key: 'saoKe',
    placeholder: 'Sao kê',
    label: 'Sao kê',
    value: '1',
    layout: '50',
    options: tmp,
    required: true
});

export const TEN = () => new TextboxItem({
    key: 'ten',
    label: 'Tên',
    placeholder: 'Tên',
    value: '',  
    layout: '50',
    directives: ''
});

export const OWNER_REGISTRATIO_NUMBER = () => new TextboxItem({
    key: 'soDksh',
    label: 'Số đăng ký sở hữu',
    placeholder: 'Số đăng ký sở hữu',
    value: '',
    layout: '50',
    directives: '',
    required: true
});

export const  STARTDATE = () => new DateTimeItem({
    key: 'startDate',
    placeholder: 'Ngày bắt đầu',
    label: 'Ngày bắt đầu',
    value: moment().format('YYYY-MM-DD'),
    layout: '50',
    minDate: '1900-01-01',
});

export const  ENDDATE = () => new DateTimeItem({
    key: 'endDate',
    placeholder: 'Ngày kết thúc',
    label: 'Ngày kết thúc',
    value: moment().format('YYYY-MM-DD'),
    layout: '50',
    minDate: '1900-01-01',
});