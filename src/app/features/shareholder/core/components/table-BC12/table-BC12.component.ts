import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { COLUMN_DEFAULT } from '@core';
import { DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import { CLD_TYPE_DEMUC } from '../../constant';
import { ShareholderService } from '../../services/shareholder.service';

@Component({
  selector: 'app-table-BC12',
  templateUrl: './table-BC12.component.html',
  styleUrls: ['./table-BC12.component.scss'],
})
export class TableBC12Component implements OnInit {

  @Input() dataSource: MatTableDataSource<any>;
  @Input() hasDataSource = false;
  displayedColumns: string[] = ['stt', 'cdLon', 'nguoiLienQuan', 'soDksh', 'ngayCap', 'noiCap', 'diaChi', 'tenNguoiDaiDienVon', 'chucVuTaiDonVi', 'cmtNguoiDaiDien', 'moiQuanHe', 'nganhNghe', 'tyLeSoHuuCdlonTaiDonVi', 'soHuuMbb', 'tyLe'];;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;
  @Output() edit = new EventEmitter();
  @Output() delete = new EventEmitter();

  constructor(
    public _shareholderService: ShareholderService,
  ) {
  }

  // Xử lý dữ liệu đầu vào
  ngOnInit(): void {}

  destroyData() {
  }

  onEdit(element) {
    this.edit.emit(element)
  }

  onDelete(element) {
    this.delete.emit(element)
  }

  checkbg(row) {
    console.log(row, 'rrrr')
  }

}