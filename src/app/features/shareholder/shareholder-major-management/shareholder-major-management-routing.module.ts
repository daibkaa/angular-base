import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/auth/auth.guard';
import { ShareholderManagementComponent } from './components/main-screens/shareholder-management.component';

const routes: Routes = [
  { 
    path: '', 
    component: ShareholderManagementComponent, 
    data: { title: 'Quản lý cổ đông lớn, nnb, nlq' },
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ShareholderMajorManagementRoutingModule {
}
