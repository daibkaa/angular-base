import { MatIconModule } from '@angular/material/icon';
import { SharedSMModule } from '@shared-sm';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ShareholderManagementRoutingModule } from './copy-share-dividend-routing.module';
import { ShareholderManagementComponent } from './components/main-screens/shareholder-management.component';
import { MatTabsModule } from '@angular/material/tabs';
import { CopyShareDividendService } from './services/copy-share-dividend.service';
import { ReportManagementComponent } from './components/report-management/report-management.component';
import { MatTableModule } from '@angular/material/table';
import { ShareHolderCoreModule } from '../core/core.module';

const COMPONENTS_DYNAMIC = [];
const SERVICE = [CopyShareDividendService];

@NgModule({
  imports: [
    CommonModule,
    SharedSMModule,
    ShareholderManagementRoutingModule,
    MatIconModule,
    MatTabsModule,
    MatTableModule,
    ShareHolderCoreModule
  ],
  declarations: [
    ShareholderManagementComponent,
    ReportManagementComponent,
  ],
  entryComponents: COMPONENTS_DYNAMIC,
  providers: [...SERVICE]
})
export class CopyShareDividendModule {
}
