import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { COLUMN_DEFAULT } from '@core';
import { DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import { CDL_TYPE_ROMAN, CLD_TYPE_DEMUC } from '../../constant';
import { ShareholderService } from '../../services/shareholder.service';

@Component({
  selector: 'app-table-C5',
  templateUrl: './table-C5.component.html',
  styleUrls: ['./table-C5.component.scss'],
})
export class TableC5Component {

  // @Input() displayedColumns;
  @Input() dataSource: MatTableDataSource<any>;
  @Input() hasDataSource = false;
  @Input() listColumnGroupVande;
  @Input() listColumnGroupBauCu;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  displayedColumns: string[] = ['stt', 'hoTen', 'soDksh', 'tongCp', 'tyLe', 'cpSauUyQuyen', 'cpThamDu', 'tyLeThamDu', 'vanDe', 'bauCu'];

  constructor(
    public _shareholderService: ShareholderService,
  ) {
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
  }

  destroyData() {
  }

}