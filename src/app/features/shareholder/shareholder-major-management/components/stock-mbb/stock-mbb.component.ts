import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ComponentAbstract, ComponentBaseAbstract, DformPaginationPlusComponent } from '@shared-sm';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize, map, mergeMap, takeUntil } from 'rxjs/operators';
import { EXCEL_XLS, EXCEL_XLSX, HttpResponse, Status } from 'src/app/core';
import { checkFileType, removeEmptyObject } from 'src/app/core/utils/utils.function';
import { REJECT_COLUMN_DISPLAYS } from '../../../core/constant';
import { FULL_NAME, GROUP_TYPE, OWNER_REGISTRATIO_NUMBER, REPORT_DATE_MAJOR, REPORT_TYPE_MAJOR, TEN_TNCN } from '../../form-setting/form-setting';
import { ShareholderMajorManagementService } from '../../services/shareholder-major-management.service';

@Component({
  selector: 'app-stock-mbb',
  templateUrl: './stock-mbb.component.html',
  styleUrls: ['./stock-mbb.component.scss'],
})
export class StockMbbComponent extends ComponentAbstract {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  ownerRegisterNumber = OWNER_REGISTRATIO_NUMBER();
  tenTNCN = TEN_TNCN();

  public dataSource: MatTableDataSource<any>;
  public hasDataSource = false;
  displayedColumns: string[];
  fullDataSource;
  
  constructor(
    protected _injector: Injector,
    private _shareholderMajorManagementService: ShareholderMajorManagementService,
  ) {
    super(_injector);
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.ownerRegisterNumber, this.tenTNCN]);
  }

  destroyData() {
  }

  searchReport(): void {
    this.validateAllFields(this.form);
    if(!this.form.valid) {
      this.dialogService.error({
        title: 'Thông báo',
        innerHTML: 'Vui lòng nhập thông tin tìm kiếm'
      });
      return;
    }
    this.indicator.showActivityIndicator();
    const formData = this.form.getRawValue()
    let params: any = removeEmptyObject(formData)
  
    this._shareholderMajorManagementService.getStockMBB(params).pipe(
      takeUntil(this.ngUnsubscribe),
      finalize(() => this.indicator.hideActivityIndicator())).subscribe((res: any) => {
      if (res && res.code === Status.SUCCESS) {
        this.hasDataSource = true;
        this.fullDataSource = res.data
        this.totalItem = res.data.length
        const data = res.data.map((obj, index) => { obj.stt = index + 1; return obj; });
        const newData = data.slice(Number(this.pageSize) * Number(this.pageIndex), Number(this.pageSize) * (Number(this.pageIndex) + 1))
        this.dataSource = new MatTableDataSource(newData);
      }
    }, error => {
      this.hasDataSource = false;
      this.dataSource = new MatTableDataSource([]);
    })
  }

  /**
   * Event load data phân trang ((page)="ChanePageIndex($event);")
   */
  ChanePageIndex($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex;
    const newData = this.fullDataSource.slice(Number(this.pageSize) * Number(this.pageIndex), Number(this.pageSize) * (Number(this.pageIndex) + 1))
    this.dataSource = new MatTableDataSource(newData);
  }

  emitUpdate(ev) {
    this.searchReport();
  }
  

}