import { ChangeDetectorRef, Component, Inject, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ComponentAbstract } from '@shared-sm';
import * as moment from 'moment';
import { NGAY_HIEU_LUC, STT } from '../../form-setting/chimuc';

@Component({
  selector: 'app-add-new-chimuc',
  templateUrl: './add-new-chimuc.component.html',
  styleUrls: ['./add-new-chimuc.component.scss']
})
export class AddNewChiMucComponent extends ComponentAbstract {
  
  STT = STT();
  NGAY_HIEU_LUC = NGAY_HIEU_LUC();

  listDemuc: Array<any> = [];
  showListDemuc: boolean = false;
  listNNB: Array<any> = [];
  showListNNB: boolean = false;

  constructor(
    protected injector: Injector,
    public dialogRef: MatDialogRef<AddNewChiMucComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {
    super(injector);
  }
  checked = false;

  protected componentInit(): void {
    this.form = this.itemControl.toFormGroup([this.STT]);

    if(this.data?.data?.id) {
      this.form.patchValue(this.data.data);
    }
  }

  saveData() {
    this.validateAllFields(this.form);
    if (!this.form.valid) {
      this.dialogService.error({
        title: 'dialog.notification',
        message: 'dialog.valid-error',
      }, res => {
        if (res) {
        }
      });
      return;
    }

    
    const formData = this.form.getRawValue()
    let body = {
      ...formData,
      id: this.data.data?.id || null,
      chiMuc: formData.stt,
    }
    let paramDialog = {
      data: {
        ...body,
      },
    }
    this.dialogRef.close(paramDialog)

  }

  closeDialog() {
    if (this.dialogRef.close) { this.dialogRef.close(null); }
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

}
