import { Component, Inject, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ComponentAbstract, DformPaginationPlusComponent } from '@shared-sm';
@Component({
  selector: 'app-table-warning',
  templateUrl: './table-warning.component.html',
  styleUrls: ['./table-warning.component.scss'],
})
export class TableWarningComponent extends ComponentAbstract {

  @Input() hasDataSource = false;
  displayedColumns: string[] = ['tenTcCn', 'soDksh', 'soLuongGdTrongKy'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('pagePage', { static: true }) dformPagination: DformPaginationPlusComponent;

  constructor(
    protected injector: Injector,
    public dialogRef: MatDialogRef<TableWarningComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
  ) {
    super(injector);
  }

  // Xử lý dữ liệu đầu vào
  protected componentInit(): void {
    this.dataSource = new MatTableDataSource(this.data.data);
  }

  destroyData() {
  }

  closeDialog() {
    if (this.dialogRef.close) { this.dialogRef.close(null); }
  }

}