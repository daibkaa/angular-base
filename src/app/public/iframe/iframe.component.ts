import { Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-iframe',
  templateUrl: './iframe.component.html',
  styleUrls: ['./iframe.component.scss']
})
export class IframeComponent implements OnInit, OnDestroy {
  urlSafe: SafeResourceUrl;
  urlParamsQuery: Subscription;
  constructor(
    private sanitizer: DomSanitizer,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.urlParamsQuery =  this.route.queryParams.subscribe((queryParams: any) => {
      const blobInfo = `blob:${location.protocol}//${location.host}/${queryParams.fileId}`;
      console.log(blobInfo, 'blobInfo');
      this.urlSafe= this.sanitizer.bypassSecurityTrustResourceUrl(blobInfo);
      window.document.title = queryParams?.processCode ? queryParams?.processCode : 'Smart Chanel'
    });
  }

  ngOnDestroy() {
    this.urlParamsQuery.unsubscribe();
  }

}
